package com.movas.mservice12;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.movas.mcement.adapter.PricelistAdapter;
import com.movas.mcement.data.PriceService;
import com.movas.mcement.data.PriceServiceImpl;
import com.movas.mcement.entity.Product;
import com.movas.mcement.helper.DialogHelper;
import com.movas.mcement.helper.ErrorHandler;
import com.movas.mcement.util.Constants;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import cz.msebera.android.httpclient.Header;

public class PricelistActivity extends ActionBarActivity {

	RecyclerView list;
	PricelistAdapter mAdapter;
	
	String location;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pricelist);
		
		location = getIntent().getStringExtra(Constants.LOCATION);
		
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
        	
            @Override
            public void onClick(View v) {
                PricelistActivity.this.onBackPressed();
            }
        });
        
        getSupportActionBar().setTitle(getSupportActionBar().getTitle() + " - " + location);
        
        list = (RecyclerView) findViewById(R.id.list);
        list.setLayoutManager(new LinearLayoutManager(this));
        
        mAdapter = new PricelistAdapter(new ArrayList<Product>(), R.layout.row_pricelist, this);
        list.setAdapter(mAdapter);
        
        loadPriceLists();
	}
	
	private void loadPriceLists() {
		
		DialogHelper.showProgressDialog(this, getString(R.string.please_wait), false);
		
		SharedPreferences prefs = getApplication().getSharedPreferences(Constants.MovaPay, Context.MODE_PRIVATE);
		String uuid = prefs.getString(Constants.CUSTOMER_UUID, "");
		String token = prefs.getString("token", "");
		
		PriceService service = new PriceServiceImpl();
		
		try {

			service.loadPrices(token, uuid, location, new JsonHttpResponseHandler() {
				
				@Override
				public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
					super.onSuccess(statusCode, headers, response);
					
					DialogHelper.hideProgressDialog();
					
					try {
						
						Integer status = response.getInt("status");
						
						if(status == 0) {
							JSONArray products = response.getJSONArray("products");
														
							List<Product> plist = new ArrayList<Product>();
							
							for(int i = 0; i < products.length(); i++) {
								JSONObject row = products.getJSONObject(i);
								
								double price = row.getDouble("unitprice");
								DecimalFormat formatter = new DecimalFormat("#,###.00");
								
								Product product = new Product(row.getString("name"), "KES " + formatter.format(price));
								
								plist.add(product);
							}
							
							mAdapter.addItems(plist);
						} else {
							ErrorHandler.handleError(PricelistActivity.this, new Throwable(response.getString("message")));
						}
						
					} catch (JSONException e1) {}
				}
				
				@Override
				public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
					super.onFailure(statusCode, headers, responseString, throwable);
					DialogHelper.hideProgressDialog();
					ErrorHandler.handleError(PricelistActivity.this, throwable);
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
					super.onFailure(statusCode, headers, throwable, errorResponse);
					
					DialogHelper.hideProgressDialog();
					ErrorHandler.handleError(PricelistActivity.this, throwable);
				}
			});
		} catch (UnsupportedEncodingException | JSONException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.pricelist, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		int id = item.getItemId();
		if (id == R.id.action_refresh) {
			mAdapter.clear();
			loadPriceLists();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
