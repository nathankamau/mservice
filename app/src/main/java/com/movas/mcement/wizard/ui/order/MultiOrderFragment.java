package com.movas.mcement.wizard.ui.order;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.movas.mservice12.R;
import com.movas.mcement.listener.OrdersChangedListener;
import com.movas.mcement.util.PageValidator;
import com.movas.mcement.wizard.model.Page;
import com.movas.mcement.wizard.model.order.MultiOrderPage;
import com.movas.mcement.wizard.model.order.OrderPage;
import com.movas.mcement.wizard.ui.PageFragmentCallbacks;
import com.movas.mservice12.MultiOrderActivity;

import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;

public class MultiOrderFragment extends Fragment {
	
    private static final String ARG_KEY = "key";
    
    private PageFragmentCallbacks mCallbacks;
    private String mKey;
    private MultiOrderPage mPage;
    
    LinearLayout tonnageContainer;
	
    public static MultiOrderFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        
        MultiOrderFragment fragment = new MultiOrderFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    public MultiOrderFragment() {
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = (MultiOrderPage) mCallbacks.onGetPage(mKey);
        
        mPage.addValidator(new PageValidator() {
			
			@Override
			public Boolean isValid() {
				return MultiOrderFragment.this.isValid();
			}
		});
        
        mPage.addOrdersChanged(new OrdersChangedListener() {
			
			@Override
			public void onOrdersChanged() {
				initView();
			}
		});
    }
    
    protected void initView() {
    	
    	if(getActivity() instanceof MultiOrderActivity) {
        	MultiOrderActivity activity = (MultiOrderActivity) getActivity();
        	ArrayList<String> list = activity.onGetModel()
        			.getCurrentPageSequence().get(1).getData().getStringArrayList(Page.SIMPLE_DATA_KEY);
        	
        	String names[] = new String[list.size()];
        	
        	String[] oldNames = mPage.getData().getStringArray(MultiOrderPage.ORDER_NAMES);
        	String[] oldTonnages = mPage.getData().getStringArray(MultiOrderPage.ORDER_TONNAGES);
        	
        	tonnageContainer.removeAllViews();
        	for (int i = 0; i < list.size(); i++) {
        		
        		names[i] = list.get(i);
        		
        		LinearLayout orderRow = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.order_tonnage_row, null);
        		tonnageContainer.addView(orderRow);
        		
        		TextView nameView = (TextView) orderRow.findViewWithTag("name"); 
        		TextView tonnageView = (TextView) orderRow.findViewWithTag("tonnage"); 
        		
        		nameView.setText(list.get(i));
        		
        		if(oldNames != null && oldTonnages != null) {
                	
                	if(oldNames.length == oldTonnages.length) {
                		            		
                		for(int x = 0; x < oldNames.length; x++) {
                			
                			String name = oldNames[x];
                			
                			if(nameView.getText().equals(name)) {
        						tonnageView.setText(oldTonnages[x] + "");
        					}
                    	}
                	}
                }
        		
        		tonnageView.addTextChangedListener(new TextWatcher() {
            		
        			@Override
        			public void onTextChanged(CharSequence s, int start, int before, int count) {}
        			
        			@Override
        			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        			
        			@Override
        			public void afterTextChanged(Editable s) {
        				
        				setTonnageValues(true);
        			}
        		});
        	}
        	
        	mPage.getData().putStringArray(MultiOrderPage.ORDER_NAMES,names);
        	setTonnageValues(false);
        }
	}
    
	protected void setTonnageValues(Boolean notify) {
		String tonnages[] = new String[tonnageContainer.getChildCount()];
		
		for(int i = 0; i < tonnageContainer.getChildCount(); i++) {
			
			LinearLayout vRow = (LinearLayout) tonnageContainer.getChildAt(i);
			
			EditText vNoView = (EditText) vRow.getChildAt(1);
			
			tonnages[i] = vNoView.getText().toString();
		}
		
		mPage.getData().putStringArray(MultiOrderPage.ORDER_TONNAGES, tonnages);
        if(notify)
        	mPage.notifyDataChanged();
	}

	private Boolean isValid() {
    	
		Boolean isValid = FormValidator.validate(this, new SimpleErrorPopupCallback(getActivity()));
		
		if(!isValid)
			return false;
		else {
//			
//			if(transportMethodSpinner.getSelectedItemPosition() != 0) {
//				
//				if(deliveryLocationView.getText().toString().trim().equals("")) {
//					deliveryLocationView.setError(getString(R.string.validation_delivery_location));
//					return false;
//				}
//				
//			} else
//				return isVehicleNoValid();
			
			return true;
		}
	}
    
    protected Boolean isVehicleNoValid() {
		
		Boolean isValid = true;
		
		String[] vNos = mPage.getData().getStringArray(OrderPage.VEHICLE_NOS_ARRAY);
		
		if(vNos != null && vNos.length == 0) {
			Toast t = Toast.makeText(getActivity(), getString(R.string.vehicle_required), Toast.LENGTH_LONG);
			t.setGravity(Gravity.TOP, 0, 0);
			t.show();
			isValid = false;
		} else if(vNos != null) {
			
//			for(int i = 0; i < vehicleNosContainer.getChildCount(); i++) {
//				
//				LinearLayout vRow = (LinearLayout) vehicleNosContainer.getChildAt(i);
//				
//				EditText vNoView = (EditText) vRow.getChildAt(1);
//				
//				String vehicleNo = vNoView.getText().toString();
//				
//				if(TextUtils.isEmpty(vehicleNo)) {
//					
//					vNoView.setError(getString(R.string.validation_vehicleno));
//					isValid = false;
//				} else if(vehicleNo.length() < 5) {
//					
//					vNoView.setError(getString(R.string.validation_vehicleno_length));
//					isValid = false;
//				} else if(!vehicleNo.matches("^(?=.*\\d)(?=.*[a-zA-Z]).{5,10}$")) {
//					
//					vNoView.setError(getString(R.string.validation_vehicle_no_alphanumeric));
//					isValid = false;
//				}
//			}
		}
		
		return isValid;
	}
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_page_multi_order_tonnage, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());
        
        tonnageContainer = (LinearLayout) rootView.findViewById(R.id.tonnageContainer);
        
        
        
        
//        tonnage = (TextView) rootView.findViewById(R.id.tonnage);
//        tonnage.setText(mPage.getData().getString(OrderPage.TONNAGE_KEY));
		
        return rootView;
    }
    
    private void addVehicleRows(String[] vNos) {
    	
    	for(int i = 0; i < vNos.length; i++) {
    		addVehicleRow(vNos[i]);
    	}
	}
    
	protected void addVehicleRow(String plateNo) {
    	
    	final LinearLayout vehicleRow = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.vehicle_row, null);
    	
    	EditText vehicleNoView = (EditText) vehicleRow.getChildAt(1);
    	vehicleNoView.setText(plateNo);
    	
    	TextView counterView = (TextView) vehicleRow.getChildAt(0);
    	EditText vehicleTonnageView = (EditText) vehicleRow.getChildAt(2);
    	TextView removeView = (TextView) vehicleRow.getChildAt(3);
    	//counterView.setText((vehicleNosContainer.getChildCount() + 1) + "");
    	removeView.setOnClickListener(new View.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				//vehicleNosContainer.removeView(vehicleRow);
			}
		});
    	
    	//vehicleNosContainer.addView(vehicleRow);
    	
    	vehicleNoView.addTextChangedListener(new TextWatcher() {
    		
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			
			@Override
			public void afterTextChanged(Editable s) {
				
				String vehicleNos = "";
//				String vNos[] = new String[vehicleNosContainer.getChildCount()];
//				
//				for(int i = 0; i < vehicleNosContainer.getChildCount(); i++) {
//					
//					LinearLayout vRow = (LinearLayout) vehicleNosContainer.getChildAt(i);
//					
//					EditText vNoView = (EditText) vRow.getChildAt(1);
//					
//					vNos[i] = vNoView.getText().toString();
//					if(!TextUtils.isEmpty(vNoView.getText().toString())) {
//						vehicleNos += vehicleNos.equals("") ? vNoView.getText().toString() :  "," + vNoView.getText().toString();
//					}
//				}
				
				//mPage.getData().putStringArray(OrderPage.VEHICLE_NOS_ARRAY, vNos);
				mPage.getData().putString(OrderPage.VEHICLE_NO_KEY, vehicleNos);
                mPage.notifyDataChanged();
			}
		});
	}
    
	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
        if (!(activity instanceof PageFragmentCallbacks)) {
            throw new ClassCastException("Activity must implement PageFragmentCallbacks");
        }
        
        mCallbacks = (PageFragmentCallbacks) activity;
    }
	
    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }
    
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        
    }
    
    protected TextWatcher getTextWatcher(final String name) {
    	
    	return new TextWatcher() {
    		
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            
            @Override
            public void afterTextChanged(Editable editable) {
                mPage.getData().putString(name,
                        (editable != null) ? editable.toString() : null);
                mPage.notifyDataChanged();
            }
        };
    }
    
    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
    }
}