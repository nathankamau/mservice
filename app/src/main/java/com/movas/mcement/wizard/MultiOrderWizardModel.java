package com.movas.mcement.wizard;

import android.content.Context;
import com.movas.mcement.wizard.model.AbstractWizardModel;
import com.movas.mcement.wizard.model.BranchPage;
import com.movas.mcement.wizard.model.MultipleFixedChoicePage;
import com.movas.mcement.wizard.model.PageList;
import com.movas.mcement.wizard.model.order.MultiOrderDetailsPage;
import com.movas.mcement.wizard.model.order.MultiOrderPage;

public class MultiOrderWizardModel extends AbstractWizardModel {
	
    public MultiOrderWizardModel(Context context) {
        super(context);
    }
    
    public MultiOrderWizardModel(Context context, Object location) {
    	super(context, location);
    }
    
    @Override
    protected PageList onNewRootPageList() {
        return new PageList(
        		new BranchPage(this, "Purchase Location")
	                .addBranch("Athi River", 
	                	new MultipleFixedChoicePage(this, "Brands")
			                .setChoices(
		                		"Nguvu Bagged",
		                		"Nguvu Bulk",
		                		"Powerplus bagged",
		                		"Powermax Bagged",
		                		"Powercrete Bagged",
								"Tembo",
								"SupaSet"
		                	).setRequired(true)
	                ).addBranch("Mombasa", 
		                	new MultipleFixedChoicePage(this, "Brand")
				                .setChoices(
			                		"Nguvu Bagged",
			                		"Nguvu Bulk ",
			                		"Powerplus Bagged",
			                		"Powerplus Bulk",
			                		"Powermax Bagged",
			                		"Powermax Bulk",
			                		"Powercrete Bagged",
			                		"Powercrete Bulk"
			                	).setRequired(true)
	                		).addBranch("Export", 
	    		                	new MultipleFixedChoicePage(this, "Brand")
						                .setChoices(
					                		"Supaset",
					                		"Tembo",
					                		"Powerplus"
					                	).setRequired(true)
			                		).setRequired(true),
			        new MultiOrderPage(this, "Enter Tonnage").setRequired(true),
        			new MultiOrderDetailsPage(this, "Enter Details").setRequired(true)
        );
    }
}