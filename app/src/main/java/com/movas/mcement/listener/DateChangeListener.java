package com.movas.mcement.listener;

public interface DateChangeListener {
	
	public void addDateChangeListener(DateChangeAdapter adapter);
}
