package com.movas.mcement.wizard.model.order;

import java.util.ArrayList;
import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.movas.mcement.listener.OrdersChangedListener;
import com.movas.mcement.wizard.model.ModelCallbacks;
import com.movas.mcement.wizard.model.Page;
import com.movas.mcement.wizard.model.ReviewItem;
import com.movas.mcement.wizard.ui.order.MultiOrderFragment;

public class MultiOrderPage extends Page {
	
	//public static final String LPO_NO_KEY = "lpo_no";
	
	public static final String ORDER_NAMES = "order_names";
	
	public static final String ORDER_TONNAGES = "tonnages";
	
	OrdersChangedListener listener;
	
    public MultiOrderPage(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
    }
    
    @Override
    public Fragment createFragment() {
        return MultiOrderFragment.create(getKey());
    }
    
    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
    	StringBuilder sb = new StringBuilder();
    	
        String[] tonnages = mData.getStringArray(ORDER_TONNAGES);
        String[] names = mData.getStringArray(ORDER_NAMES);
        if (tonnages != null && tonnages.length > 0) {
            for (int i = 0; i < tonnages.length; i++) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append(names[i] + ": " + tonnages[i]);
            }
        }
        
        dest.add(new ReviewItem("Tonnage", sb.toString(), getKey()));
    }
    
    @Override
    public boolean isCompleted() {
    	
    	Boolean isComplete = true;
    	
    	String names[] = mData.getStringArray(ORDER_NAMES);
    	String tValues[] = mData.getStringArray(ORDER_TONNAGES);
    	
    	if(names != null && tValues != null) {
    		
    		if(names.length == tValues.length) {
        		
        		for(int i = 0; i < names.length; i++) {
        			
        			String t = tValues[i];
        			
        			if(TextUtils.isEmpty(t)) {
        				return false;
        			}
        		}
        	} else
        		return false;
    		
    	} else
    		return false;
    	
    	
        return isComplete;
    }

	public void notifySelectedOrdersChanged() {
		
		if(listener != null)
			listener.onOrdersChanged();
	}
	
	public void addOrdersChanged(OrdersChangedListener listener) {
		this.listener = listener;
	}
}