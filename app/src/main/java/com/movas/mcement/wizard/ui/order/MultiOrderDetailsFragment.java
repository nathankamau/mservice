package com.movas.mcement.wizard.ui.order;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.movas.mservice12.R;
import com.movas.mcement.util.PageValidator;
import com.movas.mcement.util.ToastUtil;
import com.movas.mcement.wizard.model.Page;
import com.movas.mcement.wizard.model.order.MultiOrderDetailsPage;
import com.movas.mcement.wizard.model.order.OrderPage;
import com.movas.mcement.wizard.ui.PageFragmentCallbacks;
import com.movas.mservice12.MultiOrderActivity;

import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;

public class MultiOrderDetailsFragment extends Fragment {
	
    private static final String ARG_KEY = "key";
    
    private PageFragmentCallbacks mCallbacks;
    private String mKey;
    private MultiOrderDetailsPage mPage;
    
    @NotEmpty(messageId = R.string.validation_lponumber, order = 1)
    EditText lpoNoView;		
	Spinner transportMethodSpinner;
	
	EditText deliveryLocationView;
	
	TextView pickupLabel;
	
	LinearLayout toBeCollectedContainer;
	LinearLayout vehicleNosContainer;
	Button addVehicleBtn;
	Boolean textChangedListenerAdded = false;
	
    public static MultiOrderDetailsFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        
        MultiOrderDetailsFragment fragment = new MultiOrderDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    public MultiOrderDetailsFragment() {
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = (MultiOrderDetailsPage) mCallbacks.onGetPage(mKey);
        
        mPage.addValidator(new PageValidator() {
			
			@Override
			public Boolean isValid() {
				return MultiOrderDetailsFragment.this.isValid();
			}
		});
    }
    
    private Boolean isValid() {
    	
		Boolean isValid = FormValidator.validate(this, new SimpleErrorPopupCallback(getActivity()));
		
		if(!isValid)
			return false;
		else {
			
			if(transportMethodSpinner.getSelectedItemPosition() != 0) {
				
				if(deliveryLocationView.getText().toString().trim().equals("")) {
					deliveryLocationView.setError(getString(R.string.validation_delivery_location));
					return false;
				}
				
			} else {
				isValid =  isVehicleNoValid();
				
				if(isValid)
					isValid = isProductValid();
				
				if(isValid)
					isValid = isTonnageValid();
				
				if(isValid)
					isValid = isTripsValid();
			}
				
			
			return isValid;
		}
	}
    
    private Boolean isTripsValid() {

    	Boolean isValid = true;
    	
    	for(int i = 0; i < vehicleNosContainer.getChildCount(); i++) {
			
			LinearLayout vRow = (LinearLayout) vehicleNosContainer.getChildAt(i);
			
			EditText tView = (EditText) vRow.findViewWithTag("trips");
			
			String trip = tView.getText().toString();
			
			if(TextUtils.isEmpty(trip)) {
				
				tView.setError(getString(R.string.validation_trips));
				tView.requestFocus();
				isValid = false;
			}
		}
    	
    	return isValid;
	}
    
    private Boolean isProductValid() {
    	
    	Boolean isValid = true;
    	
    	for(int i = 0; i < vehicleNosContainer.getChildCount(); i++) {
    		
			LinearLayout vRow = (LinearLayout) vehicleNosContainer.getChildAt(i);
						
			Spinner brandSpinner = (Spinner) vRow.findViewWithTag("brand");
			
			String brand = brandSpinner.getSelectedItem().toString();
			
			if(TextUtils.isEmpty(brand)) {
				
				ToastUtil.makeText(getActivity(), "Please select the brand to be carried by the chosen vehicle(s)", Toast.LENGTH_LONG).show();
				isValid = false;
			}
		}
    	
    	return isValid;
	}
    
    private Boolean isTonnageValid() {
    	
    	Boolean isValid = true;
    	
    	for(int i = 0; i < vehicleNosContainer.getChildCount(); i++) {
    		
			LinearLayout vRow = (LinearLayout) vehicleNosContainer.getChildAt(i);
			
			EditText tView = (EditText) vRow.findViewWithTag("tonnage");
			
			String tonnage = tView.getText().toString();
			
			if(TextUtils.isEmpty(tonnage)) {
				
				tView.setError(getString(R.string.validation_tonnage));
				tView.requestFocus();
				isValid = false;
			}
		}
    	
    	return isValid;
	}
    
	protected Boolean isVehicleNoValid() {
		
		Boolean isValid = true;
				
		if(vehicleNosContainer.getChildCount() == 0) {
			
			Toast t = Toast.makeText(getActivity(), getString(R.string.vehicle_required), Toast.LENGTH_LONG);
			t.setGravity(Gravity.TOP, 0, 0);
			t.show();
			isValid = false;
			
		} else {
			
			for(int i = 0; i < vehicleNosContainer.getChildCount(); i++) {
				
				LinearLayout vRow = (LinearLayout) vehicleNosContainer.getChildAt(i);
				
				EditText vNoView = (EditText) vRow.findViewWithTag("vehicleNo");
				
				String vehicleNo = vNoView.getText().toString();
				
				if(TextUtils.isEmpty(vehicleNo)) {
					
					vNoView.setError(getString(R.string.validation_vehicleno));
					vNoView.requestFocus();
					isValid = false;
				} else if(vehicleNo.length() < 5) {
					
					vNoView.setError(getString(R.string.validation_vehicleno_length));
					vNoView.requestFocus();
					isValid = false;
				} else if(!vehicleNo.matches("^(?=.*\\d)(?=.*[a-zA-Z]).{5,10}$")) {
					
					vNoView.setError(getString(R.string.validation_vehicle_no_alphanumeric));
					vNoView.requestFocus();
					isValid = false;
				}
			}
		}
		
		return isValid;
	}
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_page_multi_order_details, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());
        
        lpoNoView = (EditText) rootView.findViewById(R.id.lpoNoView);
        lpoNoView.setText(mPage.getData().getString(OrderPage.LPO_NO_KEY));
		pickupLabel = (TextView) rootView.findViewById(R.id.pickupLabel);
		transportMethodSpinner = (Spinner) rootView.findViewById(R.id.transportMethodSpinner);
		deliveryLocationView = (EditText) rootView.findViewById(R.id.deliveryLocationView);
		deliveryLocationView.setText(mPage.getData().getString(OrderPage.LPO_NO_KEY));
		toBeCollectedContainer = (LinearLayout) rootView.findViewById(R.id.toBeCollectedContainer);
		vehicleNosContainer = (LinearLayout) rootView.findViewById(R.id.vehicleNosContainer);
		addVehicleBtn = (Button) rootView.findViewById(R.id.addVehicleBtn);
		
		ArrayAdapter<CharSequence> transMethodAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.transport_methods, android.R.layout.simple_spinner_item);
		transMethodAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		transportMethodSpinner.setAdapter(transMethodAdapter);
		
		String tMethod = mPage.getData().getString(OrderPage.TRANSPORT_METHOD_KEY);
		
		if(!TextUtils.isEmpty(tMethod)) {
			for(int t = 0; t < transMethodAdapter.getCount(); t++) {
				
				if(transMethodAdapter.getItem(t).toString().equals(tMethod)) {
					transportMethodSpinner.setSelection(t);
					mPage.getData().putString(OrderPage.TRANSPORT_METHOD_KEY, transportMethodSpinner.getSelectedItem().toString());
					break;
				}
			}
		}
		
		transportMethodSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				
				deliveryLocationView.setVisibility(position == 1 ? View.VISIBLE : View.GONE);
				toBeCollectedContainer.setVisibility(position == 0 ? View.VISIBLE : View.GONE);
				
				mPage.getData().putString(OrderPage.TRANSPORT_METHOD_KEY, transportMethodSpinner.getSelectedItem().toString());
				
				if(position == 0) {
					
					deliveryLocationView.setText("");
					mPage.getData().putString(OrderPage.DELIVERY_LOCATION_KEY, "");
				} else if(position == 1) {
					
					//vehicleNoView.setText("");
					mPage.getData().putString(OrderPage.VEHICLE_NO_KEY, "");
				}
				
                mPage.notifyDataChanged();
			}
			
			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});
		
		addVehicleBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				addVehicleRow(-1);
			}
		});
		
			String[] vNos = mPage.getData().getStringArray(MultiOrderDetailsPage.VEHICLE_NOS_ARRAY);
						
			if(vNos != null && vNos.length > 1)
				addVehicleRows();
			else if(vNos != null && vNos.length == 1)
				addVehicleRow(0);
			else
				addVehicleRow(-1);
			
			lpoNoView.addTextChangedListener(getTextWatcher(OrderPage.LPO_NO_KEY));
	        deliveryLocationView.addTextChangedListener(getTextWatcher(OrderPage.DELIVERY_LOCATION_KEY));
		
        return rootView;
    }
    
    private void addVehicleRows() {
    	
    	String[] vNos = mPage.getData().getStringArray(MultiOrderDetailsPage.VEHICLE_NOS_ARRAY);
    	
    	for(int i = 0; i < vNos.length; i++) {
    		addVehicleRow(i);
    	}
    	
    	save();
	}
    
	protected void addVehicleRow(int index) {
    	
    	final LinearLayout vehicleRow = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.vehicle_row, null);
    	
    	Spinner brandSpinner = (Spinner) vehicleRow.findViewWithTag("brand");
    	EditText vehicleNoView = (EditText) vehicleRow.findViewWithTag("vehicleNo");
    	
    	EditText vehicleTonnageView = (EditText) vehicleRow.findViewWithTag("tonnage");
    	EditText tripsView = (EditText) vehicleRow.findViewWithTag("trips");
    	TextView removeView = (TextView) vehicleRow.findViewWithTag("removeBtn");
    	removeView.setOnClickListener(new View.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				vehicleNosContainer.removeView(vehicleRow);
				save();
			}
		});
    	
    	ArrayAdapter<String> brandAdapter = null;
    	
    	if(getActivity() instanceof MultiOrderActivity) {
        	MultiOrderActivity activity = (MultiOrderActivity) getActivity();
        	ArrayList<String> list = activity.onGetModel()
        			.getCurrentPageSequence().get(1).getData().getStringArrayList(Page.SIMPLE_DATA_KEY);
        	
        	ArrayList<String> nList = new ArrayList<String>();
        	nList.add("");
        	for(String s : list)
        		nList.add(s);
        	
        	brandAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, nList);
        	brandAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        	brandSpinner.setAdapter(brandAdapter);
    	
    	}
    	
    	if(index > -1) {
    		String[] vNos = mPage.getData().getStringArray(MultiOrderDetailsPage.VEHICLE_NOS_ARRAY);
    		String[] trips = mPage.getData().getStringArray(MultiOrderDetailsPage.TRIPS_ARRAY);
    		String[] tonnageArray = mPage.getData().getStringArray(MultiOrderDetailsPage.TONNAGE_ARRAY);
    		String[] brands = mPage.getData().getStringArray(MultiOrderDetailsPage.BRANDS_ARRAY);
    		
    		Boolean hasSelection = false;
    		
    		try {
    			
    			vehicleNoView.setText(vNos[index]);
    			vehicleTonnageView.setText(tonnageArray[index]);
    			tripsView.setText(trips[index]);
    			
    			if(brandAdapter != null) {
    				
    				String b = brands[index];
    				for (int x = 0; x < brandAdapter.getCount(); x++) {
    					
    					if(brandAdapter.getItem(x).equals(b)) {
    						brandSpinner.setSelection(x);
    						hasSelection = true;
    					}
    					
    				}
    			}
    			
    		} catch (Exception e) {}
    		
    		if(hasSelection)
    			vehicleNosContainer.addView(vehicleRow);
    	} else {
    		vehicleNosContainer.addView(vehicleRow);
    	}
    	
    	addVehicleRowListeners(vehicleNoView);
    	addVehicleRowListeners(tripsView);
    	addVehicleRowListeners(vehicleTonnageView);
    	brandSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				save();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});
    	
	}
	
	protected void addVehicleRowListeners(EditText view) {
		view.addTextChangedListener(new TextWatcher() {
    		
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			
			@Override
			public void afterTextChanged(Editable s) {
				save();
			}
		});
	}
	
	protected void save() {
		saveVehicleNos();
		saveBrands();
		saveTrips();
		saveTonnage();
		mPage.notifyDataChanged();
	}
	
	protected void saveTrips() {

		String trips[] = new String[vehicleNosContainer.getChildCount()];
		
		for(int i = 0; i < vehicleNosContainer.getChildCount(); i++) {
			
			LinearLayout vRow = (LinearLayout) vehicleNosContainer.getChildAt(i);
			
			EditText t = (EditText) vRow.findViewWithTag("trips");
			
			trips[i] = t.getText().toString();
			
		}
		
		mPage.getData().putStringArray(MultiOrderDetailsPage.TRIPS_ARRAY, trips);
	}
	
	protected void saveTonnage() {
		
		String tonnage[] = new String[vehicleNosContainer.getChildCount()];
		
		for(int i = 0; i < vehicleNosContainer.getChildCount(); i++) {
			
			LinearLayout vRow = (LinearLayout) vehicleNosContainer.getChildAt(i);
			
			EditText t = (EditText) vRow.findViewWithTag("tonnage");
			
			tonnage[i] = t.getText().toString();
			
		}
		
		mPage.getData().putStringArray(MultiOrderDetailsPage.TONNAGE_ARRAY, tonnage);
	}
	
	protected void saveBrands() {
		
		String brands[] = new String[vehicleNosContainer.getChildCount()];
		
		for(int i = 0; i < vehicleNosContainer.getChildCount(); i++) {
			
			LinearLayout vRow = (LinearLayout) vehicleNosContainer.getChildAt(i);
			
			Spinner bSpinner = (Spinner) vRow.findViewWithTag("brand");
			
			brands[i] = bSpinner.getSelectedItem().toString();
			
		}
		
		mPage.getData().putStringArray(MultiOrderDetailsPage.BRANDS_ARRAY, brands);
	}
	
	protected void saveVehicleNos() {
		
		String vNos[] = new String[vehicleNosContainer.getChildCount()];
		
		for(int i = 0; i < vehicleNosContainer.getChildCount(); i++) {
			
			LinearLayout vRow = (LinearLayout) vehicleNosContainer.getChildAt(i);
			
			EditText vNoView = (EditText) vRow.findViewWithTag("vehicleNo");
			
			vNos[i] = vNoView.getText().toString();
			
		}
		
		mPage.getData().putStringArray(MultiOrderDetailsPage.VEHICLE_NOS_ARRAY, vNos);
	}
	
	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
        if (!(activity instanceof PageFragmentCallbacks)) {
            throw new ClassCastException("Activity must implement PageFragmentCallbacks");
        }
        
        mCallbacks = (PageFragmentCallbacks) activity;
    }
	
    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }
    
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        
    }
    
    protected TextWatcher getTextWatcher(final String name) {
    	
    	return new TextWatcher() {
    		
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mPage.getData().putString(name,
                        (charSequence != null) ? charSequence.toString() : "");
            }
            
            @Override
            public void afterTextChanged(Editable editable) {
                mPage.getData().putString(name,
                        (editable != null) ? editable.toString() : null);
                mPage.notifyDataChanged();
            }
        };
    }
    
    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        
        if (lpoNoView != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            if (!menuVisible) {
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            }
        }
    }
}