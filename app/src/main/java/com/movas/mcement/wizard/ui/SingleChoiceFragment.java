package com.movas.mcement.wizard.ui;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import com.movas.mservice12.R;
import com.movas.mcement.wizard.model.Page;
import com.movas.mcement.wizard.model.SingleFixedChoicePage;
import com.movas.mservice12.SingleOrderActivity;

public class SingleChoiceFragment extends ListFragment {
	
	private static final String ARG_KEY = "key";
	
    private PageFragmentCallbacks mCallbacks;
    private List<String> mChoices;
    private String mKey;
    private Page mPage;
    
    ListView listView;

    public static SingleChoiceFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);

        SingleChoiceFragment fragment = new SingleChoiceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public SingleChoiceFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = mCallbacks.onGetPage(mKey);
        
        SingleFixedChoicePage fixedChoicePage = (SingleFixedChoicePage) mPage;
        mChoices = new ArrayList<String>();
        for (int i = 0; i < fixedChoicePage.getOptionCount(); i++) {
            mChoices.add(fixedChoicePage.getOptionAt(i));
        }
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	
    	View rootView = inflater.inflate(R.layout.fragment_page, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());
        
        listView = (ListView) rootView.findViewById(android.R.id.list);
        setListAdapter(new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_single_choice,
                android.R.id.text1,
                mChoices));
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        
        // Pre-select currently selected item.
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                String selection = mPage.getData().getString(Page.SIMPLE_DATA_KEY);
                for (int i = 0; i < mChoices.size(); i++) {
                    if (mChoices.get(i).equals(selection)) {
                        listView.setItemChecked(i, true);
                        break;
                    }
                }
            }
        });
        
        return rootView;
    }
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
        if (!(activity instanceof PageFragmentCallbacks)) {
            throw new ClassCastException("Activity must implement PageFragmentCallbacks");
        }
        
        mCallbacks = (PageFragmentCallbacks) activity;
        
        if(activity instanceof SingleOrderActivity) {
//        	((SingleOrderActivity)activity).addObjectChangeListener(new ObjectChangeListener() {
//				
//				@Override
//				public void onChange(String name, Object value) {
//					
//					//Toast.makeText(getActivity(), value.toString(), Toast.LENGTH_LONG).show();
//					
//					if(listView != null) {
//						
//						String v = ((String)value);
//						
//						if(v.equals("Athi-River")) {
//							
//							setListAdapter(new ArrayAdapter<String>(getActivity(),
//					                android.R.layout.simple_list_item_single_choice,
//					                android.R.id.text1,
//					                Arrays.asList(Constants.ATHI_RIVER_PRODUCTS)));
//							
//						} else if(v.equals("Mombasa")) {
//							
//							setListAdapter(new ArrayAdapter<String>(getActivity(),
//					                android.R.layout.simple_list_item_single_choice,
//					                android.R.id.text1,
//					                Arrays.asList(Constants.MOMBASA_PRODUCTS)));
//							
//						} else if(v.equals("Kitui Road")) {
//							
//							setListAdapter(new ArrayAdapter<String>(getActivity(),
//					                android.R.layout.simple_list_item_single_choice,
//					                android.R.id.text1,
//					                Arrays.asList(Constants.KITUI_RD_PRODUCTS)));
//						}
//					}
//				}
//			});
        }
    }
    
    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }
    
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        mPage.getData().putString(Page.SIMPLE_DATA_KEY,
                getListAdapter().getItem(position).toString());
        mPage.notifyDataChanged();
    }
}