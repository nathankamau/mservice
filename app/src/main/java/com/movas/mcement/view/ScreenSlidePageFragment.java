package com.movas.mcement.view;

import com.movas.mservice12.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ScreenSlidePageFragment extends Fragment {
	
    public static final String ARG_PAGE = "page";
    
    private int mPageNumber;
    
    public static ScreenSlidePageFragment create(int pageNumber) {
        ScreenSlidePageFragment fragment = new ScreenSlidePageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNumber);
        fragment.setArguments(args);
        return fragment;
    }
    
    public ScreenSlidePageFragment() {
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNumber = getArguments().getInt(ARG_PAGE);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

    	ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_screen_slide_1, container, false);
    	
    	switch (mPageNumber) {
		case 0:
			rootView = (ViewGroup) inflater.inflate(R.layout.fragment_screen_slide_1, container, false);
			break;
		case 1:
			rootView = (ViewGroup) inflater.inflate(R.layout.fragment_screen_slide_2, container, false);
			break;
		case 2:
			rootView = (ViewGroup) inflater.inflate(R.layout.fragment_screen_slide_3, container, false);
			break;
		case 3:
			rootView = (ViewGroup) inflater.inflate(R.layout.fragment_screen_slide_4, container, false);
			break;
		case 4:
			rootView = (ViewGroup) inflater.inflate(R.layout.fragment_screen_slide_5, container, false);
			break;
		default:
			break;
		}

        return rootView;
    }
    
    public int getPageNumber() {
        return mPageNumber;
    }
}