package com.movas.mcement.data;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;
import com.movas.mservice12.MovaPay;

import cz.msebera.android.httpclient.HttpEntity;

public class HttpClient {
	
	static HttpClient instance;
    static AsyncHttpClient client;
	
	public static HttpClient instance() {
		if (instance == null)
			instance = new HttpClient();
		return instance;
	}

	public AsyncHttpClient getAsyncClient() {

        if(client == null) {

            client = new AsyncHttpClient();
            Context mContext = MovaPay.get().getApplicationContext();
            PersistentCookieStore myCookies = new PersistentCookieStore(mContext);
            client.setCookieStore(MovaPay.get().getCookieStore());
            client.setTimeout(100);
        }

		return client;
	}
	
	public void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
		getAsyncClient().get(getAbsoluteUrl(url), params, responseHandler);
	}
	
	public void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
		getAsyncClient().post(getAbsoluteUrl(url), params, responseHandler);
	}
	
	public void post(Context context, String url, HttpEntity entity, String contentType, AsyncHttpResponseHandler responseHandler) {
		getAsyncClient().post(context, getAbsoluteUrl(url), entity, contentType, responseHandler);
	}
	
	public String getAbsoluteUrl(String relativeUrl) {
		return UrlUtils.baseURL + "/" + relativeUrl;
	}
}