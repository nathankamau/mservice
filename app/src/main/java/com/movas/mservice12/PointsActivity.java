package com.movas.mservice12;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.movas.mcement.data.PointsService;
import com.movas.mcement.data.PointsServiceImpl;
import com.movas.mcement.helper.DialogHelper;
import com.movas.mcement.helper.ErrorHandler;
import com.movas.mcement.util.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;

public class PointsActivity extends ActionBarActivity {


    TextView balance;


    LinearLayout balance_container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_points);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        balance_container = (LinearLayout) findViewById(R.id.balance_container);

        balance = (TextView) findViewById(R.id.balance);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                PointsActivity.this.onBackPressed();
          }
        });

        loadPoints();
    }

    private void loadPoints() {

        DialogHelper.showProgressDialog(this, getString(R.string.please_wait), false);

        PointsService service = new PointsServiceImpl();

        SharedPreferences prefs = getApplication().getSharedPreferences(Constants.MovaPay, Context.MODE_PRIVATE);
        String accountNumber =prefs.getString(Constants.CUSTOMER_ID,"");
        String token = prefs.getString("token", "");

        try {
            service.getPoints(accountNumber,token,new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);

                    DialogHelper.hideProgressDialog();

                    try {

                        Integer status = response.getInt("status");

                        if(status == 0) {

                            int points = response.getInt("points");

                            balance_container.setVisibility(View.VISIBLE);
                            balance.setText("" + points);



                        } else {
                            ErrorHandler.handleError(PointsActivity.this, new Throwable(response.getString("message") ));
                        }

                    } catch (JSONException e1) {}
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    DialogHelper.hideProgressDialog();
                    ErrorHandler.handleError(PointsActivity.this, throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);

                    DialogHelper.hideProgressDialog();
                    ErrorHandler.handleError(PointsActivity.this, throwable);
                }

            });
        } catch (UnsupportedEncodingException | JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_refresh, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_refresh) {

            balance_container.setVisibility(View.GONE);
            loadPoints();
            return true;


        }
        return super.onOptionsItemSelected(item);
    }
}