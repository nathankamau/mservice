package com.movas.mservice12;

import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import org.json.JSONObject;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.movas.mcement.data.AuthService;
import com.movas.mcement.data.AuthServiceImpl;
import com.movas.mcement.helper.DialogHelper;
import com.movas.mcement.helper.ErrorHandler;
import com.movas.mcement.util.Constants;

import cz.msebera.android.httpclient.Header;
import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.annotations.MaxLength;
import eu.inmite.android.lib.validations.form.annotations.MinLength;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.os.Bundle;

public class ChangePinActivity extends ActionBarActivity {

	@NotEmpty(messageId = R.string.validation_current_pin, order = 1)
	@MinLength(messageId = R.string.validation_pin_length, order = 2, value = 4)
	@MaxLength(messageId = R.string.validation_pin_length, order = 3, value = 4)
	EditText currentPinView;
	
	@NotEmpty(messageId = R.string.validation_new_pin, order = 4)
	@MinLength(messageId = R.string.validation_pin_length, order = 5, value = 4)
	@MaxLength(messageId = R.string.validation_pin_length, order = 6, value = 4)
	EditText newPinView;
	
	@NotEmpty(messageId = R.string.validation_new_pin_repeat, order = 7)
	@MinLength(messageId = R.string.validation_pin_length, order = 8, value = 4)
	@MaxLength(messageId = R.string.validation_pin_length, order = 9, value = 4)
	EditText newPinRepeatView;
	
	Button changeBtn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_change_pin);
		
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		toolbar.setNavigationOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				ChangePinActivity.this.onBackPressed();
			}
		});

		currentPinView = (EditText) findViewById(R.id.currentPinView);
		newPinView = (EditText) findViewById(R.id.newPinView);
		newPinRepeatView = (EditText) findViewById(R.id.newPinRepeatView);
		
		changeBtn = (Button) findViewById(R.id.changeBtn);
		
		changeBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (isValid())
					changePin();
			}
		});
	}
	
	protected void changePin() {
		
		DialogHelper.showProgressDialog(this, getString(R.string.please_wait), false);

		SharedPreferences prefs = getApplication().getSharedPreferences(Constants.MovaPay, Context.MODE_PRIVATE);
		String token = prefs.getString("token", "");

		AuthService service = new AuthServiceImpl();
		try {
			service.changePin(token, currentPinView.getText().toString(), newPinView.getText().toString(), newPinRepeatView.getText().toString(), new JsonHttpResponseHandler() {

				@Override
				public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
					super.onSuccess(statusCode, headers, response);

					System.out.print(".........................changePin..............................");
					DialogHelper.hideProgressDialog();
					
					try {
						
						String status = response.getString("status");
						
						if("0".equals(status)) {
							Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.pin_change_success_message) , Toast.LENGTH_LONG);
							toast.setGravity(Gravity.TOP, 0, 0);
							toast.show();
							finish();
							
						} else {
							ErrorHandler.handleError(ChangePinActivity.this, new Throwable(response.getString("message")));
						}
						
					} catch (JSONException e1) {}
				}
				
				@Override
				public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
					super.onFailure(statusCode, headers, responseString, throwable);
					
					DialogHelper.hideProgressDialog();
					ErrorHandler.handleError(ChangePinActivity.this, throwable);
				}
				
				@Override
				public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
					super.onFailure(statusCode, headers, throwable, errorResponse);
					
					DialogHelper.hideProgressDialog();
					ErrorHandler.handleError(ChangePinActivity.this, throwable);
				}
			});
			
		} catch (UnsupportedEncodingException | JSONException e) {
			e.printStackTrace();
		}
	}

	protected Boolean isValid() {
		
		Boolean isValid = FormValidator.validate(this, new SimpleErrorPopupCallback(this));

		return isValid;
	}
}