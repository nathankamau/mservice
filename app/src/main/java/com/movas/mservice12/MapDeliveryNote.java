package com.movas.mservice12;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.movas.mcement.helper.DialogHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class MapDeliveryNote extends Activity {

    private static final String TAG = MapDeliveryNote.class.getSimpleName();
    private String tag_string_req = "string_req";

    private EditText urlText;

    private TextView textView;

    private Button requestBtn;

    private ProgressDialog dialog;

    private boolean success = true;


    public static String lat, lon, speed, milage, car_id, itemDescription, truckReg;

    public static int tonnage;

    String url = "http://80.241.222.62:8787/api/tracking/order/", editCode;

    ArrayList<HashMap<String, String>> oslist = new ArrayList<HashMap<String, String>>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_delivery_note);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        textView = (TextView) findViewById(R.id.myText);

        requestBtn = (Button) findViewById(R.id.requestBtn);


        requestBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                urlText = (EditText) findViewById(R.id.myUrl);

                if (urlText.getText().toString().trim().equals("")) {
                    urlText.setError("Delivery note should note be empty");
                } else {
                    editCode = urlText.getText().toString().trim();
                    getData();

                }


            }
        });
    }

    public void getData() {

        DialogHelper.showProgressDialog(MapDeliveryNote.this, getString(R.string.please_wait), false);
        //String dataUrl = "http://104.154.249.14/SMART_MOBILE/usr/fetchHospitalByLocation.action?rgId=1&usrId=186";
        //String data ="http://80.241.222.62:8787/api/tracking/order/"+editCode";

        StringRequest stringRequest = new StringRequest("http://80.241.222.62:8787/api/tracking/order/" + editCode,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showJSON(response);
                        DialogHelper.hideProgressDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        DialogHelper.hideProgressDialog();
                        AlertDialog.Builder builder = new AlertDialog.Builder(MapDeliveryNote.this);
                        builder.setTitle("error".toUpperCase());
                        builder.setCancelable(true);
                        //builder.setIcon(R.mipmap.ic_launcher);
                        builder.setMessage("Server connection timed out");
                        builder.show();

                        //Toast.makeText(MapDeliveryNote.this, error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    // AppController.getInstance().getRequestQueue().getCache().invalidate(url, true);\\
    public void showJSON(String response) {
        Log.i("Response", response);

        try {
            JSONObject jsonObject = new JSONObject(response);
            itemDescription = jsonObject.getString("itemDescription");
            truckReg = jsonObject.getString("truckReg");

            //JSONArray data = jsonObject.getJSONArray("jsonData");


            JSONArray data = jsonObject.getJSONArray("gps");
            for (int j = 0; j < data.length(); j++) {
                JSONObject obj = data.getJSONObject(j);

                /*lon = obj.getString("hospLatitude");
                lat = obj.getString("hospLongitude");*/


                lon = obj.getString("latitude");
                lat = obj.getString("longitude");
                speed = obj.getString("speed");
                milage = obj.getString("milage");
                car_id = obj.getString("car_id");


                Intent intent = new Intent(MapDeliveryNote.this, MapsActivity.class);
                startActivity(intent);

                HashMap<String, String> map = new HashMap<String, String>();

                map.put("longitude", obj.getString("longitude"));
                map.put("latitude", obj.getString("latitude"));
                map.put("speed", obj.getString("speed"));
                map.put("speed", obj.getString("car_id"));
                map.put("fuel", obj.getString("fuel"));
                map.put("status", obj.getString("status"));
                map.put("mileage", obj.getString("mileage"));
                map.put("acc", obj.getString("acc"));
                map.put("terminal_key", obj.getString("terminal_key"));


                oslist.add(map);

                lon = oslist.get(data.length()).get("longitude")
                        .toString();

                lat = oslist.get(data.length()).get("longitude")
                        .toString();

            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

};



