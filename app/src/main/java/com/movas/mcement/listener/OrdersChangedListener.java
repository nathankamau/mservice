package com.movas.mcement.listener;

public interface OrdersChangedListener {

	public void onOrdersChanged();
}
