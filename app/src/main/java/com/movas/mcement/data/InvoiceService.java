package com.movas.mcement.data;

import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import com.loopj.android.http.JsonHttpResponseHandler;

public interface InvoiceService {

	public void requestInvoice(String token, String type, String text, String id, JsonHttpResponseHandler handler)
			throws UnsupportedEncodingException, JSONException;
}