package com.movas.mcement.view;

import com.movas.mservice12.R;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnKeyListener;
import android.view.KeyEvent;

public class ErrorAlert implements OnKeyListener{

	  private final Context mContext;

	  public ErrorAlert(final Context context) {
	    mContext = context;
	  }

	  public void showErrorDialog(final String title, final String message) {
	    AlertDialog aDialog = new AlertDialog.Builder(mContext).setMessage(message).setTitle(title)
	        .setNeutralButton(mContext.getString(R.string.close), new OnClickListener() {
	          public void onClick(final DialogInterface dialog, final int which) {
	        	  
//	        	  if(mContext.getString(R.string.account_locked).equalsIgnoreCase(message)) {
//	        		  if(mContext instanceof Activity) {
//	        			  
//	        			  Intent intent = new Intent(mContext, CustomerRegistrationActivity.class);
//	        			  //ActivityUtils.startActivity(intent, (Activity) mContext);
//	        			  ((Activity) mContext).finish();
//	        		  }
//	        	  }
	          }
	        }).create();
	    aDialog.setOnKeyListener(this);
	    aDialog.show();
	  }

	  @Override
	  public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
		  if(keyCode == KeyEvent.KEYCODE_BACK){}
		  return true;
	  }
}