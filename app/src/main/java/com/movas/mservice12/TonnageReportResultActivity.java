package com.movas.mservice12;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.movas.mcement.adapter.TonnageReportAdapter;
import com.movas.mcement.entity.TonnageReport;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.os.Bundle;
import android.view.View;

public class TonnageReportResultActivity extends ActionBarActivity {

	RecyclerView list;
	TonnageReportAdapter mAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tonnage_report_result);
		
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
        	
            @Override
            public void onClick(View v) {
                TonnageReportResultActivity.this.onBackPressed();
            }
        });

        list = (RecyclerView) findViewById(R.id.list);
        list.setLayoutManager(new LinearLayoutManager(this));
        
        mAdapter = new TonnageReportAdapter(new ArrayList<TonnageReport>(), R.layout.row_tonnage_report, this);
        list.setAdapter(mAdapter);

        try {

			JSONObject jsonObject = new JSONObject(getIntent().getStringExtra("data"));
			
			if(jsonObject.has("data")) {
				
				List<TonnageReport> data = new ArrayList<>();
				
				JSONArray arr = jsonObject.getJSONArray("data");

                for(int i = 0; i < arr.length(); i++) {
					
					JSONObject row = arr.getJSONObject(i);
					
					TonnageReport tonnageReport = new TonnageReport();
			        tonnageReport.setName(row.getString("name"));
			        tonnageReport.setTonnage(row.getString("tonnage"));

			        data.add(tonnageReport);
				}

                mAdapter.addItems(data);
			}

		} catch (JSONException e) {}
	}
}
