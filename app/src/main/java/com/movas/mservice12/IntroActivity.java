package com.movas.mservice12;

import com.movas.mcement.view.ScreenSlidePageFragment;
import com.movas.mcement.wizard.ui.StepPagerStrip;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.content.Intent;
import android.os.Bundle;

public class IntroActivity extends FragmentActivity {
	
	private static final int NUM_PAGES = 5;
	
    private ViewPager mPager;
    
    private PagerAdapter mPagerAdapter;
    
    private StepPagerStrip mStepPagerStrip;
    
    private Button continueBtn;
    
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
            	mStepPagerStrip.setCurrentPage(position);
               // supportInvalidateOptionsMenu();
            }
        });
        
        mStepPagerStrip = (StepPagerStrip) findViewById(R.id.strip);
        mStepPagerStrip.setOnPageSelectedListener(new StepPagerStrip.OnPageSelectedListener() {
            @Override
            public void onPageStripSelected(int position) {
                position = Math.min(mPagerAdapter.getCount() - 1, position);
                if (mPager.getCurrentItem() != position) {
                    mPager.setCurrentItem(position);
                }
            }
        });
        
        mStepPagerStrip.setPageCount(mPagerAdapter.getCount());
        
        continueBtn = (Button) findViewById(R.id.continueBtn);
        continueBtn.setOnClickListener(new View.OnClickListener() {
        	
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(IntroActivity.this, HomeActivity.class);
				startActivity(intent);
				finish();
			}
		});
    }
	
	private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }
        
        @Override
        public Fragment getItem(int position) {
            return ScreenSlidePageFragment.create(position);
        }
        
        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }
}