package com.movas.mcement.view;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.movas.mcement.util.Constants;
import com.movas.mservice12.R;
import com.movas.mcement.data.OrderService;
import com.movas.mcement.data.OrderServiceImpl;
import com.movas.mcement.data.StatementService;
import com.movas.mcement.data.StatementServiceImpl;
import com.movas.mcement.helper.DialogHelper;
import com.movas.mcement.helper.ErrorHandler;
import com.movas.mcement.listener.DateChangeAdapter;
import com.movas.mservice12.FullStatementActivity;
import com.movas.mservice12.TonnageReportActivity;
import com.movas.mservice12.TonnageReportResultActivity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import cz.msebera.android.httpclient.Header;

public class DateFragment extends Fragment {
	
	DatePicker datePicker;
	Button nextBtn;
	Button prevBtn;
	TextView dateLabel;
	Date fromDate;
	
	int position = 0;
	
	public static DateFragment init(int val) {
		DateFragment fragment = new DateFragment();
		
		Bundle args = new Bundle();
		args.putInt("position", val);
		fragment.setArguments(args);
		return fragment;
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.date_view, container, false);
	}
	
	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		dateLabel = (TextView) view.findViewById(R.id.dateLabel);
		datePicker = (DatePicker) view.findViewById(R.id.datePicker);
		nextBtn = (Button) view.findViewById(R.id.nextBtn);
		prevBtn = (Button) view.findViewById(R.id.prevBtn);
		
        if(android.os.Build.VERSION.SDK_INT >= 11) {
        	try {
        		Method m = datePicker.getClass().getMethod("setCalendarViewShown", boolean.class);
        		m.invoke(datePicker, false);//
        	} catch(Exception e) {
        		e.printStackTrace();
        	}
        }
        
        if(getArguments().containsKey("position")) {
        	position = getArguments().getInt("position");
        	
        	if(position == 0)
        		dateLabel.setText(getString(R.string.select_start_date));
        	else if(position == 1) {
        		dateLabel.setText(getString(R.string.select_end_date));
        		prevBtn.setVisibility(View.VISIBLE);
        		nextBtn.setText(getString(R.string.request));
        	}
        }
        
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
	        setMaxDate();
	    }
        
        prevBtn.setOnClickListener(new View.OnClickListener() {
        	
			@Override
			public void onClick(View v) {
				if(getActivity() instanceof FullStatementActivity) {
					FullStatementActivity activity = (FullStatementActivity) getActivity();
					activity.setCurrentItem(position - 1);
				} else if(getActivity() instanceof TonnageReportActivity) {
					TonnageReportActivity activity = (TonnageReportActivity) getActivity();
					activity.setCurrentItem(position - 1);
				}
			}
		});
                
        nextBtn.setOnClickListener(new View.OnClickListener() {
        	
			@Override
			public void onClick(View v) {
				
				if(position == 0) {
					if(getActivity() instanceof FullStatementActivity) {
						
						FullStatementActivity activity = (FullStatementActivity) getActivity();
						activity.setCurrentItem(position + 1);
						
						if(activity.adapter != null) {
							activity.adapter.onDateChange(getDateFromDatePicker(datePicker));
						}
						
					} else if(getActivity() instanceof TonnageReportActivity) {
						
						TonnageReportActivity activity = (TonnageReportActivity) getActivity();
						activity.setCurrentItem(position + 1);
						
						if(activity.adapter != null) {
							activity.adapter.onDateChange(getDateFromDatePicker(datePicker));
						}
						
					}
				} else {
					
					if(getActivity() instanceof FullStatementActivity)
						requestStatement();
					else if(getActivity() instanceof TonnageReportActivity)
						requestTonnageReport();
				}
			}
		});
        
        if(position == 1) {
        	if(getActivity() instanceof FullStatementActivity) {
            	
        		FullStatementActivity activity = (FullStatementActivity) getActivity();
            	activity.addDateChangeListener(new DateChangeAdapter() {
            		
    				@Override
    				public void onDateChange(Date date) {
    					fromDate = date;
    					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
    				        //setMinDate(date);
    				        setMaxDate();
    				    }
    				}
    			});
            } else if(getActivity() instanceof TonnageReportActivity) {
            	
            	TonnageReportActivity activity = (TonnageReportActivity) getActivity();
            	activity.addDateChangeListener(new DateChangeAdapter() {
            		
    				@Override
    				public void onDateChange(Date date) {
    					fromDate = date;
    					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
    				        //setMinDate(date);
    				        setMaxDate();
    				    }
    				}
    			});
            }
        }
	}
	
	protected void requestTonnageReport() {
		
		SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd");			
		final String fDate = dFormat.format(fromDate);
		final String tDate = dFormat.format(getDateFromDatePicker(datePicker));

        DialogHelper.showProgressDialog(getActivity(), getString(R.string.please_wait), false);

		OrderService service = new OrderServiceImpl();

		SharedPreferences prefs = getActivity().getApplication().getSharedPreferences(Constants.MovaPay, Context.MODE_PRIVATE);
		String token = prefs.getString("token", "");
		
		try {
            
			service.loadTonnageHistory(token, fDate, tDate, new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);

                    DialogHelper.hideProgressDialog();
					
					Intent intent = new Intent(getActivity(), TonnageReportResultActivity.class);
					intent.putExtra("from_date", fDate);
					intent.putExtra("to_date", tDate);
					intent.putExtra("data", response.toString());
					
					getActivity().finish();
					getActivity().startActivity(intent);
				}

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    DialogHelper.hideProgressDialog();
                    ErrorHandler.handleError(getActivity(), throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);

                    DialogHelper.hideProgressDialog();
                    ErrorHandler.handleError(getActivity(), throwable);
                }
			});
		} catch (UnsupportedEncodingException | JSONException e) {
			DialogHelper.hideProgressDialog();
		}
	}
	
	protected void requestStatement() {
		
		SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd");
       // System.out.println(dt1.format(date));
		
		String fDate = dFormat.format(fromDate);
		String tDate = dFormat.format(getDateFromDatePicker(datePicker));
		

		DialogHelper.showProgressDialog(getActivity(), getString(R.string.please_wait), false);
		
		StatementService service = new StatementServiceImpl();

		SharedPreferences prefs = getActivity().getApplication().getSharedPreferences(Constants.MovaPay, Context.MODE_PRIVATE);
		String token = prefs.getString("token", "");
		
		try {
			service.requestFullStatement(token, fDate, tDate, new JsonHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
					super.onSuccess(statusCode, headers, response);
					
					DialogHelper.hideProgressDialog();

					try {

						Integer status = response.getInt("status");

						if(status == 0) {

							Toast toast = Toast.makeText(getActivity(), getString(R.string.full_statement_success_message) , Toast.LENGTH_LONG);
							toast.setGravity(Gravity.TOP, 0, 0);
							toast.show();
							getActivity().finish();
							
						} else {
							
							String msg = response.getString("message");							
							ErrorHandler.handleError(getActivity(), new Throwable(msg));
						}
						
					} catch (JSONException e1) {e1.printStackTrace();}
					
				}
				
				@Override
				public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
					super.onFailure(statusCode, headers, responseString, throwable);
					DialogHelper.hideProgressDialog();
					ErrorHandler.handleError(getActivity(), throwable);
				}
				
				@Override
				public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
					super.onFailure(statusCode, headers, throwable, errorResponse);
					
					DialogHelper.hideProgressDialog();
					ErrorHandler.handleError(getActivity(), throwable);
				}
			});
		} catch (UnsupportedEncodingException | JSONException e) {
			DialogHelper.hideProgressDialog();
		}
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	protected void setMinDate(Date date) {
		
		if(new Date().after(date)) {
			try {
				datePicker.setMinDate(date.getTime());
			} catch(Exception e) {}
		}
	}
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	protected void setMaxDate() {
		datePicker.setMaxDate(new Date().getTime() - 1000);
	}
	
	public void setDateString(String label) {
		dateLabel.setText(label);
	}
	
	public static java.util.Date getDateFromDatePicker(DatePicker datePicker){
	    int day = datePicker.getDayOfMonth();
	    int month = datePicker.getMonth();
	    int year =  datePicker.getYear();
	    
	    Calendar calendar = Calendar.getInstance();
	    calendar.set(year, month, day);

	    return calendar.getTime();
	}
}