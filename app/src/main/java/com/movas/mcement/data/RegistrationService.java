package com.movas.mcement.data;

import java.io.UnsupportedEncodingException;

import org.json.JSONException;

import com.loopj.android.http.JsonHttpResponseHandler;

public interface RegistrationService {

	public void isRegistered(String phone, JsonHttpResponseHandler handler) throws JSONException, UnsupportedEncodingException;
	
	public void register(String firstName, String lastName, String middleName, String phone, String geoLocation, JsonHttpResponseHandler handler) throws JSONException, UnsupportedEncodingException;
}