package com.movas.mservice12;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.movas.mcement.data.InvoiceService;
import com.movas.mcement.data.InvoiceServiceImpl;
import com.movas.mcement.helper.DialogHelper;
import com.movas.mcement.helper.ErrorHandler;
import com.movas.mcement.util.Constants;
import com.movas.mcement.util.ToastUtil;

import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import cz.msebera.android.httpclient.Header;

public class RequestInvoiceActivity extends ActionBarActivity {

    Button requestBtn;

    Spinner requestFieldSpinner;

    static EditText  textView;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_invoice);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestInvoiceActivity.this.onBackPressed();
            }
        });

        requestFieldSpinner = (Spinner) findViewById(R.id.requestFieldSpinner);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.request_fields, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        requestFieldSpinner.setAdapter(adapter);

        requestFieldSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                textView.setText("");
                textView.setHint(requestFieldSpinner.getSelectedItem().toString());

                if (position == 2) {
                    //show date picker
                    DialogFragment datePicker = new DatePickerFragment();
                    datePicker.setCancelable(false);
                    datePicker.show(getSupportFragmentManager(), "datePicker");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        textView = (EditText) findViewById(R.id.textView);

        requestBtn = (Button) findViewById(R.id.requestBtn);
        requestBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Boolean isValid = true;
                String msg = "";

                if (requestFieldSpinner.getSelectedItemPosition() == 2 && !"".equals(textView.getText().toString().trim())) {

                    if (!isDateValid(textView.getText().toString().trim())) {
                        textView.setError("Date is Invalid. Date format is yyyy-MM-dd");
                        textView.requestFocus();
                        isValid = false;
                    }

                } else if ("".equals(textView.getText().toString().trim())) {

                    String field = requestFieldSpinner.getSelectedItem().toString();
                    isValid = false;
                    msg = field + " is required.";
                    textView.setError(msg);
                    textView.requestFocus();
                }

                if (isValid)
                    sendRequest();
            }
        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    protected void sendRequest() {

        SharedPreferences prefs = getApplication().getSharedPreferences(Constants.MovaPay, Context.MODE_PRIVATE);
        String uuid = prefs.getString(Constants.CUSTOMER_UUID, "");
        String customerID = prefs.getString(Constants.CUSTOMER_ID, "");
        String token = prefs.getString("token", "");

        hideKeyBoard();
        DialogHelper.showProgressDialog(RequestInvoiceActivity.this, getString(R.string.requesting), false);
        InvoiceService service = new InvoiceServiceImpl();

        String type = (requestFieldSpinner.getSelectedItemPosition() + 1) + "";
        String text = textView.getText().toString().trim();

        try {

            service.requestInvoice(token, type, text, uuid, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);

                    DialogHelper.hideProgressDialog();

                    try {

                        Integer status = response.getInt("status");

                        if (status == 0) {

                            ToastUtil.makeText(getApplicationContext(), getString(R.string.invoice_request_success_msg), Toast.LENGTH_LONG).show();
                            RequestInvoiceActivity.this.finish();

                        } else {
                            ErrorHandler.handleError(RequestInvoiceActivity.this, new Throwable(response.getString("message")));
                        }

                    } catch (JSONException e1) {
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    DialogHelper.hideProgressDialog();
                    ErrorHandler.handleError(RequestInvoiceActivity.this, throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);

                    DialogHelper.hideProgressDialog();
                    ErrorHandler.handleError(RequestInvoiceActivity.this, throwable);
                }

            });
        } catch (UnsupportedEncodingException | JSONException e) {
            e.printStackTrace();
        }
    }

  //  @Override
//    public void onStart() {
//        super.onStart();
//
//        // ATTENTION: This was auto-generated to implement the App Indexing API.
//        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        client.connect();
//        Action viewAction = Action.newAction(
//                Action.TYPE_VIEW, // TODO: choose an action type.
//                "RequestInvoice Page", // TODO: Define a title for the content shown.
//                // TODO: If you have web page content that matches this app activity's content,
//                // make sure this auto-generated web page URL is correct.
//                // Otherwise, set the URL to null.
//                Uri.parse("http://host/path"),
//                // TODO: Make sure this auto-generated app deep link URI is correct.
//                Uri.parse("android-app:// com.movas.mservice1/http/host/path")
//        );
//        AppIndex.AppIndexApi.start(client, viewAction);
//    }

//    @Override
//    public void onStop() {
//        super.onStop();
//
//        // ATTENTION: This was auto-generated to implement the App Indexing API.
//        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        Action viewAction = Action.newAction(
//                Action.TYPE_VIEW, // TODO: choose an action type.
//                "RequestInvoice Page", // TODO: Define a title for the content shown.
//                // TODO: If you have web page content that matches this app activity's content,
//                // make sure this auto-generated web page URL is correct.
//                // Otherwise, set the URL to null.
//                Uri.parse("http://host/path"),
//                // TODO: Make sure this auto-generated app deep link URI is correct.
//                Uri.parse("android-app://com.movas.mservice1/http/host/path")
//        );
//        AppIndex.AppIndexApi.end(client, viewAction);
//        client.disconnect();
//    }
    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            String d = day < 10 ? "0" + day : "" + day;
            String m = month < 10 ? "0" + (month + 1) : "" + (month + 1);
            textView.setText(year + "-" + m + "-" + d);
        }
    }

    private Boolean isDateValid(String d) {

        try {
            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
            f.setLenient(false);
            f.parse(d);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    private void hideKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(textView.getWindowToken(), 0);
    }
}