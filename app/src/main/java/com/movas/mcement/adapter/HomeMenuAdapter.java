package com.movas.mcement.adapter;

import com.movas.mservice12.R;
import com.movas.mcement.entity.HomeMenuItem;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class HomeMenuAdapter extends ArrayAdapter<HomeMenuItem> {

	public HomeMenuAdapter(Context context, String bgColor) {
		super(context, 0);
	}

	public HomeMenuAdapter(Context context) {
		super(context, 0);
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		HomeMenuItem menuItem = this.getItem(position);

		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.partial_home_menu_row, null);
		}

		TextView marker = (TextView) convertView.findViewById(R.id.marker);
		//marker.setBackground(getContext().getResources().getDrawable(R.drawable.rounded_edges));
		
//		Drawable drawable = getContext().getResources().getDrawable(R.drawable.rounded_edges);
//		//drawable.setColorFilter(new PorterDuffColorFilter(Color.parseColor("#AE6118"), PorterDuff.Mode.MULTIPLY));
//		
//		int aqua = getContext().getResources().getColor(R.color.aqua);
//		ColorFilter filter = new LightingColorFilter( aqua, aqua);
//		drawable.setColorFilter(filter);
//		
//		marker.setBackground(drawable);
		//marker.setBackgroundColor(getContext().getResources().getColor(R.color.aqua));
		
		TextView name = (TextView) convertView.findViewById(R.id.name);
		name.setText(menuItem.getTag());

		return convertView;
	}
}