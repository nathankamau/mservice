package com.movas.mservice12;

import com.movas.mcement.util.Constants;
import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class OrderDetailsActivity extends ActionBarActivity {
	
	Boolean isInitialOrder = false;
	
	@NotEmpty(messageId = R.string.validation_lponumber, order = 1)
	EditText lpoNoView;
	
	EditText vehicleNoView;
	
	Spinner pickupSpinner;
	
	Spinner transportMethodSpinner;
	
	EditText deliveryLocationView;
	
	TextView pickupLabel;
	
	Button nextBtn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_order_details);
		
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderDetailsActivity.this.onBackPressed();
            }
        });
        
		lpoNoView = (EditText) findViewById(R.id.lpoNoView);
		vehicleNoView = (EditText) findViewById(R.id.vehicleNoView);
		pickupSpinner = (Spinner) findViewById(R.id.pickupSpinner);
		pickupLabel = (TextView) findViewById(R.id.pickupLabel);
		transportMethodSpinner = (Spinner) findViewById(R.id.transportMethodSpinner);
		deliveryLocationView = (EditText) findViewById(R.id.deliveryLocationView);
		nextBtn = (Button) findViewById(R.id.nextBtn);
		
		ArrayAdapter<CharSequence> pickupAdapter = ArrayAdapter.createFromResource(this, R.array.pickup_locations, android.R.layout.simple_spinner_item);
		pickupAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		pickupSpinner.setAdapter(pickupAdapter);
		
		
		ArrayAdapter<CharSequence> transMethodAdapter = ArrayAdapter.createFromResource(this, R.array.transport_methods, android.R.layout.simple_spinner_item);
		transMethodAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		transportMethodSpinner.setAdapter(transMethodAdapter);
		
		transportMethodSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				deliveryLocationView.setText("");
				deliveryLocationView.setVisibility(position == 1 ? View.VISIBLE : View.GONE);
				vehicleNoView.setVisibility(position == 0 ? View.VISIBLE : View.GONE);
			}
			
			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});
		
		nextBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(isValid())
					//show confirmation
					//move next
					new AlertDialog.Builder(OrderDetailsActivity.this)
						.setMessage(getString(R.string.order_details_continue_confirmation))
						.setCancelable(false)
						.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								next();
							}
						})
						.setNegativeButton("No", null)
						.show();
			}
		});
		
		isInitialOrder = getIntent().getBooleanExtra(Constants.INITIAL_ORDER, false);
	}
	
	private Boolean isValid() {
		
		Boolean isValid = FormValidator.validate(this, new SimpleErrorPopupCallback(this));
		
		if(!isValid)
			return false;
		else {
			
			if(pickupSpinner.getSelectedItemPosition() == 0) {
				
				TextView v = (TextView) pickupSpinner.getSelectedView();
				v.setError(getString(R.string.validation_pickup_location));
				
				Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.validation_pickup_location), Toast.LENGTH_LONG);
				toast.setGravity(Gravity.TOP, 0, 0);
				toast.show();
				
				return false;
				
			} else {
				
				if(transportMethodSpinner.getSelectedItemPosition() != 0) {
					
					if(deliveryLocationView.getText().toString().trim().equals("")) {
						deliveryLocationView.setError(getString(R.string.validation_delivery_location));
						return false;
					}
				} else
					return isVehicleNoValid();
				
				return true;
			}
		}
	}
	
	protected Boolean isVehicleNoValid() {
		
		Boolean isValid = true;
		
		String vehicleNo = vehicleNoView.getText().toString();
		
		if(TextUtils.isEmpty(vehicleNo)) {
			vehicleNoView.setError(getString(R.string.validation_vehicleno));
			isValid = false;
		} else if(vehicleNo.length() < 5) {
			vehicleNoView.setError(getString(R.string.validation_vehicleno_length));
			isValid = false;
		} else if(!vehicleNo.matches("^(?=.*\\d)(?=.*[a-zA-Z]).{5,10}$")) {
			vehicleNoView.setError(getString(R.string.validation_vehicle_no_alphanumeric));
			isValid = false;
		}
		
		return isValid;
	}
	
	protected void next() {
		Intent intent = new Intent(OrderDetailsActivity.this, OrderItemActivity.class);
		intent.putExtra(Constants.INITIAL_ORDER, true);
		intent.putExtra(Constants.LPO_NO, lpoNoView.getText().toString());
		intent.putExtra(Constants.VEHICLE_NO, vehicleNoView.getText().toString());
		intent.putExtra(Constants.PICKUP_LOCATION, pickupSpinner.getSelectedItem().toString());
		
		intent.putExtra(Constants.TO_DELIVER, transportMethodSpinner.getSelectedItemPosition() == 0 ? "no" : "yes");
		intent.putExtra(Constants.DELIVERY_LOCATION, deliveryLocationView.getText().toString());
		
		startActivity(intent);
		finish();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.order_details, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}