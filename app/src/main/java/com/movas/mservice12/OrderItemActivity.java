package com.movas.mservice12;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import java.util.List;
import com.movas.mcement.listener.ObjectChangeListener;
import com.movas.mcement.util.Constants;
import com.movas.mcement.wizard.OrderWizardModel;
import com.movas.mcement.wizard.model.AbstractWizardModel;
import com.movas.mcement.wizard.model.ModelCallbacks;
import com.movas.mcement.wizard.model.Page;
import com.movas.mcement.wizard.model.order.OrderDetailsPage;
import com.movas.mcement.wizard.model.order.OrderPage;
import com.movas.mcement.wizard.ui.PageFragmentCallbacks;
import com.movas.mcement.wizard.ui.ReviewFragment;
import com.movas.mcement.wizard.ui.StepPagerStrip;

public class OrderItemActivity extends FragmentActivity implements PageFragmentCallbacks, ReviewFragment.Callbacks, ModelCallbacks {
    
	private ViewPager mPager;
    private MyPagerAdapter mPagerAdapter;
    
    private boolean mEditingAfterReview;
    
    private AbstractWizardModel mWizardModel = null;
    
    private boolean mConsumePageSelectedEvent;
    
    private Button mNextButton;
    private Button mPrevButton;
    
    private List<Page> mCurrentPageSequence;
    private StepPagerStrip mStepPagerStrip;
    
    Boolean isInitialOrder = false;
    
    ObjectChangeListener objectChangeListener;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_item);
        
        if (savedInstanceState != null) {
            mWizardModel.load(savedInstanceState.getBundle("model"));
        } else {
        	String pickupLocation = getIntent().getStringExtra(Constants.PICKUP_LOCATION);

        	mWizardModel = new OrderWizardModel(this, pickupLocation);
        }
        
        mWizardModel.registerListener(this);
        
        if(getIntent().getBooleanExtra(Constants.EDIT, false)) {
        	
        	String brand = getIntent().getStringExtra("brand");
        	String tonnage = getIntent().getStringExtra("tonnage");
        	
        	mWizardModel.getCurrentPageSequence().get(0).getData().putString(Page.SIMPLE_DATA_KEY, brand);
        	mWizardModel.getCurrentPageSequence().get(1).getData().putString(OrderDetailsPage.TONNAGE_KEY, tonnage);
        }
        
        mPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(mPagerAdapter);
        mStepPagerStrip = (StepPagerStrip) findViewById(R.id.strip);
        mStepPagerStrip.setOnPageSelectedListener(new StepPagerStrip.OnPageSelectedListener() {
            @Override
            public void onPageStripSelected(int position) {
                position = Math.min(mPagerAdapter.getCount() - 1, position);
                if (mPager.getCurrentItem() != position) {
                    mPager.setCurrentItem(position);
                }
            }
        });

        mNextButton = (Button) findViewById(R.id.next_button);
        mPrevButton = (Button) findViewById(R.id.prev_button);

        mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                mStepPagerStrip.setCurrentPage(position);
                
                if (mConsumePageSelectedEvent) {
                    mConsumePageSelectedEvent = false;
                    return;
                }

                mEditingAfterReview = false;
                updateBottomBar();
            }
        });

        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPager.getCurrentItem() == mCurrentPageSequence.size()) {
                	
                	String brand = mWizardModel.getCurrentPageSequence().get(0).getData().getString(Page.SIMPLE_DATA_KEY);
                	String tonnage = mWizardModel.getCurrentPageSequence().get(1).getData().getString(OrderDetailsPage.TONNAGE_KEY);
                	
                	if(!isInitialOrder) {
                		
                		Intent returnIntent = new Intent();
                		returnIntent.putExtra("brand",brand);
                    	returnIntent.putExtra("tonnage",tonnage);
                    	
                    	setResult(RESULT_OK,returnIntent);
                    	
                	} else {
                		Intent intent = new Intent(OrderItemActivity.this, OrderActivity.class);
                		intent.putExtra("brand",brand);
                		intent.putExtra("tonnage",tonnage);
                		intent.putExtra(Constants.LPO_NO, getIntent().getStringExtra(Constants.LPO_NO));
                    	intent.putExtra(Constants.VEHICLE_NO, getIntent().getStringExtra(Constants.VEHICLE_NO));
                    	
                    	
                    	intent.putExtra(Constants.VEHICLE_NUMBERS, getIntent().getStringArrayExtra(OrderPage.VEHICLE_NOS_ARRAY));
                    	intent.putExtra(Constants.PICKUP_LOCATION, getIntent().getStringExtra(Constants.PICKUP_LOCATION));
                    	intent.putExtra(Constants.TO_DELIVER, getIntent().getStringExtra(Constants.TO_DELIVER));
                    	intent.putExtra(Constants.DELIVERY_LOCATION, getIntent().getStringExtra(Constants.DELIVERY_LOCATION));
                    	
                		intent.putExtra(Constants.INITIAL_ORDER, true);
                		
                		startActivity(intent);
                	}
                	
                	finish();
                	
                } else {
                	
                    if (mEditingAfterReview) {
                        mPager.setCurrentItem(mPagerAdapter.getCount() - 1);
                    } else {
                        mPager.setCurrentItem(mPager.getCurrentItem() + 1);
                    } 
                }
            }
        });
        
        mPrevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPager.setCurrentItem(mPager.getCurrentItem() - 1);
            }
        });
        
        onPageTreeChanged();
        updateBottomBar();
        
        isInitialOrder = getIntent().getBooleanExtra(Constants.INITIAL_ORDER, false);
    }
    
    @Override
    public void onPageTreeChanged() {
        mCurrentPageSequence = mWizardModel.getCurrentPageSequence();
        recalculateCutOffPage();
        mStepPagerStrip.setPageCount(mCurrentPageSequence.size() + 1); // + 1 = review step
        mPagerAdapter.notifyDataSetChanged();
        updateBottomBar();
    }
    
    private void updateBottomBar() {
        int position = mPager.getCurrentItem();
        if (position == mCurrentPageSequence.size()) {
            mNextButton.setText(R.string.add);
            mNextButton.setBackgroundResource(R.drawable.finish_background);
            mNextButton.setTextColor(getResources().getColor(android.R.color.white));
        } else {
            mNextButton.setText(mEditingAfterReview ? R.string.review : R.string.next);
            mNextButton.setBackgroundResource(R.drawable.selectable_item_background);
            TypedValue v = new TypedValue();
            getTheme().resolveAttribute(android.R.attr.textAppearanceMedium, v, true);
            mNextButton.setTextAppearance(this, v.resourceId);
            mNextButton.setEnabled(position != mPagerAdapter.getCutOffPage());
        }
        
        mPrevButton.setVisibility(position <= 0 ? View.INVISIBLE : View.VISIBLE);
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mWizardModel.unregisterListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle("model", mWizardModel.save());
    }

    @Override
    public AbstractWizardModel onGetModel() {
        return mWizardModel;
    }

    @Override
    public void onEditScreenAfterReview(String key) {
        for (int i = mCurrentPageSequence.size() - 1; i >= 0; i--) {
            if (mCurrentPageSequence.get(i).getKey().equals(key)) {
                mConsumePageSelectedEvent = true;
                mEditingAfterReview = true;
                mPager.setCurrentItem(i);
                updateBottomBar();
                break;
            }
        }
    }

    @Override
    public void onPageDataChanged(Page page) {
        if (page.isRequired()) {
            if (recalculateCutOffPage()) {
                mPagerAdapter.notifyDataSetChanged();
                updateBottomBar();
            }
        }
    }

    @Override
    public Page onGetPage(String key) {
        return mWizardModel.findByKey(key);
    }

    private boolean recalculateCutOffPage() {
        // Cut off the pager adapter at first required page that isn't completed
        int cutOffPage = mCurrentPageSequence.size() + 1;
        for (int i = 0; i < mCurrentPageSequence.size(); i++) {
            Page page = mCurrentPageSequence.get(i);
            if (page.isRequired() && !page.isCompleted()) {
                cutOffPage = i;
                break;
            }
        }

        if (mPagerAdapter.getCutOffPage() != cutOffPage) {
            mPagerAdapter.setCutOffPage(cutOffPage);
            return true;
        }

        return false;
    }

    public class MyPagerAdapter extends FragmentStatePagerAdapter {
        private int mCutOffPage;
        private Fragment mPrimaryItem;

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            if (i >= mCurrentPageSequence.size()) {
                return new ReviewFragment();
            }

            return mCurrentPageSequence.get(i).createFragment();
        }

        @Override
        public int getItemPosition(Object object) {
            // TODO: be smarter about this
            if (object == mPrimaryItem) {
                // Re-use the current fragment (its position never changes)
                return POSITION_UNCHANGED;
            }

            return POSITION_NONE;
        }
        
        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);
            mPrimaryItem = (Fragment) object;
        }
        
        @Override
        public int getCount() {
        	
            if (mCurrentPageSequence == null) {
                return 0;
            }
            
            return Math.min(mCutOffPage + 1, mCurrentPageSequence.size() + 1);
        }
        
        public void setCutOffPage(int cutOffPage) {
        	
            if (cutOffPage < 0) {
                cutOffPage = Integer.MAX_VALUE;
            }
            
            mCutOffPage = cutOffPage;
        }
        
        public int getCutOffPage() {
            return mCutOffPage;
        }
    }

	@Override
	public void fireObjectChanged(String name, Object value) {
		
		if(objectChangeListener != null)
			objectChangeListener.onChange(name, value);
	}

	@Override
	public void addObjectChangeListener(ObjectChangeListener listener) {
		this.objectChangeListener = listener;
	}
}