package com.movas.mcement.listener;

import java.util.Date;

public interface DateChangeAdapter {

	public void onDateChange(Date date);
}
