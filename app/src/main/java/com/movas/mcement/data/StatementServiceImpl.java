package com.movas.mcement.data;

import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class StatementServiceImpl implements StatementService {
	
	@Override
	public void getMiniStatement(String token, JsonHttpResponseHandler handler) throws UnsupportedEncodingException, JSONException {
		
		String url ="http://193.34.144.238:8181/mservice-proxy/api/mini-statement";
		
		HttpClient client = HttpClient.instance();

		RequestParams params = new RequestParams();
		params.put("token", token);
		params.setUseJsonStreamer(true);
		
		client.getAsyncClient().get(url, params, handler);
	}
	
	@Override
	public void requestFullStatement(String token, String fromDate, String toDate, JsonHttpResponseHandler handler) throws UnsupportedEncodingException, JSONException {
		
		String url = "http://193.34.144.238:8181/mservice-proxy/api/fullStatement";
		
		HttpClient client = HttpClient.instance();
		
		RequestParams params = new RequestParams();
		params.put("fromDate", fromDate);
		params.put("toDate", toDate);
		params.put("token", token);
		params.setUseJsonStreamer(true);

		client.getAsyncClient().post(url, params, handler);
	}
}