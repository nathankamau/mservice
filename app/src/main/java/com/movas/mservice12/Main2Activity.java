package com.movas.mservice12;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

public class Main2Activity extends AppCompatActivity {
    private static final String DEBUG_TAG = "HttpExample";
    private EditText urlText;
    private TextView textView;
    private Button button;



    String url = "http://80.241.222.62:8787/api/tracking/order";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        urlText = (EditText) findViewById(R.id.urlText);
        textView = (TextView) findViewById(R.id.textView);
        button = (Button) findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(urlText.getText().toString().trim().equals(""))
                {
                    urlText.setError("delivery note cannot be empty");
                }else
                {
                    try {
                        request(urlText.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }







//                String stringUrl = urlText.getText().toString();
//                ConnectivityManager connMgr = (ConnectivityManager)
//                        getSystemService(Context.CONNECTIVITY_SERVICE);
//                NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
//                if (networkInfo != null && networkInfo.isConnected()) {
//                    new DownloadWebpageTask().execute(stringUrl);
//                } else {
//                    textView.setText("No network connection available.");
//                }
//            }
//        });
            }

            class DownloadWebpageTask extends AsyncTask<String, Void, String> {
                @Override
                protected String doInBackground(String... urls) {

                    // params comes from the execute() call: params[0] is the url.
                    try {
                        return downloadUrl(urls[0]);
                    } catch (IOException e) {
                        return "Unable to retrieve web page. URL may be invalid.";
                    }
                }

                // onPostExecute displays the results of the AsyncTask.
                @Override
                protected void onPostExecute(String result) {
                    Intent intent = new Intent(Main2Activity.this, MapsActivity.class);
                    intent.putExtra("data", result);
                    startActivity(intent);
                }
            }

            private String downloadUrl(String myurl) throws IOException {
                InputStream is = null;
                int len = 500;

                try {
                    URL url = new URL(myurl);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setReadTimeout(10000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);
                    // Starts the query
                    conn.connect();
                    int response = conn.getResponseCode();

                    Log.d(DEBUG_TAG, "The response is: " + response);
                    is = conn.getInputStream();

                    // Convert the InputStream into a string
                    String contentAsString = readIt(is, len);
                    return contentAsString;

                    // Makes sure that the InputStream is closed after the app is
                    // finished using it.
                } finally {
                    if (is != null) {
                        is.close();
                    }
                }
            }

            public String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
                Reader reader = null;
                reader = new InputStreamReader(stream, "UTF-8");
                char[] buffer = new char[len];
                reader.read(buffer);
                return new String(buffer);
            }

            public boolean request(String text) {



                return true;
            }

        });
    }
}
