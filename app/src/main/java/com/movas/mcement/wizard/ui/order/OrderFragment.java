package com.movas.mcement.wizard.ui.order;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.movas.mservice12.R;
import com.movas.mcement.util.Constants;
import com.movas.mcement.util.PageValidator;
import com.movas.mcement.wizard.model.order.OrderPage;
import com.movas.mcement.wizard.ui.PageFragmentCallbacks;
import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;

public class OrderFragment extends Fragment {
	
    private static final String ARG_KEY = "key";
    
    private PageFragmentCallbacks mCallbacks;
    private String mKey;
    private OrderPage mPage;
    
    @NotEmpty(messageId = R.string.validation_lponumber, order = 1)
    EditText lpoNoView;		
	Spinner transportMethodSpinner;
	
	EditText deliveryLocationView;
	EditText vehicleNoView;
	
	TextView pickupLabel;
	
	LinearLayout toBeCollectedContainer;
	LinearLayout vehicleNosContainer;
	Button addVehicleBtn;
	
    public static OrderFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        
        OrderFragment fragment = new OrderFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    public OrderFragment() {
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = (OrderPage) mCallbacks.onGetPage(mKey);
        
        mPage.addValidator(new PageValidator() {
			
			@Override
			public Boolean isValid() {
				return OrderFragment.this.isValid();
			}
		});
    }
    
    private Boolean isValid() {
    	
		Boolean isValid = FormValidator.validate(this, new SimpleErrorPopupCallback(getActivity()));
		
		if(!isValid)
			return false;
		else {
			
			if(transportMethodSpinner.getSelectedItemPosition() != 0) {
				
				if(deliveryLocationView.getText().toString().trim().equals("")) {
					deliveryLocationView.setError(getString(R.string.validation_delivery_location));
					return false;
				}
				
			} else
				return isVehicleNoValid();
			
			return true;
		}
	}
    
    protected Boolean isVehicleNoValid() {
		
		Boolean isValid = true;
		
		String[] vNos = mPage.getData().getStringArray(OrderPage.VEHICLE_NOS_ARRAY);
		
		if(vNos != null && vNos.length == 0) {
			Toast t = Toast.makeText(getActivity(), getString(R.string.vehicle_required), Toast.LENGTH_LONG);
			t.setGravity(Gravity.TOP, 0, 0);
			t.show();
			isValid = false;
		} else if(vNos != null) {
			
			for(int i = 0; i < vehicleNosContainer.getChildCount(); i++) {
				
				LinearLayout vRow = (LinearLayout) vehicleNosContainer.getChildAt(i);
				
				EditText vNoView = (EditText) vRow.getChildAt(1);
				
				String vehicleNo = vNoView.getText().toString();
				
				if(TextUtils.isEmpty(vehicleNo)) {
					
					vNoView.setError(getString(R.string.validation_vehicleno));
					isValid = false;
				} else if(vehicleNo.length() < 5) {
					
					vNoView.setError(getString(R.string.validation_vehicleno_length));
					isValid = false;
				} else if(!vehicleNo.matches("^(?=.*\\d)(?=.*[a-zA-Z]).{5,10}$")) {
					
					vNoView.setError(getString(R.string.validation_vehicle_no_alphanumeric));
					isValid = false;
				}
			}
		}
		
		return isValid;
	}
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_page_order_first, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());
        
//        tonnage = (TextView) rootView.findViewById(R.id.tonnage);
//        tonnage.setText(mPage.getData().getString(OrderPage.TONNAGE_KEY));
        
        lpoNoView = (EditText) rootView.findViewById(R.id.lpoNoView);
        lpoNoView.setText(mPage.getData().getString(OrderPage.LPO_NO_KEY));
		vehicleNoView = (EditText) rootView.findViewById(R.id.vehicleNoView);
		vehicleNoView.setText(mPage.getData().getString(OrderPage.VEHICLE_NO_KEY));
		pickupLabel = (TextView) rootView.findViewById(R.id.pickupLabel);
		transportMethodSpinner = (Spinner) rootView.findViewById(R.id.transportMethodSpinner);
		deliveryLocationView = (EditText) rootView.findViewById(R.id.deliveryLocationView);
		deliveryLocationView.setText(mPage.getData().getString(OrderPage.LPO_NO_KEY));
		toBeCollectedContainer = (LinearLayout) rootView.findViewById(R.id.toBeCollectedContainer);
		vehicleNosContainer = (LinearLayout) rootView.findViewById(R.id.vehicleNosContainer);
		addVehicleBtn = (Button) rootView.findViewById(R.id.addVehicleBtn);
		
		ArrayAdapter<CharSequence> transMethodAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.transport_methods, android.R.layout.simple_spinner_item);
		transMethodAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		transportMethodSpinner.setAdapter(transMethodAdapter);
		
		String tMethod = mPage.getData().getString(OrderPage.TRANSPORT_METHOD_KEY);
		
		if(!TextUtils.isEmpty(tMethod)) {
			for(int t = 0; t < transMethodAdapter.getCount(); t++) {
				
				if(transMethodAdapter.getItem(t).toString().equals(tMethod)) {
					transportMethodSpinner.setSelection(t);
					break;
				}
			}
		}
		
		transportMethodSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				
				deliveryLocationView.setVisibility(position == 1 ? View.VISIBLE : View.GONE);
				toBeCollectedContainer.setVisibility(position == 0 ? View.VISIBLE : View.GONE);
				
				mPage.getData().putString(OrderPage.TRANSPORT_METHOD_KEY, transportMethodSpinner.getSelectedItem().toString());
				
				if(position == 0) {
					
					deliveryLocationView.setText("");
					mPage.getData().putString(OrderPage.DELIVERY_LOCATION_KEY, "");
				} else if(position == 1) {
					
					vehicleNoView.setText("");
					mPage.getData().putString(OrderPage.VEHICLE_NO_KEY, "");
				}
				
                mPage.notifyDataChanged();
			}
			
			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});
		
		addVehicleBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				addVehicleRow("");
			}
		});
		
		Boolean isSingleOrder = getActivity().getIntent().getBooleanExtra(Constants.IS_SINGLE_ORDER, false);
		
//		if(!isSingleOrder) {
//			
//			String[] vNos = mPage.getData().getStringArray(OrderPage.VEHICLE_NOS_ARRAY);
//			
//			if(vNos != null && vNos.length > 1)
//				addVehicleRows(vNos);
//			else if(vNos != null && vNos.length == 1)
//				addVehicleRow(vNos[0]);
//			else
//				addVehicleRow("");
//			
//		} else {
			addVehicleBtn.setVisibility(View.GONE);
			vehicleNosContainer.setVisibility(View.GONE);
			rootView.findViewById(R.id.vehicleDesc).setVisibility(View.GONE);
			vehicleNoView.setVisibility(View.VISIBLE);
		//}
		
        return rootView;
    }
    
    private void addVehicleRows(String[] vNos) {
    	
    	for(int i = 0; i < vNos.length; i++) {
    		addVehicleRow(vNos[i]);
    	}
	}
    
	protected void addVehicleRow(String plateNo) {
    	
    	final LinearLayout vehicleRow = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.vehicle_row, null);
    	
    	EditText vehicleNoView = (EditText) vehicleRow.getChildAt(1);
    	vehicleNoView.setText(plateNo);
    	
    	TextView counterView = (TextView) vehicleRow.getChildAt(0);
    	EditText vehicleTonnageView = (EditText) vehicleRow.getChildAt(2);
    	TextView removeView = (TextView) vehicleRow.getChildAt(3);
    	counterView.setText((vehicleNosContainer.getChildCount() + 1) + "");
    	removeView.setOnClickListener(new View.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				vehicleNosContainer.removeView(vehicleRow);
			}
		});
    	
    	vehicleNosContainer.addView(vehicleRow);
    	
    	vehicleNoView.addTextChangedListener(new TextWatcher() {
    		
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			
			@Override
			public void afterTextChanged(Editable s) {
				
				String vehicleNos = "";
				String vNos[] = new String[vehicleNosContainer.getChildCount()];
				
				for(int i = 0; i < vehicleNosContainer.getChildCount(); i++) {
					
					LinearLayout vRow = (LinearLayout) vehicleNosContainer.getChildAt(i);
					
					EditText vNoView = (EditText) vRow.getChildAt(1);
					
					vNos[i] = vNoView.getText().toString();
					if(!TextUtils.isEmpty(vNoView.getText().toString())) {
						vehicleNos += vehicleNos.equals("") ? vNoView.getText().toString() :  "," + vNoView.getText().toString();
					}
				}
				
				mPage.getData().putStringArray(OrderPage.VEHICLE_NOS_ARRAY, vNos);
				mPage.getData().putString(OrderPage.VEHICLE_NO_KEY, vehicleNos);
                mPage.notifyDataChanged();
			}
		});
	}
    
	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
        if (!(activity instanceof PageFragmentCallbacks)) {
            throw new ClassCastException("Activity must implement PageFragmentCallbacks");
        }
        
        mCallbacks = (PageFragmentCallbacks) activity;
    }
	
    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }
    
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        
        lpoNoView.addTextChangedListener(getTextWatcher(OrderPage.LPO_NO_KEY));
        deliveryLocationView.addTextChangedListener(getTextWatcher(OrderPage.DELIVERY_LOCATION_KEY));
        vehicleNoView.addTextChangedListener(getTextWatcher(OrderPage.VEHICLE_NO_KEY));
    }
    
    protected TextWatcher getTextWatcher(final String name) {
    	
    	return new TextWatcher() {
    		
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            
            @Override
            public void afterTextChanged(Editable editable) {
                mPage.getData().putString(name,
                        (editable != null) ? editable.toString() : null);
                mPage.notifyDataChanged();
            }
        };
    }
    
    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        
        if (lpoNoView != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            if (!menuVisible) {
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            }
        }
    }
}