package com.movas.mcement.data;

import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class PriceServiceImpl implements PriceService {
	
	@Override
	public void loadPrices(String token, String id, String location, JsonHttpResponseHandler handler) throws UnsupportedEncodingException, JSONException {
		
		String url = "http://193.34.144.238:8181/mservice-proxy/api/productList";
		
		HttpClient client = HttpClient.instance();
		
		RequestParams params = new RequestParams();
		params.put("location", location);
		params.put("token", token);
		params.setUseJsonStreamer(true);
		client.getAsyncClient().get(url, params, handler);
	}
}
