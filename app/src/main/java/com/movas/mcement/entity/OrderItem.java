package com.movas.mcement.entity;

import java.io.Serializable;

public class OrderItem implements Serializable {
	
	String brand;
	
	String tonnage;
	
	String lpoNo;
	
	String vehicleNo;
	
	public OrderItem() {}
	
	public OrderItem(String brand, String tonnage) {
		this.brand = brand;
		this.tonnage = tonnage;
	}
	
	public OrderItem(String brand, String tonnage, String lpoNo, String vehicleNo) {
		this.brand = brand;
		this.tonnage = tonnage;
		this.lpoNo = lpoNo;
		this.vehicleNo = vehicleNo;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getTonnage() {
		return tonnage;
	}

	public void setTonnage(String tonnage) {
		this.tonnage = tonnage;
	}

	public String getLpoNo() {
		return lpoNo;
	}

	public void setLpoNo(String lpoNo) {
		this.lpoNo = lpoNo;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
}