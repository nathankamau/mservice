package com.movas.mcement.wizard.model.order;

import java.util.ArrayList;
import android.support.v4.app.Fragment;

import com.movas.mcement.wizard.model.ModelCallbacks;
import com.movas.mcement.wizard.model.Page;
import com.movas.mcement.wizard.model.ReviewItem;
import com.movas.mcement.wizard.ui.order.MultiOrderDetailsFragment;

public class MultiOrderDetailsPage extends Page {
	
	public static final String LPO_NO_KEY = "lpo_no";
		
	public static final String VEHICLE_NOS_ARRAY = "v_nos_array";
	
	public static final String TRIPS_ARRAY = "trips";
	
	public static final String TONNAGE_ARRAY = "tonnage";
	
	public static final String BRANDS_ARRAY = "brands";
	
	public static final String TRANSPORT_METHOD_KEY = "transport_method";
	
	public static final String DELIVERY_LOCATION_KEY = "delivery_location";
	
    public MultiOrderDetailsPage(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
    }
    
    @Override
    public Fragment createFragment() {
        return MultiOrderDetailsFragment.create(getKey());
    }
    
    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
    	
        dest.add(new ReviewItem("LPO No", mData.getString(LPO_NO_KEY), getKey(), -1));
        dest.add(new ReviewItem("Transport Method", mData.getString(TRANSPORT_METHOD_KEY), getKey(), -1));
        dest.add(new ReviewItem("Delivery Location", mData.getString(DELIVERY_LOCATION_KEY), getKey(), -1));
        
        StringBuilder sb = new StringBuilder();
        String[] vehicles = mData.getStringArray(VEHICLE_NOS_ARRAY);

        if (vehicles != null && vehicles.length > 0) {
            for (String selection : vehicles) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append(selection);
            }
        }
        
        dest.add(new ReviewItem("Vehicles", sb.toString(), getKey(), -1));
    }
    
    @Override
    public boolean isCompleted() {
    	
//    	Boolean isComplete = false;
//    	isComplete = !TextUtils.isEmpty(mData.getString(LPO_NO_KEY));
//    	isComplete = isComplete ? !TextUtils.isEmpty(mData.getString(TRANSPORT_METHOD_KEY)) : false;
    	
//    	if(isComplete) {
    	
//    		if(MovaPay.get().getString(R.string.to_be_collected).equals(mData.getString(TRANSPORT_METHOD_KEY))) {
//    			//isComplete = !TextUtils.isEmpty(mData.getString(VEHICLE_NO_KEY));
//        	} else if(MovaPay.get().getString(R.string.to_be_collected).equals(mData.getString(TRANSPORT_METHOD_KEY))) {
//        		isComplete = !TextUtils.isEmpty(mData.getString(DELIVERY_LOCATION_KEY));
//        	}
//    	}
    	
        return true;
    }
}