package com.movas.mservice12;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.movas.mcement.adapter.OrderItemsAdapter;
import com.movas.mcement.data.OrderService;
import com.movas.mcement.data.OrderServiceImpl;
import com.movas.mcement.entity.OrderItem;
import com.movas.mcement.helper.DialogHelper;
import com.movas.mcement.helper.ErrorHandler;
import com.movas.mcement.util.Constants;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import cz.msebera.android.httpclient.Header;

public class MyOrderDetailsActivity extends ActionBarActivity {
	
	RecyclerView itemsList;
	RecyclerView vehiclesList;
	ProgressBar progressBar;
	private OrderItemsAdapter mAdapter;
	TextView lpoNoView;
	TextView otherDetailsView;
	TextView orderStatusView;
	ImageView statusImageView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_order_details);
		
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyOrderDetailsActivity.this.onBackPressed();
            }
        });
        
        itemsList = (RecyclerView) findViewById(R.id.list);
        //vehiclesList = (RecyclerView) findViewById(R.id.vehiclesList);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        
        itemsList.setLayoutManager(new LinearLayoutManager(this));
        
        mAdapter = new OrderItemsAdapter(new ArrayList<OrderItem>(), R.layout.partial_order_item_row, this);
        itemsList.setAdapter(mAdapter);
        
        lpoNoView = (TextView) findViewById(R.id.lpoNoView);
        otherDetailsView = (TextView) findViewById(R.id.otherDetailsView);
        
        lpoNoView.setText("LPO: " + getIntent().getStringExtra("lponumber"));
        
        String vehicleReg = getIntent().getStringExtra("vehiclereg");
                
        Boolean hasVehicleNo = !"null".equals(vehicleReg) && vehicleReg != null && !TextUtils.isEmpty(vehicleReg);
        if(hasVehicleNo) {
        	otherDetailsView.setText(vehicleReg);
        } else {
        	//show pickup location
        	otherDetailsView.setText(getIntent().getStringExtra("deliverylocation"));
        }
        
        orderStatusView = (TextView) findViewById(R.id.orderStatusView);
        statusImageView = (ImageView) findViewById(R.id.statusImageView);
        
        String status = getIntent().getStringExtra("status");
        
        orderStatusView.setText(status);
        
        if("Awaiting Collection".equals(status))
        	statusImageView.setImageResource(R.drawable.order_processed); 
        else if("First Weight".equals(status))
        	statusImageView.setImageResource(R.drawable.first_weight); 
        else if("Second Weight".equals(status))
        	statusImageView.setImageResource(R.drawable.second_weight); 
        else if("Awaiting Delivery Confirmation".equals(status))
        	statusImageView.setImageResource(R.drawable.confirm_delivery);
        
        loadOrderItems();
	}
	
	private void loadOrderItems() {
		
		Integer orderID = getIntent().getIntExtra("orderrequestid", 0);
		OrderService service = new OrderServiceImpl();

		SharedPreferences prefs = getApplication().getSharedPreferences(Constants.MovaPay, Context.MODE_PRIVATE);
		String token = prefs.getString("token", "");
		
		try {
			service.loadMyOrderItems(token, orderID + "", new JsonHttpResponseHandler() {

				@Override
				public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
					super.onSuccess(statusCode, headers, response);

					for(int i = 0; i < response.length(); i++) {

						try {

							JSONObject row = response.getJSONObject(i);
							
							OrderItem orderItem = new OrderItem();
				            
				            orderItem.setBrand(row.getString("name"));
				            orderItem.setTonnage(row.getString("tonnage"));
				            mAdapter.addItem(orderItem);
							
						} catch (JSONException e) {}
					}
					
					mAdapter.notifyDataSetChanged();
										
					itemsList.setVisibility(View.VISIBLE);
		            progressBar.setVisibility(View.GONE);
				}
				
				@Override
				public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
					super.onFailure(statusCode, headers, responseString, throwable);

					DialogHelper.hideProgressDialog();
					ErrorHandler.handleError(MyOrderDetailsActivity.this, throwable);
				}
				
				@Override
				public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
					super.onFailure(statusCode, headers, throwable, errorResponse);

					DialogHelper.hideProgressDialog();
					ErrorHandler.handleError(MyOrderDetailsActivity.this, throwable);
				}
			});
		} catch (UnsupportedEncodingException | JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}