package com.movas.mservice12;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.movas.mcement.adapter.OrderItemsAdapter;
import com.movas.mcement.data.OrderService;
import com.movas.mcement.data.OrderServiceImpl;
import com.movas.mcement.entity.OrderItem;
import com.movas.mcement.helper.DialogHelper;
import com.movas.mcement.helper.ErrorHandler;
import com.movas.mcement.util.Constants;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import cz.msebera.android.httpclient.Header;

public class OrderActivity extends ActionBarActivity implements OnClickListener {
	
	private static final int SCALE_DELAY = 30;
	
	LinearLayout rowContainer;
	ImageButton addBtn;
	
	private OrderItemsAdapter mAdapter;
    private RecyclerView mRecyclerView;
    
    TextView lpoNoView;
    TextView otherDetailsView;
    
    Boolean isInitialOrder;
    
    String lpoNo = "";
	String vehicleNo = "";
	String[] vehicleNos;
	String pickupLocation = "";
	
	int editPosition = -1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_order);
		
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderActivity.this.onBackPressed();
            }
        });
        
        lpoNoView = (TextView) findViewById(R.id.lpoNoView);
        otherDetailsView = (TextView) findViewById(R.id.otherDetailsView);
        
        mRecyclerView = (RecyclerView) findViewById(R.id.list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        
        mAdapter = new OrderItemsAdapter(new ArrayList<OrderItem>(), R.layout.partial_order_item_row, this);
        mRecyclerView.setAdapter(mAdapter);
        
        rowContainer = (LinearLayout) findViewById(R.id.row_container);
        addBtn = (ImageButton) findViewById(R.id.addBtn);
        addBtn.setOnClickListener(this);
        
        isInitialOrder = getIntent().getBooleanExtra(Constants.INITIAL_ORDER, false);
        
        if(isInitialOrder)
        	addRow(getIntent());
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.order, menu);
		
		MenuItem item = menu.findItem(R.id.action_order);
		item.setVisible(mAdapter.getItemCount() > 0);
		
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		int id = item.getItemId();
		
		switch (id) {
			case android.R.id.home:
				break;
			case R.id.action_order:
				
				new AlertDialog.Builder(this)
					.setMessage(getString(R.string.order_confirmation))
					.setCancelable(false)
					.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							makeOrder();
						}
					})
					.setNegativeButton("No", null)
					.show();
				break;
			default:
				break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		
		new AlertDialog.Builder(this)
		.setMessage(getString(R.string.order_cancel_confirmation))
		.setCancelable(false)
		.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				finish();
			}
		})
		.setNegativeButton("No", null)
		.show();
	}
	
	public boolean onContextItemSelected(MenuItem item) {
		
		int position = -1;
		
		try {
	        position = mAdapter.getPosition();
	    } catch (Exception e) {
	        return super.onContextItemSelected(item);
	    }
		
	    String title = item.getTitle().toString();
	    
	    if(getString(R.string.edit).equals(title)) {
	    	
	    	Intent intent = new Intent(OrderActivity.this, OrderItemActivity.class);
	    	intent.putExtra(Constants.PICKUP_LOCATION, pickupLocation);
	    	
	    	intent.putExtra(Constants.EDIT, true);
	    	intent.putExtra("brand", mAdapter.getItems().get(position).getBrand());
    		intent.putExtra("tonnage",mAdapter.getItems().get(position).getTonnage());
    		
	    	startActivityForResult(intent, 101);
	    	
	    } else if(getString(R.string.delete).equals(title)) {
	    	mAdapter.removeItem(position);
	    }
	    
	    return super.onContextItemSelected(item);
	}
	
	private void makeOrder() {
		
		OrderService service = new OrderServiceImpl();
		
		DialogHelper.showProgressDialog(this, getString(R.string.please_wait), false);
		
		SharedPreferences prefs = getApplication().getSharedPreferences(Constants.MovaPay, Context.MODE_PRIVATE);
		String customerID = prefs.getString(Constants.CUSTOMER_ID, "");
		String uuid = prefs.getString(Constants.CUSTOMER_UUID, "");
		String token = prefs.getString("token", "");
		
		String todeliver = prefs.getString(Constants.TO_DELIVER, "no");
		String deliveryLocation = prefs.getString(Constants.DELIVERY_LOCATION, "");
		
		try {
			
			service.placeOrder(token, customerID, lpoNo, pickupLocation, todeliver, deliveryLocation, vehicleNo, mAdapter.getItems(), uuid, new JsonHttpResponseHandler() {
				
				@Override
				public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
					super.onSuccess(statusCode, headers, response);
					
					DialogHelper.hideProgressDialog();
					
					try {
						
						Integer status = response.getInt("status");


						if(status == 0) {
							Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.order_success_message) , Toast.LENGTH_LONG);
							toast.setGravity(Gravity.TOP, 0, 0);
							toast.show();
							finish();
							
						} else {
							ErrorHandler.handleError(OrderActivity.this, new Throwable(response.getString("message")));
						}
						
					} catch (JSONException e1) {}
				}
				
				@Override
				public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
					super.onFailure(statusCode, headers, responseString, throwable);
					DialogHelper.hideProgressDialog();
					throwable.printStackTrace();
					ErrorHandler.handleError(OrderActivity.this, throwable);
				}
				
				@Override
				public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
					super.onFailure(statusCode, headers, throwable, errorResponse);
					
					DialogHelper.hideProgressDialog();
					ErrorHandler.handleError(OrderActivity.this, throwable);
				}
			});
		} catch (UnsupportedEncodingException | JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View v) {
		if(v == addBtn) {
			Intent intent = new Intent(this, OrderItemActivity.class);
			intent.putExtra(Constants.PICKUP_LOCATION, pickupLocation);
			startActivityForResult(intent, 100);
		}
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		System.out.print("....................onActivityResult.............................");
		System.out.print(requestCode);

	    if (requestCode == 100) {
	    	
	        if(resultCode == RESULT_OK){
	            addRow(data);  
	        }
	        
	    } if(requestCode == 101) {
	    	
	    	if(resultCode == RESULT_OK) {
	    		OrderItem orderItem = mAdapter.getItems().get(mAdapter.getPosition());
	    		
	    		String brand=data.getStringExtra("brand");
	            String tonnage = data.getStringExtra("tonnage");
	            
	            orderItem.setBrand(brand);
	            orderItem.setTonnage(tonnage);
	            
	            mAdapter.notifyDataSetChanged();
	    	}
	    }
	}
	
	private void addRow(Intent data) {

		System.out.print("....................addRow......................");
		System.out.print(data);
		Boolean isInitialOrder = data.getBooleanExtra(Constants.INITIAL_ORDER, false);
		
		if(isInitialOrder) {
			lpoNo = data.getStringExtra(Constants.LPO_NO);
			vehicleNo = data.getStringExtra(Constants.VEHICLE_NO);
			vehicleNos = data.getStringArrayExtra(Constants.VEHICLE_NUMBERS);
			pickupLocation = data.getStringExtra(Constants.PICKUP_LOCATION);
			
			lpoNoView.setText(getString(R.string.lpoNo) + ": " + lpoNo);
			otherDetailsView.setText(pickupLocation + " # " + vehicleNo);
		}
		
		String brand=data.getStringExtra("brand");
        String tonnage = data.getStringExtra("tonnage");
        
        OrderItem orderItem = new OrderItem();
        orderItem.setBrand(brand);
        orderItem.setTonnage(tonnage);
        mAdapter.addItem(orderItem);
        supportInvalidateOptionsMenu();
	}
}