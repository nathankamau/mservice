package com.movas.mcement.data;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;


public interface PointsService {
     void getPoints(String accountNumber,String token ,JsonHttpResponseHandler handler) throws UnsupportedEncodingException, JSONException;
}
