package com.movas.mcement.data;

import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class InvoiceServiceImpl implements  InvoiceService {
	
	@Override
	public void requestInvoice(String token, String type, String text, String id, JsonHttpResponseHandler handler)
			throws UnsupportedEncodingException, JSONException {
		
		String url = "http://193.34.144.238:8181/mservice-proxy/api/invoice";
		
		HttpClient client = HttpClient.instance();
		
		RequestParams params = new RequestParams();
		params.put("type", type);
		params.put("text", text);
		params.put("token", token);
		params.setUseJsonStreamer(true);

		client.getAsyncClient().post(url, params, handler);
	}
}