package com.movas.mcement.entity;


import java.util.Date;
import java.util.List;

public class Track {

    public String accountNumber;

    public Date deliveryDate;

    public Date deliveryTime;

    public String itemDescription;

    public int tonnage;

    public String deliveryNoteNumber;

    public String truckReg;

    public String invoiceNo;

    public List<Gps> gps;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Date getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Date deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getItemDescription() {

        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public int getTonnage() {
        return tonnage;
    }

    public void setTonnage(int tonnage) {
        this.tonnage = tonnage;
    }

    public String getDeliveryNoteNumber() {
        return deliveryNoteNumber;
    }

    public void setDeliveryNoteNumber(String deliveryNoteNumber) {
        this.deliveryNoteNumber = deliveryNoteNumber;
    }

    public String getTruckReg() {
        return truckReg;
    }

    public void setTruckReg(String truckReg) {
        this.truckReg = truckReg;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public List<Gps> getGps() {
        return gps;
    }

    public void setGps(List<Gps> gps) {
        this.gps = gps;
    }


    public static class Gps {

        public int carId;

        public Date gpsTime;

        public Date rcvTime;

        public double longitude;

        public double latitude;

        public double speed;

        public double head;

        public boolean valid;

        public int fuel;

        public int acc;

        public String mileage;

        public String terminalKey;


        public void setCarId(int carId) {
            this.carId = carId;
        }

        public Date getGpsTime() {
            return gpsTime;
        }

        public void setGpsTime(Date gpsTime) {
            this.gpsTime = gpsTime;
        }

        public Date getRcvTime() {
            return rcvTime;
        }

        public void setRcvTime(Date rcvTime) {
            this.rcvTime = rcvTime;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getSpeed() {
            return speed;
        }

        public void setSpeed(double speed) {
            this.speed = speed;
        }

        public double getHead() {
            return head;
        }

        public void setHead(double head) {
            this.head = head;
        }

        public boolean isValid() {
            return valid;
        }

        public void setValid(boolean valid) {
            this.valid = valid;
        }

        public int getFuel() {
            return fuel;
        }

        public void setFuel(int fuel) {
            this.fuel = fuel;
        }

        public int isAcc() {
            boolean isOn=true;
            if(isOn){
                acc=1;
            }else {
                acc=0;
            }
            return acc;
        }

        public void setAcc(int acc) {
            this.acc = acc;
        }

        public String getMileage() {
            return mileage;
        }

        public void setMileage(String mileage) {
            this.mileage = mileage;
        }

        public String getTerminalKey() {
            return terminalKey;
        }

        public void setTerminalKey(String terminalKey) {
            this.terminalKey = terminalKey;
        }




    }

}

