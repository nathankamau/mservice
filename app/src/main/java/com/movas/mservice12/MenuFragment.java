package com.movas.mservice12;

import java.io.UnsupportedEncodingException;

import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.movas.mcement.adapter.HomeMenuAdapter;
import com.movas.mcement.data.AuthService;
import com.movas.mcement.data.AuthServiceImpl;
import com.movas.mcement.entity.HomeMenuItem;
import com.movas.mcement.helper.DialogHelper;
import com.movas.mcement.util.Constants;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import cz.msebera.android.httpclient.Header;

public class MenuFragment extends Fragment {
	
	HomeMenuAdapter adapter;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.partial_home_menu, container, false);
		
		ListView listView = (ListView) view.findViewById(android.R.id.list);
		//listView.setDivider(getActivity().getResources().getDrawable(R.drawable.abc_list_divider_holo_dark));
		
		listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		//listView.setSelector(R.drawable.list_selector);
		
		adapter = new HomeMenuAdapter(getActivity().getApplicationContext());
		
		//adapter.add(new HomeMenuItem("My Profile"));
		adapter.add(new HomeMenuItem("Change PIN"));
		adapter.add(new HomeMenuItem("Feedback"));
//		adapter.add(new HomeMenuItem("About"));
//		adapter.add(new HomeMenuItem("Help"));
		adapter.add(new HomeMenuItem("Logout"));
		
		listView.setAdapter(adapter);
		return view;
	}
	
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		ListView listView = (ListView) this.getView().findViewById(
				android.R.id.list);
				
		listView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
				toggle();
				showFragment(adapter.getItem(position));
			}
		});
	}
	
	protected void showFragment(HomeMenuItem item) {
		
		if(item.getTag().equals("Logout")) {
			DialogHelper.showProgressDialog(getActivity(), getString(R.string.logging_out), false);
			logout();
		}  else if(item.getTag().equals("Change PIN")) {
			Intent intent = new Intent(getActivity(), ChangePinActivity.class);
			getActivity().startActivity(intent);
		} else if("Feedback".equals(item.getTag())) {
			Intent intent = new Intent(getActivity(), FeedbackActivity.class);
			getActivity().startActivity(intent);
		}
	}
	
	private void logout() {

		SharedPreferences prefs = getActivity().getApplication().getSharedPreferences(Constants.MovaPay, Context.MODE_PRIVATE);
		String token = prefs.getString("token", "");

		AuthService service = new AuthServiceImpl();
		try {
			service.logout(token, new JsonHttpResponseHandler() {
				
				@Override
				public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
					super.onSuccess(statusCode, headers, response);
					DialogHelper.hideProgressDialog();
					
//					getActivity()
//						.getApplication()
//						.getSharedPreferences(Constants.MovaPay, Context.MODE_PRIVATE)
//						.edit().clear().commit();
					
					getActivity().finish();
					
					Intent intent = new Intent(getActivity(), SplashActivity.class);
					startActivity(intent);
				}
				
				@Override
				public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
					super.onFailure(statusCode, headers, responseString, throwable);
					DialogHelper.hideProgressDialog();
					throwable.printStackTrace();
					Toast toast = Toast.makeText(getActivity(), responseString, Toast.LENGTH_LONG);
					toast.setGravity(Gravity.TOP, 0, 0);
					toast.show();
				}
				
				@Override
				public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
					super.onFailure(statusCode, headers, throwable, errorResponse);
					
					DialogHelper.hideProgressDialog();
					Toast toast = Toast.makeText(getActivity(), throwable.getMessage(), Toast.LENGTH_LONG);
					toast.setGravity(Gravity.TOP, 0, 0);
					toast.show();
				}
			});
		} catch (UnsupportedEncodingException | JSONException e) {
			e.printStackTrace();
		}
	}

	public void toggle() {
		if(getActivity() instanceof HomeActivity) {
			HomeActivity activity = (HomeActivity) getActivity();
			activity.toggle();
		}
	}
}