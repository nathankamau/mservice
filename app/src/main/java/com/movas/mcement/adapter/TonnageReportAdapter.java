package com.movas.mcement.adapter;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;
import com.movas.mservice12.R;
import com.movas.mcement.entity.TonnageReport;

public class TonnageReportAdapter extends RecyclerView.Adapter<TonnageReportAdapter.ViewHolder> {

    private List<TonnageReport> tonnageReports;
    private int rowLayout;
    private Activity mAct;

    public TonnageReportAdapter(List<TonnageReport> tonnageReports, int rowLayout, Activity act) {
        this.tonnageReports = tonnageReports;
        this.rowLayout = rowLayout;
        this.mAct = act;
    }


    public void clear() {
        int size = this.tonnageReports.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                tonnageReports.remove(0);
            }

            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void addItems(List<TonnageReport> tonnageReports) {
        this.tonnageReports.addAll(tonnageReports);
        this.notifyItemRangeInserted(0, tonnageReports.size() - 1);
    }
    
    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(rowLayout, viewGroup, false);
        ViewHolder holder = new ViewHolder(v, mAct);
        return holder;
    }
    
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        final TonnageReport tonnageReport = tonnageReports.get(i);
        viewHolder.nameView.setText(tonnageReport.getName());
        viewHolder.tonnageView.setText(tonnageReport.getTonnage().toString());
    }
    
    @Override
    public int getItemCount() {
        return tonnageReports == null ? 0 : tonnageReports.size();
    }
    
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    	
    	public TextView nameView;
    	public TextView tonnageView;
    	
        Activity hAct;
        
        public ViewHolder(View itemView, Activity mAct) {
            super(itemView);
            nameView = (TextView) itemView.findViewById(R.id.nameView);
            tonnageView = (TextView) itemView.findViewById(R.id.tonnageView);
            hAct = mAct;
            nameView.setOnClickListener(this);
        }
        
		@Override
		public void onClick(View v) {
		}
		
		@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
		private void startActivity16(Intent i){
	    	ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(hAct);
	    	hAct.startActivity(i, transitionActivityOptions.toBundle());
	    }
    }
}