package com.movas.mcement.data;

import java.io.UnsupportedEncodingException;
import org.json.JSONException;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class ReviewServiceImpl implements ReviewService {
	
	@Override
	public void submitReview(String token, String sapClientId, String id, String review, Integer type, JsonHttpResponseHandler handler)
			throws UnsupportedEncodingException, JSONException {
		
		String url = "http://193.34.144.238:8181/mservice-proxy/api/postComments";
		
		//HttpClient client = HttpClient.instance();
		AsyncHttpClient client = new AsyncHttpClient();
		
		RequestParams params = new RequestParams();
		params.put("customerid", sapClientId);
		params.put("status", type + "");
		params.put("message", review);
		params.put("token", token);
		params.setUseJsonStreamer(true);
		
		client.post(url, params, handler);
		
	}
}