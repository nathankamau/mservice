package com.movas.mservice12;

import java.util.ArrayList;
import java.util.List;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.CanvasTransformer;
import com.movas.mcement.adapter.MenuAdapter;
import com.movas.mcement.util.Constants;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class HomeActivity extends ActionBarActivity {
	
	private List<com.movas.mcement.entity.MenuItem> itemsList = new ArrayList<com.movas.mcement.entity.MenuItem>();
	
	private MenuAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;
    
    ImageView laucherView;
    
    TextView customerIDView;
    SlidingMenu slidingMenu;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		
		laucherView = (ImageView) findViewById(R.id.laucherView);
		
        mRecyclerView = (RecyclerView) findViewById(R.id.list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        
        customerIDView = (TextView) findViewById(R.id.customerIDView);
        
        mAdapter = new MenuAdapter(new ArrayList<com.movas.mcement.entity.MenuItem>(), R.layout.row_menu, HomeActivity.this);
        mRecyclerView.setAdapter(mAdapter);
        
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        
        SharedPreferences prefs = getApplication().getSharedPreferences(Constants.MovaPay, Context.MODE_PRIVATE);
        //set the customerId on top of the menu
		String customerID = prefs.getString(Constants.CUSTOMER_ID, "");
		customerIDView.setText(getString(R.string.customer_id) + " # " + customerID);
		
        new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				new InitializeApplicationsTask().execute();
			}
		}, 600);
        
        laucherView.setOnClickListener(new View.OnClickListener() {
        	
			@Override
			public void onClick(View v) {
				slidingMenu.toggle();
			}
		});
        
        attachSlidingMenu();
	}
	
	public void toggle() {
		slidingMenu.toggle();
	}
	
	@Override
	public void onBackPressed() {
		if(slidingMenu.isMenuShowing())
			slidingMenu.toggle();
		else {
			this.moveTaskToBack(true);
		}
	}
	
	private void attachSlidingMenu() {
		
		slidingMenu = new SlidingMenu(this);
        slidingMenu.setMode(SlidingMenu.RIGHT);
        slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        slidingMenu.setShadowWidthRes(R.dimen.shadow_width);
        slidingMenu.setFadeEnabled(false);
        slidingMenu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        slidingMenu.setFadeDegree(0.35f);
        slidingMenu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        slidingMenu.setMenu(R.layout.fragment_menu);
        
		CanvasTransformer transformer = new CanvasTransformer() {
			@Override
			public void transformCanvas(Canvas canvas, float percentOpen) {
				float scale = (float) (percentOpen*0.25 + 0.75);
				canvas.scale(scale, scale, canvas.getWidth()/2, canvas.getHeight()/2);
			}
		};
		
		//menu.setBehindScrollScale(0.0f);
		slidingMenu.setBehindCanvasTransformer(transformer);
		
		slidingMenu.setOnOpenedListener(new SlidingMenu.OnOpenedListener() {
			@Override
			public void onOpened() {
				hideSoftKeyPad();
			}
		});
		
		slidingMenu.setOnClosedListener(new SlidingMenu.OnClosedListener() {
			@Override
			public void onClosed() {
				hideSoftKeyPad();
			}
		});
	}
	
	public void hideSoftKeyPad() {
		InputMethodManager inputManager = (InputMethodManager) this
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		
		View v = this.getCurrentFocus();
		if (v == null)
			return;
		
		inputManager.hideSoftInputFromWindow(v.getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}
	
	@Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(menu);
	}
	
	public boolean onContextItemSelected(MenuItem item) {
		return super.onContextItemSelected(item);
	}
	
    private class InitializeApplicationsTask extends AsyncTask<Void, Void, Void> {
    	
        @Override
        protected void onPreExecute() {
            mAdapter.clearApplications();
            super.onPreExecute();
        }
        
        @Override
        protected Void doInBackground(Void... params) {
        	
        	itemsList.clear();
        	
            final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
            mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            
            String menuNames[] = new String[]{
	    		getString(R.string.place_order),
	    		getString(R.string.view_pricelist),
	    		getString(R.string.check_balance),
				getString(R.string.check_points),
	    		getString(R.string.confirm_delivery),
	    		getString(R.string.my_orders),
//	    		getString(R.string.mini_statement),
	    		getString(R.string.request_full_statement),
	    		getString(R.string.Track_Order),
	    		getString(R.string.request_invoice),
	    		getString(R.string.tonnage_report),
//			    getString(R.string.product_loan)
            };
            
            for (String s : menuNames) {
            	itemsList.add(new com.movas.mcement.entity.MenuItem(s, null));
            }
            
            return null;
        }
        
        @Override
        protected void onPostExecute(Void result) {
        	
            mRecyclerView.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.GONE);
            mAdapter.addItems(itemsList);
            super.onPostExecute(result);
        }
    }
}