package com.movas.mcement.helper;

import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import org.json.JSONObject;

import com.movas.mservice12.R;
import com.movas.mcement.util.Constants;
import com.movas.mcement.view.ErrorAlert;
import com.movas.mservice12.CustomerRegistrationActivity;

import android.content.Context;
import android.content.Intent;

public class ErrorHandler {
	
	public ErrorHandler() {}
	
	public static void handleError(Context context, Throwable t) {
		
		if("Unauthorized".equals(t.getMessage()))
			showLoginView(context);
		else {
			new ErrorAlert(context).showErrorDialog(context.getString(R.string.error), getErrorMsg(t, context));
		}
	}
	
	private static void showLoginView(Context context) {
		Intent intent = new Intent(context, CustomerRegistrationActivity.class);
		intent.putExtra(Constants.BLOCKING, true);
		context.startActivity(intent);
	}
	
	public static String getErrorMsg(Throwable t, Context context) {
		
		String msg = "";
		try {
			msg = t.getMessage().toLowerCase();
		} catch (Exception e) {}

		if(t instanceof UnknownHostException)
			return context.getString(R.string.unknown_host);
		else if(t instanceof SocketException)
			return context.getString(R.string.connection_timedout);
		else if(t instanceof SocketTimeoutException)
			return context.getString(R.string.connection_timedout);
		else if(msg.equals("database error"))
			return context.getString(R.string.database_error);
		return msg;
	}
	
	public static void onFailure(Context context, int statusCode, Throwable e, JSONObject errorResponse) {
		new ErrorAlert(context).showErrorDialog(context.getString(R.string.error), getErrorMsg(e, context));
	}
	
	public static String translateError(Context context, Throwable e) {
		
		String error = "";
		
		error += getErrorMsg(e, context);
		//error = addSupportMessage(context, error);
		return error;
	}
	
//	private static String addSupportMessage(Context context, String error) {
//		if(!error.endsWith("."))
//			error += ".";
//			
//		error += " " + context.getString(R.string.support_message);
//		return error;
//	}
}