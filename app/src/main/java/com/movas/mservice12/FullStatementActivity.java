package com.movas.mservice12;

import com.movas.mcement.listener.DateChangeAdapter;
import com.movas.mcement.listener.DateChangeListener;
import com.movas.mcement.view.DateFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																								public class 																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																				FullStatementActivity extends ActionBarActivity implements DateChangeListener {
	
	static final int ITEMS = 2;
	MyAdapter mAdapter;
	ViewPager mPager;
	 
	public DateChangeAdapter adapter;
	 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_full_statement);
		
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FullStatementActivity.this.onBackPressed();
            }
        });
        
        mAdapter = new MyAdapter(getSupportFragmentManager());
		mPager = (ViewPager) findViewById(R.id.pager);
		mPager.setAdapter(mAdapter);
	}
	
	public static class MyAdapter extends FragmentPagerAdapter {
		public MyAdapter(FragmentManager fragmentManager) {
			super(fragmentManager);
		}
		
		@Override
		public int getCount() {
			return ITEMS;
		}
		
		@Override
		public Fragment getItem(int position) {
			switch (position) {
			case 0:
				return DateFragment.init(position);
			case 1:
				return DateFragment.init(position);
			default:
				return DateFragment.init(position);
			}
		}
	}
	
	public void setCurrentItem(int position) {
		mPager.setCurrentItem(position);
	}

	@Override
	public void addDateChangeListener(DateChangeAdapter adapter) {
		this.adapter = adapter;
	}
	

	public void onBackPressed() {
		
		new AlertDialog.Builder(this)
			.setMessage(getString(R.string.exit_confirmation))
			.setCancelable(false)
			.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					finish();
				}
			})
			.setNegativeButton(android.R.string.no, null)
			.show();
	}
}