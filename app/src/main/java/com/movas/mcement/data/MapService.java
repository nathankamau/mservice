package com.movas.mcement.data;

import com.movas.mcement.entity.Gps;
import com.movas.mcement.entity.Track;

import java.util.List;

public interface MapService {

    void save(Track track) throws Exception;

    List<Gps> fetch(String deliveryNoteNo) throws Exception;

    Gps TrackByDeliveryNoteNo(String deliveryNoteNo) throws Exception;

   List<Gps > fetchTrackByLocation(double latitude, double longitude) throws Exception;
}

