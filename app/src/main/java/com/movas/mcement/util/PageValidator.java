package com.movas.mcement.util;

public abstract class PageValidator {

	public abstract Boolean isValid();

}