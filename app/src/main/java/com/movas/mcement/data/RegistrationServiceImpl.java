package com.movas.mcement.data;

import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import org.json.JSONObject;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.movas.mservice12.MovaPay;

import cz.msebera.android.httpclient.entity.StringEntity;

public class RegistrationServiceImpl implements RegistrationService {
	
	@Override
	public void isRegistered(String phone, JsonHttpResponseHandler handler) throws JSONException, UnsupportedEncodingException {
		
		String url = UrlUtils.isRegisteredRoute;
		
		HttpClient client = HttpClient.instance();
		
		RequestParams params = new RequestParams();
		params.put("phone", phone);
		
		JSONObject jsonParams = new JSONObject();
        jsonParams.put("phone", phone);
        
        StringEntity entity = new StringEntity(jsonParams.toString());
        
        client.post(MovaPay.get(), url, entity, "application/json", handler);
	}
	
	@Override
	public void register(String firstName, String lastName, String middleName, String phone, String geoLocation, JsonHttpResponseHandler handler) throws JSONException, UnsupportedEncodingException {
		
		String url = UrlUtils.registerRoute;
		
		HttpClient client = HttpClient.instance();
		
		JSONObject jsonParams = new JSONObject();
        jsonParams.put("phone", phone);
        jsonParams.put("firstname", firstName);
        jsonParams.put("lastname", lastName);
        jsonParams.put("middlename", middleName);
        jsonParams.put("geolocation", geoLocation);
        
        StringEntity entity = new StringEntity(jsonParams.toString());
        
        client.post(MovaPay.get(), url, entity, "application/json", handler);
        
	}
}