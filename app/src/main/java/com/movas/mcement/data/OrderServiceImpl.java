package com.movas.mcement.data;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.movas.mcement.entity.OrderItem;
import com.movas.mcement.util.Constants;

import cz.msebera.android.httpclient.entity.StringEntity;

public class OrderServiceImpl implements OrderService {
	
	@Override
	public void confirmDelivery(String token, String lponumber, String comment, Boolean isComplaint, String id, JsonHttpResponseHandler handler) throws UnsupportedEncodingException, JSONException {
		
		String url ="http://193.34.144.238:8181/mservice-proxy/api/confirmDelivery";
		
		HttpClient client = HttpClient.instance();
		
		RequestParams params = new RequestParams();
		params.put("lponumber", lponumber);
		params.put("comment", comment);
		params.put("positive", isComplaint.toString());
		params.put("token", token);
		params.setUseJsonStreamer(true);
		client.getAsyncClient().post(url, params, handler);
	}
	
	@Override
	public void placeOrder(String token, String sapClientId, String lponumber, String pickup_location,
			String todeliver, String deliveryLocation,
			String vehicleRegistration, List<OrderItem> orders, String id,
			JsonHttpResponseHandler handler)
			throws UnsupportedEncodingException, JSONException {
		
		//String url = UrlUtils.placeOrderRoute;

        String url = "http://193.34.144.238:8181/mservice-proxy/api/placeOrder";


        //HttpClient client= HttpClient.instance();

		AsyncHttpClient client = new AsyncHttpClient();


		JSONObject jsonParams = new JSONObject();
		jsonParams.put("userid", Constants.USER_ID);
		jsonParams.put("apikey", Constants.API_KEY);
		jsonParams.put("timestamp", new Date().getTime() + "");
		jsonParams.put("id", id);
		jsonParams.put("responseurl", "");
		jsonParams.put("lponumber", lponumber);
		jsonParams.put("sapClientId", sapClientId);
		jsonParams.put("todeliver", todeliver);
		jsonParams.put("deliveryLocation", deliveryLocation);
		jsonParams.put("location", pickup_location);
        jsonParams.put("token",token);

		
		
		JSONArray objects = new JSONArray();
		for(int i = 0; i < orders.size(); i++) {
			JSONObject obj = new JSONObject();
			obj.put("tonnage", orders.get(i).getTonnage());
			obj.put("name", orders.get(i).getBrand());
			objects.put(obj);
		}
		
		JSONArray vehicleNos = new JSONArray();
		
		String nos[] = vehicleRegistration.split(",");
		
		for(int x = 0; x < nos.length; x++) {
			JSONObject ob = new JSONObject();
			ob.put("no", nos[x]);
			vehicleNos.put(ob);
		}
		
		jsonParams.put("vehicleRegistration", vehicleNos);
		
		jsonParams.put("orders", objects);
		
//		String encoded = Base64.encode(jsonParams.toString(), "UTF-8");
//		Log.v("OrderServiceImpl", encoded);

		//jsonParams.put("token", token);
		
		StringEntity entity = new StringEntity(jsonParams.toString());

        RequestParams params = new RequestParams();
        params.put("token", token);
        params.put("json", jsonParams.toString());
		params.setUseJsonStreamer(true);

		client.post(url, params, handler);


       //client.post(MovaPay.get(), url, entity, "application/json", handler);
	}
	
	@Override
	public void loadMyOrders(String token, JsonHttpResponseHandler handler) throws UnsupportedEncodingException, JSONException {

		String url ="http://193.34.144.238:8181/mservice-proxy/api/customerOrders";

		HttpClient client = HttpClient.instance();

		RequestParams params = new RequestParams();
		params.put("token", token);
		
		client.getAsyncClient().get(url, params, handler);
		
	}
	
	@Override
	public void loadMyOrderItems(String token, String orderID, JsonHttpResponseHandler handler) throws UnsupportedEncodingException, JSONException {
		
		String url = UrlUtils.url + "/api/orders/my-orders/items/" + orderID;
		
		HttpClient client = HttpClient.instance();

		RequestParams params = new RequestParams();
		params.put("token", token);
		params.setUseJsonStreamer(true);
		
		client.getAsyncClient().get(url, params, handler);
	}
	
	@Override
	public void saveMultiOrders(String token, JSONObject jsonObject, JsonHttpResponseHandler handler) throws UnsupportedEncodingException, JSONException {
		
		String url ="http://193.34.144.238:8181/mservice-proxy/api/placeOrder/multiple";
		
		//HttpClient client = HttpClient.instance();
		AsyncHttpClient client = new AsyncHttpClient();
		RequestParams params = new RequestParams();
		params.put("token", token);
		params.setUseJsonStreamer(true);
		Iterator<String> keys = jsonObject.keys();
		
		while(keys.hasNext()) {
			String key = keys.next();
			params.put(key, jsonObject.get(key).toString());

		}
		
		//String encoded = Base64.encode(jsonObject.toString(), "UTF-8");
		//Log.v("OrderServiceImpl", encoded);
		
		client.post(url, params, handler);
	}

	@Override
	public void loadTonnageHistory(String token, String fromDate, String toDate, JsonHttpResponseHandler handler) throws UnsupportedEncodingException, JSONException {
		
		String url = UrlUtils.url + "/api/order/ordered-tonnage";
		
		HttpClient client = HttpClient.instance();
				
		RequestParams params = new RequestParams();
		params.put("token", token);
		params.put("from_date", fromDate);
		params.put("to_date", toDate);
		params.setUseJsonStreamer(true);
		
		client.getAsyncClient().post(url, params, handler);
	}
}