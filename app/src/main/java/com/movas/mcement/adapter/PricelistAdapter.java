package com.movas.mcement.adapter;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;
import com.movas.mservice12.R;
import com.movas.mcement.entity.Product;

public class PricelistAdapter extends RecyclerView.Adapter<PricelistAdapter.ViewHolder> {

    private List<Product> products;
    private int rowLayout;
    private Activity mAct;

    public PricelistAdapter(List<Product> applications, int rowLayout, Activity act) {
        this.products = applications;
        this.rowLayout = rowLayout;
        this.mAct = act;
    }


    public void clear() {
        int size = this.products.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                products.remove(0);
            }

            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void addItems(List<Product> applications) {
        this.products.addAll(applications);
        this.notifyItemRangeInserted(0, applications.size() - 1);
    }
    
    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(rowLayout, viewGroup, false);
        ViewHolder holder = new ViewHolder(v, mAct);
        return holder;
    }
    
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        final Product product = products.get(i);
        viewHolder.productView.setText(product.getName());
        viewHolder.priceView.setText(product.getPrice().toString());
    }
    
    @Override
    public int getItemCount() {
        return products == null ? 0 : products.size();
    }
    
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    	
    	public TextView productView;
    	public TextView priceView;
    	
        Activity hAct;
        
        public ViewHolder(View itemView, Activity mAct) {
            super(itemView);
            productView = (TextView) itemView.findViewById(R.id.productView);
            priceView = (TextView) itemView.findViewById(R.id.priceView);
            hAct = mAct;
            productView.setOnClickListener(this);
        }
        
		@Override
		public void onClick(View v) {
		}
		
		@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
		private void startActivity16(Intent i){
	    	ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(hAct);
	    	hAct.startActivity(i, transitionActivityOptions.toBundle());
	    }
    }
}