package com.movas.mcement.data;

import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import com.loopj.android.http.JsonHttpResponseHandler;

public interface PriceService {

	public void loadPrices(String token, String id, String location, JsonHttpResponseHandler handler)
			throws UnsupportedEncodingException, JSONException;
}
