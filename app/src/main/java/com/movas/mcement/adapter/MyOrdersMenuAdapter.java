package com.movas.mcement.adapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.movas.mservice12.R;
import com.movas.mcement.entity.Order;
import com.movas.mservice12.MyOrderDetailsActivity;
import com.movas.mservice12.MyOrdersActivity;

public class MyOrdersMenuAdapter extends RecyclerView.Adapter<MyOrdersMenuAdapter.ViewHolder> {
	
    private List<Order> orders;
    private int rowLayout;
    private MyOrdersActivity mAct;
    
    public MyOrdersMenuAdapter(List<Order> orders, int rowLayout, MyOrdersActivity act) {
        this.orders = orders;
        this.rowLayout = rowLayout;
        this.mAct = act;
    }
    
    public void clearApplications() {
        int size = this.orders.size();
        if (size > 0) {
        	
            for (int i = 0; i < size; i++) {
                orders.remove(0);
            }
            
            this.notifyItemRangeRemoved(0, size);
        }
    }
    
    public Order getOrder(int pos) {
    	return orders.get(pos);
    }
    
    public void addOrders(List<Order> orders) {
        this.orders.addAll(orders);
        this.notifyItemRangeInserted(0, orders.size() - 1);
    }
    
    public void addOrder(Order order) {
    	this.orders.add(order);
    	this.notifyDataSetChanged();
    }
    
    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(rowLayout, viewGroup, false);
        ViewHolder holder = new ViewHolder(v, mAct);
        return holder;
    }
    
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        final Order item = orders.get(i);
        viewHolder.lpoNo.setText("LPO: " + item.getLponumber());
        
		try {
			
		    String reformattedStr = new SimpleDateFormat("dd/MM/yyyy")
			    .format(new SimpleDateFormat("yyyy-MM-dd")
			    .parse(item.getDatecreated()));
		    viewHolder.dateView.setText(reformattedStr);
		    
		} catch (ParseException e) {}
		
        String status = item.getStatus();
        
        if("Awaiting Collection".equals(status))
        	viewHolder.statusImageView.setImageResource(R.drawable.order_processed); 
        else if("First Weight".equals(status))
        	viewHolder.statusImageView.setImageResource(R.drawable.first_weight); 
        else if("Second Weight".equals(status))
        	viewHolder.statusImageView.setImageResource(R.drawable.second_weight); 
        else if("Awaiting Delivery Confirmation".equals(status))
        	viewHolder.statusImageView.setImageResource(R.drawable.confirm_delivery);
    }
    
    @Override
    public int getItemCount() {
        return orders == null ? 0 : orders.size();
    }
    
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    	
    	public TextView lpoNo;
    	public TextView dateView;
    	public ImageView statusImageView;
        MyOrdersActivity hAct;
        
        public ViewHolder(View itemView, MyOrdersActivity mAct) {
            super(itemView);
            
            lpoNo = (TextView) itemView.findViewById(R.id.lpoNoView);
            dateView = (TextView) itemView.findViewById(R.id.dateView);
            statusImageView = (ImageView) itemView.findViewById(R.id.statusImageView);
            hAct = mAct;
            itemView.setOnClickListener(this);
        }
        
		@Override
		public void onClick(View v) {
			
			int position = getPosition();
			
			Intent intent = new Intent(hAct, MyOrderDetailsActivity.class);
			Order order = hAct.getOrder(position);
			
			intent.putExtra("orderrequestid", order.getOrderrequestid());
			intent.putExtra("lponumber", order.getLponumber());
			intent.putExtra("status", order.getStatus());
			intent.putExtra("vehiclereg", order.getVehiclereg());
			intent.putExtra("collectionpoint", order.getCollectionpoint());
			intent.putExtra("deliverylocation", order.getDeliverylocation());
			
			//todo put order details in intent
			
			start(intent);
			
		}
		
		protected void start(Intent i) {
			if(i != null) {
				
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
					startActivity16(i);
				} else
					hAct.startActivity(i);
			}
		}

		@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
		private void startActivity16(Intent i){
	    	ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(hAct);
	    	hAct.startActivity(i, transitionActivityOptions.toBundle());
	    }
    }
}