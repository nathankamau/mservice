package com.movas.mcement.adapter;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;
import com.movas.mservice12.R;
import com.movas.mcement.entity.OrderItem;
import com.movas.mservice12.OrderItemActivity;

public class OrderItemsAdapter extends RecyclerView.Adapter<OrderItemsAdapter.ViewHolder> {
	
    private List<OrderItem> orderItems;
    private int rowLayout;
    private int position;
    private Activity mAct;
    
    public OrderItemsAdapter(List<OrderItem> orderItems, int rowLayout, Activity act) {
        this.orderItems = orderItems;
        this.rowLayout = rowLayout;
        this.mAct = act;
    }
    
    public void clearOrderItems() {
        int size = this.orderItems.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
            	orderItems.remove(0);
            }

            this.notifyItemRangeRemoved(0, size);
        }
    }
    
    public List<OrderItem> getItems() {
    	return orderItems;
    }

    public void addItems(List<OrderItem> orderItems) {
        this.orderItems.addAll(orderItems);
        this.notifyItemRangeInserted(0, orderItems.size() - 1);
    }
    
    public void addItem(OrderItem orderItem) {
    	this.orderItems.add(orderItem);
    	this.notifyItemInserted(orderItems.size() -1);
    }
    
    public void removeItem(int position) {
    	this.orderItems.remove(position);
    	this.notifyDataSetChanged();
    }
    
    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(rowLayout, viewGroup, false);
        ViewHolder holder = new ViewHolder(v, mAct);
        return holder;
    }
    
    @Override
    public void onViewRecycled(ViewHolder holder) {
        holder.itemView.setOnLongClickListener(null);
        super.onViewRecycled(holder);
    }
    
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        final OrderItem item = orderItems.get(i);
        viewHolder.brand.setText(item.getBrand());
        viewHolder.tonnage.setText(item.getTonnage() + " " + mAct.getString(R.string.tonnes));
        
        viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                setPosition(viewHolder.getPosition());
                return false;
            }
        });
    }
    
    protected void setPosition(int position) {
		this.position = position;
	}
    
    public Integer getPosition() {
    	return this.position;
    }

	@Override
    public int getItemCount() {
        return orderItems == null ? 0 : orderItems.size();
    }
    
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnCreateContextMenuListener {
    	
    	public TextView brand;
        public TextView tonnage;
        public TextView lpoNo;
        public TextView vehicleNo;
        Activity hAct;
        
        public ViewHolder(View itemView, Activity mAct) {
            super(itemView);
            brand = (TextView) itemView.findViewById(R.id.brand);
            tonnage = (TextView) itemView.findViewById(R.id.tonnage);
            hAct = mAct;
            
            if(hAct instanceof OrderItemActivity)
            	itemView.setOnCreateContextMenuListener(this);
        }
        
		@Override
		public void onClick(View v) {
		}
		
		@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
		private void startActivity16(Intent i){
	    	ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(hAct);
	    	hAct.startActivity(i, transitionActivityOptions.toBundle());
	    }

		@Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
			
	        menu.add(0, v.getId(), 0, R.string.edit);
	        menu.add(0, v.getId(), 1, R.string.delete);
	        
        }
    }
}