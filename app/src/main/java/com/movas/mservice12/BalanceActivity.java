package com.movas.mservice12;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.movas.mcement.data.BalanceService;
import com.movas.mcement.data.BalanceServiceImpl;
import com.movas.mcement.helper.DialogHelper;
import com.movas.mcement.helper.ErrorHandler;
import com.movas.mcement.util.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;

import cz.msebera.android.httpclient.Header;

public class BalanceActivity extends ActionBarActivity {

	TextView balance;
	TextView bookBalance;

	LinearLayout balance_container;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_balance);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        balance_container = (LinearLayout) findViewById(R.id.balance_container);

        balance = (TextView) findViewById(R.id.balance);
        bookBalance = (TextView) findViewById(R.id.bookBalance);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                BalanceActivity.this.onBackPressed();
            }
        });

        loadBalance();
	}

	private void loadBalance() {

		DialogHelper.showProgressDialog(this, getString(R.string.please_wait), false);

		BalanceService service = new BalanceServiceImpl();

        SharedPreferences prefs = getApplication().getSharedPreferences(Constants.MovaPay, Context.MODE_PRIVATE);
        String token = prefs.getString("token", "");

		try {
			service.getBalance(token,new JsonHttpResponseHandler() {

				@Override
				public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
					super.onSuccess(statusCode, headers, response);

					DialogHelper.hideProgressDialog();

					try {

						Integer status = response.getInt("status");

						if(status == 0) {


						double bal = response.getDouble("availableBalance");
							DecimalFormat formatter = new DecimalFormat("#,###.00");
							String availableBalStr = formatter.format(bal);

							if(availableBalStr.startsWith("."))
								availableBalStr = "0" + availableBalStr;

							balance_container.setVisibility(View.VISIBLE);
							balance.setText("KES " + availableBalStr);

							double bookBal = response.getDouble("bookBalance");
							String bookBalanceStr = formatter.format(bookBal);

							if(bookBalanceStr.startsWith("."))
								bookBalanceStr = "0" + bookBalanceStr;

							bookBalance.setText("KES " + bookBalanceStr);

						} else {
							ErrorHandler.handleError(BalanceActivity.this, new Throwable(response.getString("message") ));
						}

					} catch (JSONException e1) {}
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
					super.onFailure(statusCode, headers, responseString, throwable);
					DialogHelper.hideProgressDialog();
					ErrorHandler.handleError(BalanceActivity.this, throwable);
				}

				@Override
				public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
 					super.onFailure(statusCode, headers, throwable, errorResponse);

					DialogHelper.hideProgressDialog();
					ErrorHandler.handleError(BalanceActivity.this, throwable);
				}

			});
		} catch (UnsupportedEncodingException | JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_refresh, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		if (id == R.id.action_refresh) {

			balance_container.setVisibility(View.GONE);
			loadBalance();
			return true;


		}
		return super.onOptionsItemSelected(item);
	}
}