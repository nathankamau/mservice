package com.movas.mcement.wizard.ui.order;

import com.movas.mservice12.R;
import com.movas.mcement.wizard.model.order.OrderDetailsPage;
import com.movas.mcement.wizard.ui.PageFragmentCallbacks;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

public class OrderDetailsFragment extends Fragment {
	
    private static final String ARG_KEY = "key";
    
    private PageFragmentCallbacks mCallbacks;
    private String mKey;
    private OrderDetailsPage mPage;
    private TextView tonnage;
    
    public static OrderDetailsFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);

        OrderDetailsFragment fragment = new OrderDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    public OrderDetailsFragment() {
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = (OrderDetailsPage) mCallbacks.onGetPage(mKey);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_page_order_details, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());
        
        tonnage = (TextView) rootView.findViewById(R.id.tonnage);
        tonnage.setText(mPage.getData().getString(OrderDetailsPage.TONNAGE_KEY));
        
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (!(activity instanceof PageFragmentCallbacks)) {
            throw new ClassCastException("Activity must implement PageFragmentCallbacks");
        }

        mCallbacks = (PageFragmentCallbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        
        tonnage.addTextChangedListener(getTextWatcher(OrderDetailsPage.TONNAGE_KEY));
    }
    
    protected TextWatcher getTextWatcher(final String name) {
    	return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1,
                    int i2) {
            }
            
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            
            @Override
            public void afterTextChanged(Editable editable) {
                mPage.getData().putString(name,
                        (editable != null) ? editable.toString() : null);
                mPage.notifyDataChanged();
            }
        };
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);

        if (tonnage != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            if (!menuVisible) {
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            }
        }
    }
}
