package com.movas.mservice12;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.movas.mcement.data.OrderService;
import com.movas.mcement.data.OrderServiceImpl;
import com.movas.mcement.helper.DialogHelper;
import com.movas.mcement.helper.ErrorHandler;
import com.movas.mcement.listener.ObjectChangeListener;
import com.movas.mcement.util.Constants;
import com.movas.mcement.wizard.MultiOrderWizardModel;
import com.movas.mcement.wizard.model.AbstractWizardModel;
import com.movas.mcement.wizard.model.ModelCallbacks;
import com.movas.mcement.wizard.model.Page;
import com.movas.mcement.wizard.model.order.MultiOrderDetailsPage;
import com.movas.mcement.wizard.model.order.MultiOrderPage;
import com.movas.mcement.wizard.model.order.OrderDetailsPage;
import com.movas.mcement.wizard.ui.PageFragmentCallbacks;
import com.movas.mcement.wizard.ui.ReviewFragment;
import com.movas.mcement.wizard.ui.StepPagerStrip;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import cz.msebera.android.httpclient.Header;

public class MultiOrderActivity extends ActionBarActivity implements PageFragmentCallbacks, ReviewFragment.Callbacks, ModelCallbacks {
	
	ObjectChangeListener objectChangeListener;
	private ViewPager mPager;
    private MyPagerAdapter mPagerAdapter;
    
    private boolean mEditingAfterReview;
    
    private AbstractWizardModel mWizardModel = new MultiOrderWizardModel(this);
    
    private boolean mConsumePageSelectedEvent;
    
    private Button mNextButton;
    private Button mPrevButton;
    
    private List<Page> mCurrentPageSequence;
    private StepPagerStrip mStepPagerStrip;
    
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_item);
        
        if (savedInstanceState != null) {
            mWizardModel.load(savedInstanceState.getBundle("model"));
        }
        
        mWizardModel.registerListener(this);
        
        if(getIntent().getBooleanExtra(Constants.EDIT, false)) {
        	
        	String brand = getIntent().getStringExtra("brand");
        	String tonnage = getIntent().getStringExtra("tonnage");
        	
        	mWizardModel.getCurrentPageSequence().get(0).getData().putString(Page.SIMPLE_DATA_KEY, brand);
        	mWizardModel.getCurrentPageSequence().get(1).getData().putString(OrderDetailsPage.TONNAGE_KEY, tonnage);
        }
        
        mPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(mPagerAdapter);
        mStepPagerStrip = (StepPagerStrip) findViewById(R.id.strip);
        mStepPagerStrip.setOnPageSelectedListener(new StepPagerStrip.OnPageSelectedListener() {
            @Override
            public void onPageStripSelected(int position) {
                position = Math.min(mPagerAdapter.getCount() - 1, position);
                if (mPager.getCurrentItem() != position) {
                    mPager.setCurrentItem(position);
                }
            }
        });

        mNextButton = (Button) findViewById(R.id.next_button);
        mPrevButton = (Button) findViewById(R.id.prev_button);

        mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                mStepPagerStrip.setCurrentPage(position);
                
                if (mConsumePageSelectedEvent) {
                    mConsumePageSelectedEvent = false;
                    return;
                }

                mEditingAfterReview = false;
                updateBottomBar();
            }
        });

        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            	
            	if(mPager.getCurrentItem() == 3) {

            		if(mWizardModel.getCurrentPageSequence().get(3).isValid())
	            		if (mEditingAfterReview) {
	                        mPager.setCurrentItem(mPagerAdapter.getCount() - 1);
	                    } else {
	                        mPager.setCurrentItem(mPager.getCurrentItem() + 1);
	                    }
            		
            	}else if (mPager.getCurrentItem() == mCurrentPageSequence.size()) {
            		
            		String pickupLocation = mWizardModel.getCurrentPageSequence().get(0).getData().getString(Page.SIMPLE_DATA_KEY);
            		ArrayList<String> brands = mWizardModel.getCurrentPageSequence().get(1).getData().getStringArrayList(Page.SIMPLE_DATA_KEY);
            		String tonnage[] = mWizardModel.getCurrentPageSequence().get(2).getData().getStringArray(MultiOrderPage.ORDER_TONNAGES);
            		
            		Bundle details = mWizardModel.getCurrentPageSequence().get(3).getData();
            		
            		String lpoNo = details.getString(MultiOrderDetailsPage.LPO_NO_KEY);
            		String transMethod = details.getString(MultiOrderDetailsPage.TRANSPORT_METHOD_KEY);
            		
            		String deliveryLocation = details.getString(MultiOrderDetailsPage.DELIVERY_LOCATION_KEY);
            		
            		String[] vehicleNos = details.getStringArray(MultiOrderDetailsPage.VEHICLE_NOS_ARRAY);
            		String[] vehicleTonnages = details.getStringArray(MultiOrderDetailsPage.TONNAGE_ARRAY);
            		String[] vehicleBrands = details.getStringArray(MultiOrderDetailsPage.BRANDS_ARRAY);
            		String[] vehicleTrips = details.getStringArray(MultiOrderDetailsPage.TRIPS_ARRAY);
            		
            		JSONObject object = new JSONObject();
            		try {
            			
						object.put("pickup_location", pickupLocation);
						object.put("transport_method", transMethod);
						object.put("lpo_no", lpoNo);
						
						JSONArray brandsArray = new JSONArray();
						
						for (int i = 0; i < brands.size(); i++) {
							JSONObject bo = new JSONObject();
							bo.put("name", brands.get(i));
							bo.put("tonnage", tonnage[i]);
							brandsArray.put(bo);
						}
						
						object.put("brands", brandsArray);
						
						if(transMethod.equals(getString(R.string.to_be_collected))) {
							
							
							
							JSONArray detailsArray = new JSONArray();
							
							for(int x = 0; x < vehicleNos.length; x++) {
								JSONObject vn = new JSONObject();
								vn.put("vehicle_no", vehicleNos[x]);
								vn.put("tonnage", vehicleTonnages[x]);
								vn.put("brand", vehicleBrands[x]);
								vn.put("trips", vehicleTrips[x]);
								detailsArray.put(vn);
							}
							
							object.put("details", detailsArray);
							
						} else {
							object.put("delivery_location", deliveryLocation);
						}
						
					} catch (JSONException e) {}
            		
            		DialogHelper.showProgressDialog(MultiOrderActivity.this, getString(R.string.please_wait), false);

                    SharedPreferences prefs = getApplication().getSharedPreferences(Constants.MovaPay, Context.MODE_PRIVATE);
                    String token = prefs.getString("token", "");
            		
            		OrderService service = new OrderServiceImpl();
            		try {
						service.saveMultiOrders(token, object, new JsonHttpResponseHandler() {
							@Override
							public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
								super.onSuccess(statusCode, headers, response);
																
								DialogHelper.hideProgressDialog();
								
								try {
									
									String status = response.getString("status");
									
									if("0".equals(status)) {
										Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.order_success_message) , Toast.LENGTH_LONG);
										toast.setGravity(Gravity.TOP, 0, 0);
										toast.show();
										finish();
										
									} else {
										ErrorHandler.handleError(MultiOrderActivity.this, new Throwable(response.getString("message")));
									}
									
								} catch (JSONException e1) {}
							}
							
							@Override
							public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
								super.onFailure(statusCode, headers, responseString, throwable);
								
								DialogHelper.hideProgressDialog();
								throwable.printStackTrace();
								ErrorHandler.handleError(MultiOrderActivity.this, throwable);
							}
							
							@Override
							public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
								super.onFailure(statusCode, headers, throwable, errorResponse);
								
								DialogHelper.hideProgressDialog();
								ErrorHandler.handleError(MultiOrderActivity.this, throwable);
							}
						});
					} catch (UnsupportedEncodingException | JSONException e) {}
            		
            		
                } else {
                	
                    if (mEditingAfterReview) {
                        mPager.setCurrentItem(mPagerAdapter.getCount() - 1);
                    } else {
                        mPager.setCurrentItem(mPager.getCurrentItem() + 1);
                    }
                }
            }
        });
        
        mPrevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPager.setCurrentItem(mPager.getCurrentItem() - 1);
            }
        });
        
        onPageTreeChanged();
        updateBottomBar();
    }
    
    @Override
    public void onPageTreeChanged() {
        mCurrentPageSequence = mWizardModel.getCurrentPageSequence();
        recalculateCutOffPage();
        mStepPagerStrip.setPageCount(mCurrentPageSequence.size() + 1); // + 1 = review step
        mPagerAdapter.notifyDataSetChanged();
        updateBottomBar();
    }
    
    private void updateBottomBar() {
    	
        int position = mPager.getCurrentItem();
        
        if (position == mCurrentPageSequence.size()) {
            mNextButton.setText(R.string.submit_order);
            mNextButton.setBackgroundResource(R.drawable.finish_background);
            mNextButton.setTextColor(getResources().getColor(android.R.color.white));
        } else {
            mNextButton.setText(mEditingAfterReview ? R.string.review : R.string.next);
            mNextButton.setBackgroundResource(R.drawable.selectable_item_background);
            TypedValue v = new TypedValue();
            getTheme().resolveAttribute(android.R.attr.textAppearanceMedium, v, true);
            mNextButton.setTextAppearance(this, v.resourceId);
            mNextButton.setEnabled(position != mPagerAdapter.getCutOffPage());
        }
        
        mPrevButton.setVisibility(position <= 0 ? View.INVISIBLE : View.VISIBLE);
    }
    
    @Override
    public void onPageDataChanged(Page page) {
        if (page.isRequired()) {
            if (recalculateCutOffPage()) {
                mPagerAdapter.notifyDataSetChanged();
                updateBottomBar();
            }
        }
        
        if(mPager.getCurrentItem() == 1) {
        	//get page at 2
        	MultiOrderPage multiOrderPage = (MultiOrderPage) mWizardModel.getCurrentPageSequence().get(2);
        	multiOrderPage.notifySelectedOrdersChanged();
        }
    }
    
    @Override
    public Page onGetPage(String key) {
        return mWizardModel.findByKey(key);
    }

    private boolean recalculateCutOffPage() {
        // Cut off the pager adapter at first required page that isn't completed
        int cutOffPage = mCurrentPageSequence.size() + 1;
        for (int i = 0; i < mCurrentPageSequence.size(); i++) {
            Page page = mCurrentPageSequence.get(i);
            if (page.isRequired() && !page.isCompleted()) {
                cutOffPage = i;
                break;
            }
        }

        if (mPagerAdapter.getCutOffPage() != cutOffPage) {
            mPagerAdapter.setCutOffPage(cutOffPage);
            return true;
        }

        return false;
    }

    public class MyPagerAdapter extends FragmentStatePagerAdapter {
        private int mCutOffPage;
        private Fragment mPrimaryItem;

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            if (i >= mCurrentPageSequence.size()) {
                return new ReviewFragment();
            }

            return mCurrentPageSequence.get(i).createFragment();
        }

        @Override
        public int getItemPosition(Object object) {
            // TODO: be smarter about this
            if (object == mPrimaryItem) {
                // Re-use the current fragment (its position never changes)
                return POSITION_UNCHANGED;
            }

            return POSITION_NONE;
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);
            mPrimaryItem = (Fragment) object;
        }

        @Override
        public int getCount() {
            if (mCurrentPageSequence == null) {
                return 0;
            }
            return Math.min(mCutOffPage + 1, mCurrentPageSequence.size() + 1);
        }

        public void setCutOffPage(int cutOffPage) {
            if (cutOffPage < 0) {
                cutOffPage = Integer.MAX_VALUE;
            }
            mCutOffPage = cutOffPage;
        }
        
        public int getCutOffPage() {
            return mCutOffPage;
        }
    }
    
    @Override
	public void fireObjectChanged(String name, Object value) {
		if(objectChangeListener != null)
			objectChangeListener.onChange(name, value);
	}
    
	@Override
	public void addObjectChangeListener(ObjectChangeListener listener) {
		this.objectChangeListener = listener;
	}
	
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mWizardModel.unregisterListener(this);
    }
    
    @Override
    public AbstractWizardModel onGetModel() {
        return mWizardModel;
    }
    
    @Override
    public void onEditScreenAfterReview(String key) {
        for (int i = mCurrentPageSequence.size() - 1; i >= 0; i--) {
            if (mCurrentPageSequence.get(i).getKey().equals(key)) {
                mConsumePageSelectedEvent = true;
                mEditingAfterReview = true;
                mPager.setCurrentItem(i);
                updateBottomBar();
                break;
            }
        }
    }
    
    public void onBackPressed() {
    	
		new AlertDialog.Builder(this)
			.setMessage(getString(R.string.exit_confirmation))
			.setCancelable(false)
			.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					finish();
				}
			})
			.setNegativeButton(android.R.string.no, null)
			.show();
	}
}