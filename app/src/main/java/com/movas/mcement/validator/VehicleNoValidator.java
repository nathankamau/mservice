package com.movas.mcement.validator;

import java.lang.annotation.Annotation;
import android.text.TextUtils;
import eu.inmite.android.lib.validations.form.validators.BaseValidator;

public class VehicleNoValidator extends BaseValidator<CharSequence> {

	@Override
	public boolean validate(Annotation annotation, CharSequence input) {

		if(TextUtils.isEmpty(input))
			return false;
		
		//Must contain both letters and numbers
		
		if(input.toString().matches("^(?=.*\\d)(?=.*[a-zA-Z]).{5,10}$"))
			return true;
		
		return false;
	}
}