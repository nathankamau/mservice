package com.movas.mcement.adapter;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;
import com.movas.mservice12.R;
import com.movas.mcement.entity.Statement;

public class MiniStatementAdapter extends RecyclerView.Adapter<MiniStatementAdapter.ViewHolder> {

    private List<Statement> statements;
    private int rowLayout;
    private Activity mAct;

    public MiniStatementAdapter(List<Statement> applications, int rowLayout, Activity act) {
        this.statements = applications;
        this.rowLayout = rowLayout;
        this.mAct = act;
    }


    public void clear() {
        int size = this.statements.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                statements.remove(0);
            }

            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void addItems(List<Statement> applications) {
        this.statements.addAll(applications);
        this.notifyItemRangeInserted(0, applications.size() - 1);
    }
    
    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(rowLayout, viewGroup, false);
        ViewHolder holder = new ViewHolder(v, mAct);
        return holder;
    }
    
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        Statement statement = statements.get(i);
        viewHolder.textView.setText(statement.getText());
        viewHolder.dateView.setText(statement.getDate());
        viewHolder.amountView.setText("KES " + statement.getAmount());
        viewHolder.typeView.setText(statement.getType());
    }
    
    @Override
    public int getItemCount() {
        return statements == null ? 0 : statements.size();
    }
    
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    	
    	public TextView textView;
    	public TextView dateView;
    	public TextView amountView;
    	public TextView typeView;
    	
        Activity hAct;
        
        public ViewHolder(View itemView, Activity mAct) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.textView);
            dateView = (TextView) itemView.findViewById(R.id.dateView);
            amountView = (TextView) itemView.findViewById(R.id.amountView);
            typeView = (TextView) itemView.findViewById(R.id.typeView);
            
            hAct = mAct;
            itemView.setOnClickListener(this);
        }
        
		@Override
		public void onClick(View v) {
		}
		
		@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
		private void startActivity16(Intent i){
	    	ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(hAct);
	    	hAct.startActivity(i, transitionActivityOptions.toBundle());
	    }
    }
}