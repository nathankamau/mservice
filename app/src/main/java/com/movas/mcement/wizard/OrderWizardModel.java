package com.movas.mcement.wizard;

import com.movas.mcement.wizard.model.AbstractWizardModel;
import com.movas.mcement.wizard.model.Page;
import com.movas.mcement.wizard.model.PageList;
import com.movas.mcement.wizard.model.SingleFixedChoicePage;
import com.movas.mcement.wizard.model.order.OrderDetailsPage;
import android.content.Context;

public class OrderWizardModel extends AbstractWizardModel {
	
    public OrderWizardModel(Context context) {
        super(context);
    }
    
    public OrderWizardModel(Context context, Object location) {
    	super(context, location);
    }
    
    @Override
    protected PageList onNewRootPageList() {
    	
    	Page p = new SingleFixedChoicePage(this, "Select Brand")
	        .setChoices(
        		"Nguvu Bagged",
        		"Nguvu Bulk",
        		"Nguvu Jumbo",
        		"Powerplus Bagged",
        		"Powerplus Jumbo",
        		"Powermax Bagged",
        		"Powermax Jumbo Bags",
        		"Powercrete Bagged",
        		"Powercrete Jumbo Bags"

			)
            .setRequired(true);
    	
    	String location = "Athi-River";
    	
    	if(variable != null)
    		location = (String) variable;
    	
    	if("Mombasa".equals(location)) {
    		p = new SingleFixedChoicePage(this, "Brand")
    			.setChoices(
            		"Nguvu Bagged",
            		"Nguvu Bulk",
            		"Powerplus Bagged",
            		"Powerplus Bulk",
            		"Powermax Bagged",
            		"Powermax Bulk",
            		"Powercrete Bagged",
            		"Powercrete Bulk"
            	).setRequired(true);
    	} else if("Kitui Road".equals(location)) {
    		p = new SingleFixedChoicePage(this, "Brand")
    			.setChoices(
            		"Powerplus Bulk",
            		"Powermax Bulk"
            	).setRequired(true);
    	} else if ("Export".equals(location)) {
    		p = new SingleFixedChoicePage(this, "Brand")
    			.setChoices(
            		"Supaset",
            		"Tembo",
            		"Powerplus"
            	).setRequired(true);
    	}
    	
    	
        return new PageList(
	                p,
                new OrderDetailsPage(this, "Enter Tonnage").setRequired(true)
        );
    }
}