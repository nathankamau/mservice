package com.movas.mcement.data;


import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;

public class PointsServiceImpl implements PointsService {
    @Override
    public void getPoints(String accountNumber , String token,JsonHttpResponseHandler handler) throws UnsupportedEncodingException, JSONException {

        String url ="http://193.34.144.238:8181/mservice-proxy/api/points";

        HttpClient client = HttpClient.instance();

        RequestParams params = new RequestParams();
        params.put("accountNumber",accountNumber);
        params.put("token",token);
        params.setUseJsonStreamer(true);

        client.getAsyncClient().get(url, params, handler);
    }
}

