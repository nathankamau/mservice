package com.movas.mservice12;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.movas.mcement.adapter.MiniStatementAdapter;
import com.movas.mcement.data.StatementService;
import com.movas.mcement.data.StatementServiceImpl;
import com.movas.mcement.entity.Statement;
import com.movas.mcement.helper.DialogHelper;
import com.movas.mcement.helper.ErrorHandler;
import com.movas.mcement.util.Constants;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import cz.msebera.android.httpclient.Header;

public class MiniStatementActivity extends ActionBarActivity {
	
	RecyclerView list;
	MiniStatementAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mini_statement);
		
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MiniStatementActivity.this.onBackPressed();
            }
        });
        
        list = (RecyclerView) findViewById(R.id.list);
        list.setLayoutManager(new LinearLayoutManager(this));
        
        adapter = new MiniStatementAdapter(new ArrayList<Statement>(), R.layout.row_mini_statement, this);
        list.setAdapter(adapter);
        
        loadMiniStatement();
	}
	
	private void loadMiniStatement() {
		
		DialogHelper.showProgressDialog(this, getString(R.string.please_wait), false);
		
		StatementService service = new StatementServiceImpl();

		SharedPreferences prefs = getApplication().getSharedPreferences(Constants.MovaPay, Context.MODE_PRIVATE);
		String token = prefs.getString("token", "");
		
		try {
			service.getMiniStatement(token, new JsonHttpResponseHandler() {
				
				@Override
				public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
					super.onSuccess(statusCode, headers, response);
					
					DialogHelper.hideProgressDialog();
					
					try {
						
						Integer status = response.getInt("status");
						
						if(status == 0) {
							JSONArray statements = response.getJSONArray("statement");
							
							
														
							List<Statement> plist = new ArrayList<Statement>();

							for(int i = 0; i < statements.length(); i++) {
								JSONObject row = statements.getJSONObject(i);
								
								double amount = row.getDouble("amount");
								DecimalFormat formatter = new DecimalFormat("#,###.00");
								
								Statement statement = new Statement();
								statement.setAmount(formatter.format(amount));
								statement.setType(row.getString("type"));
								statement.setText(row.getString("text"));
								
								String dateStr = row.getString("date");
								DateFormat dateFormatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
								
								try {
									Date date = (Date)dateFormatter.parse(dateStr);
									
									statement.setDate(new SimpleDateFormat("yyyy-MM-dd").format(date));
									
								} catch (ParseException e) {}
								catch(Exception e) {}
								
								plist.add(statement);
							}
							
							adapter.addItems(plist);
						} else {
							
							String msg = response.getString("message");
							
							if(msg.indexOf("no postings exist within time interval") != -1) {
								msg = "There have been no transactions on your account for the past 6 Months";
							}
							
							ErrorHandler.handleError(MiniStatementActivity.this, new Throwable(msg));
						}
						
					} catch (JSONException e1) {e1.printStackTrace();}
					
				}
				
				@Override
				public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
					super.onFailure(statusCode, headers, responseString, throwable);
					DialogHelper.hideProgressDialog();
					ErrorHandler.handleError(MiniStatementActivity.this, throwable);
				}
				
				@Override
				public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
					super.onFailure(statusCode, headers, throwable, errorResponse);
					
					DialogHelper.hideProgressDialog();
					ErrorHandler.handleError(MiniStatementActivity.this, throwable);
				}
			});
		} catch (UnsupportedEncodingException | JSONException e) {}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_refresh, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		int id = item.getItemId();
		if (id == R.id.action_refresh) {
			
			adapter.clear();
			adapter.notifyDataSetChanged();
			
			loadMiniStatement();
			
			return true;
			
			
		}
		return super.onOptionsItemSelected(item);
	}
}
