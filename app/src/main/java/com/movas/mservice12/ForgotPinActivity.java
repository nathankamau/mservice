package com.movas.mservice12;

import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import org.json.JSONObject;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.movas.mcement.data.AuthService;
import com.movas.mcement.data.AuthServiceImpl;
import com.movas.mcement.helper.DialogHelper;
import com.movas.mcement.helper.ErrorHandler;

import cz.msebera.android.httpclient.Header;
import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.annotations.MinLength;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.os.Bundle;

public class ForgotPinActivity extends ActionBarActivity {

	@NotEmpty(messageId = R.string.validation_customer_id, order = 1)
	@MinLength(value = 3, messageId = R.string.validation_customer_id_length, order = 2)
	EditText customerIDView;
	Button resetBtn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_forgot_pin);
		
		customerIDView = (EditText) findViewById(R.id.customerIDView);
		resetBtn = (Button) findViewById(R.id.resetBtn);
		
		resetBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (FormValidator.validate(ForgotPinActivity.this, new SimpleErrorPopupCallback(ForgotPinActivity.this))) {
					reset();
				}
			}
		});
	}

	protected void reset() {
		AuthService service = new AuthServiceImpl();
		try {
			service.resetPin(customerIDView.getText().toString().trim(), new JsonHttpResponseHandler() {
				
				@Override
				public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
					super.onSuccess(statusCode, headers, response);
					
					DialogHelper.hideProgressDialog();
					
					try {
						
						String status = response.getString("status");
						
						if("success".equals(status)) {
							Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.request_received_message) , Toast.LENGTH_LONG);
							toast.setGravity(Gravity.TOP, 0, 0);
							toast.show();
							finish();
							
						} else {
							ErrorHandler.handleError(ForgotPinActivity.this, new Throwable(response.getString("message")));
						}
						
					} catch (JSONException e1) {}
				}
				
				@Override
				public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
					super.onFailure(statusCode, headers, responseString, throwable);
					
					DialogHelper.hideProgressDialog();
					ErrorHandler.handleError(ForgotPinActivity.this, throwable);
				}
				
				@Override
				public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
					super.onFailure(statusCode, headers, throwable, errorResponse);
					
					DialogHelper.hideProgressDialog();
					ErrorHandler.handleError(ForgotPinActivity.this, throwable);
				}
				
			});
		} catch (UnsupportedEncodingException | JSONException e) {
			e.printStackTrace();
		}
	}
}
