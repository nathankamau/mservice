package com.movas.mcement.data;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.movas.mcement.entity.OrderItem;

public interface OrderService {
	
	public void confirmDelivery(String token, String lponumber, String note, Boolean isComplaint, String id, JsonHttpResponseHandler handler)
			throws UnsupportedEncodingException, JSONException;
	
	public void placeOrder(String token, String sapClientId, String lponumber, String pickup_location, String todeliver, String deliveryLocation, String vehicleRegistration, List<OrderItem> orders, String id, JsonHttpResponseHandler handler)
			throws UnsupportedEncodingException, JSONException;
	
	public void saveMultiOrders(String token, JSONObject object, JsonHttpResponseHandler handler) throws UnsupportedEncodingException, JSONException;
	
	public void loadMyOrders(String token, JsonHttpResponseHandler handler)
			throws UnsupportedEncodingException, JSONException;
	
	public void loadMyOrderItems(String token, String orderID, JsonHttpResponseHandler handler)
			throws UnsupportedEncodingException, JSONException;
	
	public void loadTonnageHistory(String token, String fromDate, String toDate, JsonHttpResponseHandler handler)
			throws UnsupportedEncodingException, JSONException;
}