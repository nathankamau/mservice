package com.movas.mservice12;

//import org.wordpress.passcodelock.AppLockManager;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.multidex.MultiDex;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.loopj.android.http.PersistentCookieStore;

public class MovaPay extends Application {

	static MovaPay instance;
	
	PersistentCookieStore cookieStore = null;

	public static final String TAG = MovaPay.class
			.getSimpleName();

	private RequestQueue mRequestQueue;
	private ImageLoader mImageLoader;

	
//	AppLockManager appLockManager;
	
	public static MovaPay get() {
		return instance;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
//		initAppLock();
	}


	public static synchronized MovaPay getInstance() {
		return instance;
	}

	public RequestQueue getRequestQueue() {
		if (mRequestQueue == null) {
			mRequestQueue = Volley.newRequestQueue(getApplicationContext());
		}

		return mRequestQueue;
	}

	public ImageLoader getImageLoader() {
		getRequestQueue();
		if (mImageLoader == null) {
			mImageLoader = new ImageLoader(this.mRequestQueue,
					new LruBitmapCache());
		}
		return this.mImageLoader;
	}

	public <T> void addToRequestQueue(Request<T> req, String tag) {
		// set the default tag if tag is empty
		req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
		getRequestQueue().add(req);
	}

	public <T> void addToRequestQueue(Request<T> req) {
		req.setTag(TAG);
		getRequestQueue().add(req);
	}

	public void cancelPendingRequests(Object tag) {
		if (mRequestQueue != null) {
			mRequestQueue.cancelAll(tag);
		}
	}
	
//	private void initAppLock() {
//		appLockManager = AppLockManager.getInstance();
//		appLockManager.enableDefaultAppLockIfAvailable(this);
//		if (appLockManager.isAppLockFeatureEnabled()) {
//
//			String[] disabledActivities = { 
//				SplashActivity.class.getName(),
//				CustomerRegistrationActivity.class.getName(),
//				IntroActivity.class.getName()
//			};
//
//			appLockManager.getCurrentAppLock().setDisabledActivities(disabledActivities);
//		}
//	}
	
	public PersistentCookieStore getCookieStore() {
		if (cookieStore == null)
            cookieStore = new PersistentCookieStore(getApplicationContext());

		return cookieStore;
	}
	
	public boolean isNetworkAvailable() {
	    ConnectivityManager connectivityManager 
	          = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
		MultiDex.install(this);
	}
}
