package com.movas.mservice12;

import com.movas.mcement.util.Constants;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SplashActivity extends Activity implements View.OnClickListener {
	
	Button phoneLoginBtn;
	Button customerIDBtn;
	TextView forgotPinLink;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		//new Task().execute();
		
		phoneLoginBtn = (Button) findViewById(R.id.phoneLoginBtn);
		customerIDBtn = (Button) findViewById(R.id.customerIDBtn);
		forgotPinLink = (TextView) findViewById(R.id.forgotPinLink);
		
		phoneLoginBtn.setOnClickListener(this);
		customerIDBtn.setOnClickListener(this);
		forgotPinLink.setOnClickListener(this);
	}
	
	class Task extends AsyncTask<Void, Void, Void> {
		
		@Override
		protected Void doInBackground(Void... params) {
			
			try {
				Thread.sleep(2800);
			} catch (InterruptedException e) {}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			
			Intent intent = null;
			SharedPreferences prefs = getApplication().getSharedPreferences(Constants.MovaPay, Context.MODE_PRIVATE);
			
			String customerID = prefs.getString(Constants.CUSTOMER_ID, "");
			
			if(TextUtils.isEmpty(customerID)) {
				intent = new Intent(SplashActivity.this, CustomerRegistrationActivity.class);
			} else {
				intent = new Intent(SplashActivity.this, HomeActivity.class);
			}
			
			if(intent != null)
				startActivity(intent);
			
			finish();
		}
	}
	
	@Override
	public void onClick(View v) {
		
		if(v == forgotPinLink) {
			
			Intent intent = new Intent(SplashActivity.this, ForgotPinActivity.class);
			startActivity(intent);
			
		} else {
			
			Intent intent = new Intent(SplashActivity.this, CustomerRegistrationActivity.class);
			if(v == phoneLoginBtn)
				intent.putExtra(Constants.LOGIN_TYPE, "phone");
			startActivity(intent);
			finish();
			
		}
	}
}