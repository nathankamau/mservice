package com.movas.mcement.entity;


public class Gps {

    public String carId;

    public String gpsTime;

    public String rcvTime;

    public Double longitude;

    public Double latitude;

    public String speed;

    public String head;

    public String getTerminalKey() {
        return terminalKey;
    }

    public void setTerminalKey(String terminalKey) {
        this.terminalKey = terminalKey;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public String getGpsTime(String gpsTime) {
        return this.gpsTime;
    }

    public void setGpsTime(String gpsTime) {
        this.gpsTime = gpsTime;
    }

    public String getRcvTime(String rcvTime) {
        return this.rcvTime;
    }

    public void setRcvTime(String rcvTime) {
        this.rcvTime = rcvTime;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Double setLongitude(String longitude) {
        int divisor = 1000000;
        Double doubleObject = new Double(longitude);
        double number = doubleObject.doubleValue();

        double d = number /divisor;

        return d;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double setLatitude(String latitude) {
        int divisor = 1000000;
        Double doubleObject = new Double(latitude);
        double number = doubleObject.doubleValue();

        double d = number /divisor;

        return d;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getValid() {
        return valid;
    }

    public void setValid(String valid) {
        this.valid = valid;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public String getAcc() {
        return acc;
    }

    public void setAcc(String acc) {
        this.acc = acc;
    }

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public String valid;

    public String fuel;

    public String acc;

    public String mileage;

    public String terminalKey;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String status;

    public double convertToDouble(String value){

        int divisor = 1000000;
        Double doubleObject = new Double(value);
        double number = doubleObject.doubleValue();

        double d = number /divisor;

        return d;

    }


}
