package com.movas.mservice12;

import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import org.json.JSONObject;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.movas.mcement.data.ReviewService;
import com.movas.mcement.data.ReviewServiceImpl;
import com.movas.mcement.helper.DialogHelper;
import com.movas.mcement.helper.ErrorHandler;
import com.movas.mcement.util.Constants;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import cz.msebera.android.httpclient.Header;

public class FeedbackActivity extends ActionBarActivity {
	
	EditText noteView;
	Button submitBtn;
	CheckBox isComplaint;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_feedback);
		
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FeedbackActivity.this.onBackPressed();
            }
        });
        
        noteView = (EditText) findViewById(R.id.noteView);
        submitBtn = (Button) findViewById(R.id.submitBtn);
        isComplaint = (CheckBox) findViewById(R.id.isComplaint);
        
        submitBtn.setOnClickListener(new View.OnClickListener() {
        	
			@Override
			public void onClick(View v) {
				submitReview();
			}
		});
	}
	
	private void submitReview() {
		
		SharedPreferences prefs = getApplication().getSharedPreferences(Constants.MovaPay, Context.MODE_PRIVATE);
		String customerID = prefs.getString(Constants.CUSTOMER_ID, "");
		String uuid = prefs.getString(Constants.CUSTOMER_UUID, "");
		String token = prefs.getString("token", "");
		
		ReviewService service = new ReviewServiceImpl();
		
		DialogHelper.showProgressDialog(this, getString(R.string.please_wait), false);
		
		try {
			
			service.submitReview(token, customerID, uuid, noteView.getText().toString(), isComplaint.isChecked() ? 1 : 0, new JsonHttpResponseHandler() {
				
				@Override
				public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
					super.onSuccess(statusCode, headers, response);
					
					DialogHelper.hideProgressDialog();
					
					try {
						
						String status = response.getString("status");
						
						if("0".equals(status)) {
							Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.feedback_success_message) , Toast.LENGTH_LONG);
							toast.setGravity(Gravity.TOP, 0, 0);
							toast.show();
							finish();
							
						} else {
							ErrorHandler.handleError(FeedbackActivity.this, new Throwable(response.getString("message")));
						}
						
					} catch (JSONException e1) {}
				}
				
				@Override
				public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
					super.onFailure(statusCode, headers, responseString, throwable);
					
					DialogHelper.hideProgressDialog();
					throwable.printStackTrace();
					ErrorHandler.handleError(FeedbackActivity.this, throwable);
				}
				
				@Override
				public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
					super.onFailure(statusCode, headers, throwable, errorResponse);
					
					DialogHelper.hideProgressDialog();
					ErrorHandler.handleError(FeedbackActivity.this, throwable);
				}
				
			});
		} catch (UnsupportedEncodingException | JSONException e) {
			e.printStackTrace();
		}
	}
}