package com.movas.mcement.wizard.model.order;

import android.support.v4.app.Fragment;
import android.text.TextUtils;
import java.util.ArrayList;
import com.movas.mcement.wizard.model.ModelCallbacks;
import com.movas.mcement.wizard.model.Page;
import com.movas.mcement.wizard.model.ReviewItem;
import com.movas.mcement.wizard.ui.order.OrderDetailsFragment;

public class OrderDetailsPage extends Page {
	
	public static final String TONNAGE_KEY = "tonnage";
	
    public OrderDetailsPage(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
    }
    
    @Override
    public Fragment createFragment() {
        return OrderDetailsFragment.create(getKey());
    }
    
    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem("Tonnage", mData.getString(TONNAGE_KEY), getKey(), -1));
    }
    
    @Override
    public boolean isCompleted() {
        return !TextUtils.isEmpty(mData.getString(TONNAGE_KEY));
    }
}