package com.movas.mcement.listener;

public interface ObjectChangeListener {

	public void onChange(String name, Object value);
}
