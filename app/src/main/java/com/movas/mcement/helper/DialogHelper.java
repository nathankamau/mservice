package com.movas.mcement.helper;

import android.app.ProgressDialog;
import android.content.Context;

public class DialogHelper {
	
	static ProgressDialog pDialog;
	
	public static void showProgressDialog(Context context, String message, Boolean cancelable) {
		pDialog = null;
		pDialog = new ProgressDialog(context);
		pDialog.setMessage(message);
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(cancelable);
		pDialog.show();
	}
	
	public static void hideProgressDialog() {
		if(pDialog != null)
			pDialog.hide();
	}
}