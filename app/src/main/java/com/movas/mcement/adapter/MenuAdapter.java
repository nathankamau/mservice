



package com.movas.mcement.adapter;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.movas.mcement.entity.MenuItem;
import com.movas.mcement.util.Constants;
import com.movas.mservice12.BalanceActivity;
import com.movas.mservice12.ConfirmDeliveryActivity;
import com.movas.mservice12.FullStatementActivity;
import com.movas.mservice12.HomeActivity;
import com.movas.mservice12.MultiOrderActivity;
import com.movas.mservice12.MyOrdersActivity;
import com.movas.mservice12.PointsActivity;
import com.movas.mservice12.PricelistActivity;
import com.movas.mservice12.R;
import com.movas.mservice12.RequestInvoiceActivity;
import com.movas.mservice12.SingleOrderActivity;

import java.util.List;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {
	
    private List<MenuItem> menuItems;
    private int rowLayout;
    private HomeActivity mAct;
    
    public MenuAdapter(List<MenuItem> applications, int rowLayout, HomeActivity act) {
        this.menuItems = applications;
        this.rowLayout = rowLayout;
        this.mAct = act;
    }
    
    public void clearApplications() {
        int size = this.menuItems.size();
        if (size > 0) {
        	
            for (int i = 0; i < size; i++) {
                menuItems.remove(0);
            }
            
            this.notifyItemRangeRemoved(0, size);
        }
    }
    
    public void addItems(List<MenuItem> applications) {
        this.menuItems.addAll(applications);
        this.notifyItemRangeInserted(0, applications.size() - 1);
    }
    
    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(rowLayout, viewGroup, false);
        ViewHolder holder = new ViewHolder(v, mAct);
        return holder;
    }
    
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        final MenuItem item = menuItems.get(i);
        viewHolder.name.setText(item.getName());
    }
    
    @Override
    public int getItemCount() {
        return menuItems == null ? 0 : menuItems.size();
    }
    
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    	
    	public TextView name;
        HomeActivity hAct;
        
        public ViewHolder(View itemView, HomeActivity mAct) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.countryName);
            hAct = mAct;
            itemView.setOnClickListener(this);
        }
        
		@Override
		public void onClick(View v) {
			
			Intent intent = null;
			
			switch (getPosition()) {
				case 0:
					
					final CharSequence[] items = { hAct.getString(R.string.single_order), hAct.getString(R.string.multiple_orders) };
					
					AlertDialog.Builder builder = new AlertDialog.Builder(hAct);
					builder.setItems(items, new DialogInterface.OnClickListener() {
						
					    public void onClick(DialogInterface dialog, int item) {
					    	
					        Boolean isSingleOrder = items[item].equals(hAct.getString(R.string.single_order));
					        
					        if(isSingleOrder) {
					        	
					        	Intent i = new Intent( hAct, SingleOrderActivity.class);
								i.putExtra(Constants.INITIAL_ORDER, true);
								i.putExtra(Constants.IS_SINGLE_ORDER, isSingleOrder);
								start(i);
								
					        } else {
					        	
					        	Intent i = new Intent( hAct, MultiOrderActivity.class);
								start(i);
					        }
					        
					        
					    }
					});
					
					AlertDialog alert = builder.create();
					alert.show();
					
					break;
				case 1:
					
					final CharSequence[] locations = { hAct.getString(R.string.athi_river), hAct.getString(R.string.mombasa) };
					//, hAct.getString(R.string.export)
					AlertDialog.Builder b = new AlertDialog.Builder(hAct);
					b.setItems(locations, new DialogInterface.OnClickListener() {
						
					    public void onClick(DialogInterface dialog, int item) {
					    	
					        String location = locations[item].toString();
					        
					        Intent i = new Intent( hAct, PricelistActivity.class);
							i.putExtra(Constants.LOCATION, location);
							
							start(i);
					    }
					});
					b.create().show();
					break;
				case 2:
					intent = new Intent(hAct, BalanceActivity.class);
					break;
				case 3:
					//TODO


					Toast toast3 = Toast.makeText(hAct.getApplicationContext(), "coming soon ......", Toast.LENGTH_LONG);
					toast3.setGravity(Gravity.TOP, 0, 0);
					toast3.show();

//					intent = new Intent(hAct, PointsActivity.class);
					break;
				case 4:
					intent = new Intent(hAct, ConfirmDeliveryActivity.class);
					break;
				case 5:
					intent = new Intent(hAct, MyOrdersActivity.class);
					break;
//				case 6:
//					intent = new Intent(hAct, MiniStatementActivity.class);
//					break;
				case 6:
					intent = new Intent(hAct, FullStatementActivity.class);
					break;
				case 7:


					Toast toast = Toast.makeText(hAct.getApplicationContext(), "Geo location in development", Toast.LENGTH_LONG);
					toast.setGravity(Gravity.TOP, 0, 0);
					toast.show();
//					intent = new Intent(hAct,MapDeliveryNote.class);
					break;
				case 8:
					intent = new Intent(hAct, RequestInvoiceActivity.class);
					break;
				case 9:
                    Toast toast2 = Toast.makeText(hAct.getApplicationContext(), "coming soon ....", Toast.LENGTH_LONG);
                    toast2.setGravity(Gravity.TOP, 0, 0);
                    toast2.show();
					//intent = new Intent(hAct, TonnageReportActivity.class);
					break;
//				case 11:
//
//					Toast toast1 = Toast.makeText(hAct.getApplicationContext(), "product loan coming soon ....", Toast.LENGTH_LONG);
//					toast1.setGravity(Gravity.TOP, 0, 0);
//					toast1.show();
//
//					break;
				default:
					break;
				}
			
			start(intent);
		}
		
		protected void start(Intent i) {
			if(i != null) {
				
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
					startActivity16(i);
				} else
					hAct.startActivity(i);
			}
		}

		@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
		private void startActivity16(Intent i){
	    	ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(hAct);
	    	hAct.startActivity(i, transitionActivityOptions.toBundle());
	    }
    }
}