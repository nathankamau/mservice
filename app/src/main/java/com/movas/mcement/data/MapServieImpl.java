package com.movas.mcement.data;

import com.movas.mcement.entity.Gps;
import com.movas.mcement.entity.Track;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class MapServieImpl implements MapService {

   /* @Override
    public void fetch(String token, String text, JsonHttpResponseHandler handler) throws UnsupportedEncodingException, JSONException {


        String url = UrlUtils.url + "/api/tracking/order/" + text;


        HttpClient client = HttpClient.instance();

        RequestParams params = new RequestParams();
        params.put("token", token);
        params.put("text", text);


        client.getAsyncClient().get(url, params, handler);

    }
}*/


    private NetworkDAO networkDAO = new NetworkDAO();


    @Override
    public void save(Track track) throws Exception {

    }

    @Override
    public List<Gps> fetch(String deliveryNoteNo) throws Exception {

        ArrayList<Gps> gpsResults = new ArrayList<>();

        String queryURL = "http://80.241.222.62:8787/api/tracking/order/";

        final String uri = queryURL + deliveryNoteNo;


        String result = networkDAO.request(uri);


        JSONObject root = new JSONObject(result);

        JSONArray data = root.getJSONArray("gps");
        Gps gps = new Gps();
        for (int i=0; i<=data.length();i++){
            JSONObject obj = data.getJSONObject(i);

            String carId = obj.getString("car_id");
            gps.setCarId(carId);
            String gpsTime = obj.getString("gps_time");
            gps.setGpsTime(gpsTime);
            String rcvTime = obj.getString("rcv_time");
            gps.setRcvTime(rcvTime);
            String longitude =obj.getString("longitude");
            gps.setLatitude(longitude);
            String latitude = obj.getString("latitude");
            gps.setLatitude(latitude);
            String speed = obj.getString("speed");
            gps.setSpeed(speed);
            String head = obj.getString("head");
            gps.setHead(head);
            String valid =obj.getString("valid");
            gps.setValid(valid);
            String fuel =obj.getString("fuel");
            gps.setFuel(fuel);
            String status = obj.getString("status");
            gps.setStatus(status);
            String mileage = obj.getString("mileage");
            gps.setMileage(mileage);
            String acc = obj.getString("acc");
            gps.setAcc(acc);
            String terminalKey = obj.getString("terminal_key");
            gps.setTerminalKey(terminalKey);

            gpsResults.add(gps);
        }

        return gpsResults;
    }


    @Override
    public Gps TrackByDeliveryNoteNo(String deliveryNoteNo) throws Exception {
        return null;
    }


    @Override
    public List<Gps> fetchTrackByLocation(double latitude, double longitude) throws Exception {

        ArrayList<Gps> gpsResults = new ArrayList<>();


        return gpsResults;
    }
}

