package com.movas.mcement.data;

import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class BalanceServiceImpl implements BalanceService {

	@Override
	public void getBalance(String token, JsonHttpResponseHandler handler)
			throws UnsupportedEncodingException, JSONException {

		String url = "http://193.34.144.238:8181/mservice-proxy/api/clientBalance";

		HttpClient client = HttpClient.instance();

        RequestParams params = new RequestParams();
        params.put("token", token);

		client.getAsyncClient().get(url, params, handler);
	}
}