package com.movas.mcement.data;

import java.io.UnsupportedEncodingException;

import org.json.JSONException;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.movas.mcement.util.Constants;

public class AuthServiceImpl implements AuthService {

    @Override
    public void login(String identifier, String pin, String type,
                      JsonHttpResponseHandler handler)
            throws UnsupportedEncodingException, JSONException {

        String url = "http://193.34.144.238:8181/mservice-proxy/api/authenticate";

        //HttpClient client = HttpClient.instance();

        AsyncHttpClient client = new AsyncHttpClient();

        RequestParams params = new RequestParams();

        params.put("sapClientId", identifier);
        params.put("pin", pin);
        params.put("phone", "254725527610");
        params.put("apiKey", Constants.API_KEY);
        params.put("userId", Constants.USER_ID);
        params.setUseJsonStreamer(true);



        client.post(url, params, handler);


    }


    @Override
    public void logout(String token, JsonHttpResponseHandler handler) throws UnsupportedEncodingException, JSONException {

        String url = "http://193.34.144.238:8181/mservice-proxy/api/logout";

        HttpClient client = HttpClient.instance();

        RequestParams params = new RequestParams();
        params.put("token", token);
        params.setUseJsonStreamer(true);


        client.getAsyncClient().get(url, params, handler);
    }

    @Override
    public void changePin(String token, String currentPin, String newPin,
                          String newPinRepeat, JsonHttpResponseHandler handler)
            throws UnsupportedEncodingException, JSONException {

        String url = "http://193.34.144.238:8181/mservice-proxy/api/changePin";

        HttpClient client = HttpClient.instance();

        RequestParams params = new RequestParams();
        params.put("currentPin", currentPin);
        params.put("newPin", newPin);
        params.put("newPinRepeat", newPinRepeat);
        params.put("token", token);

        params.setUseJsonStreamer(true);

        client.getAsyncClient().post(url, params, handler);
    }

    @Override
    public void resetPin(String customerID, JsonHttpResponseHandler handler)
            throws UnsupportedEncodingException, JSONException {

        String url = "http://193.34.144.238:8181/mservice-proxy/api/resetPin";

        HttpClient client = HttpClient.instance();

        RequestParams params = new RequestParams();
        params.put("customerID", customerID);

        client.getAsyncClient().post(url, params, handler);

    }
}
