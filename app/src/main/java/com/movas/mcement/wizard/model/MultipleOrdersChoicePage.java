package com.movas.mcement.wizard.model;

import java.util.ArrayList;
import java.util.Arrays;
import com.movas.mcement.wizard.ui.MultipleOrdersChoiceFragment;
import android.support.v4.app.Fragment;
import android.text.TextUtils;

public class MultipleOrdersChoicePage extends Page {
	
	protected ArrayList<String> mChoices = new ArrayList<String>();
	
	public MultipleOrdersChoicePage(ModelCallbacks callbacks, String title) {
		super(callbacks, title);
	}
	
	@Override
	public Fragment createFragment() {
		return MultipleOrdersChoiceFragment.create(getKey());
	}
	
	public String getOptionAt(int position) {
        return mChoices.get(position);
    }
	
    public int getOptionCount() {
        return mChoices.size();
    }
    
    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem(getTitle(), mData.getString(SIMPLE_DATA_KEY), getKey()));
    }
    
    @Override
    public boolean isCompleted() {
        return !TextUtils.isEmpty(mData.getString(SIMPLE_DATA_KEY));
    }
    
    public MultipleOrdersChoicePage setChoices(String... choices) {
        mChoices.addAll(Arrays.asList(choices));
        return this;
    }
    
    public MultipleOrdersChoicePage setValue(String value) {
        mData.putString(SIMPLE_DATA_KEY, value);
        return this;
    }
}