package com.movas.mcement.wizard;

import com.movas.mcement.wizard.model.AbstractWizardModel;
import com.movas.mcement.wizard.model.BranchPage;
import com.movas.mcement.wizard.model.PageList;
import com.movas.mcement.wizard.model.SingleFixedChoicePage;
import com.movas.mcement.wizard.model.order.OrderDetailsPage;
import com.movas.mcement.wizard.model.order.OrderPage;

import android.content.Context;

public class SingleOrderWizardModel extends AbstractWizardModel {
	
    public SingleOrderWizardModel(Context context) {
        super(context);
    }
    
    @Override
    protected PageList onNewRootPageList() {
        return new PageList(
        		new BranchPage(this, "Purchase Location")
	                .addBranch("Athi River", 
	                	new SingleFixedChoicePage(this, "Brand")
			                .setChoices(
		                		"Nguvu Bagged",
		                		"Nguvu Bulk",
		                		"Powerplus bagged",
		                		"Powermax Bagged",
		                		"Powercrete Bagged",
								"Tembo",
								"Supaset"
		                	).setRequired(true)
	                ).addBranch("Mombasa", 
		                	new SingleFixedChoicePage(this, "Brand")
				                .setChoices(
			                		"Nguvu Bagged",
			                		"Nguvu Bulk ",
			                		"Powerplus Bagged",
			                		"Powerplus Bulk",
			                		"Powermax Bagged",
			                		"Powermax Bulk",
			                		"Powercrete Bagged",
			                		"Powercrete Bulk"
			                	).setRequired(true)
	                		).addBranch("Export", 
	    		                	new SingleFixedChoicePage(this, "Brand")
						                .setChoices(
					                		"Supaset",
					                		"Tembo",
					                		"Powerplus"
					                	).setRequired(true)
			                		).setRequired(true),
        			new OrderPage(this, "Enter Details").setRequired(true),
                new OrderDetailsPage(this, "Enter Tonnage").setRequired(true)
        );
    }
}