package com.movas.mcement.entity;

public class TonnageReport {

	String tonnage;
	
	String name;

	public String getTonnage() {
		return tonnage;
	}

	public void setTonnage(String tonnage) {
		this.tonnage = tonnage + " Tonnes";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
