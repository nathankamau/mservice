package com.movas.mcement.data;

import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import com.loopj.android.http.JsonHttpResponseHandler;

public interface AuthService {
	
	public void resetPin(String customerID, JsonHttpResponseHandler handler)
			throws UnsupportedEncodingException, JSONException;
	
	public void changePin(String token, String currentPin, String newPin, String newPinRepeat, JsonHttpResponseHandler handler)
			throws UnsupportedEncodingException, JSONException;
	
	public void login(String identifier, String pin, String type, JsonHttpResponseHandler handler)
			throws UnsupportedEncodingException, JSONException;
	
	public void logout(String token, JsonHttpResponseHandler handler) throws UnsupportedEncodingException, JSONException;
}