package com.movas.mcement.wizard.model.order;

import java.util.ArrayList;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import com.movas.mservice12.R;
import com.movas.mcement.wizard.model.ModelCallbacks;
import com.movas.mcement.wizard.model.Page;
import com.movas.mcement.wizard.model.ReviewItem;
import com.movas.mcement.wizard.ui.order.OrderFragment;
import com.movas.mservice12.MovaPay;

public class OrderPage extends Page {
	
	public static final String LPO_NO_KEY = "lpo_no";
	
	public static final String VEHICLE_NO_KEY = "vehicle_no";
	
	public static final String VEHICLE_NOS_ARRAY = "";
	
	public static final String TRANSPORT_METHOD_KEY = "transport_method";
	
	public static final String DELIVERY_LOCATION_KEY = "delivery_location";
	
    public OrderPage(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
    }
    
    @Override
    public Fragment createFragment() {
        return OrderFragment.create(getKey());
    }
    
    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
    	
        dest.add(new ReviewItem("LPO No", mData.getString(LPO_NO_KEY), getKey(), -1));
        dest.add(new ReviewItem("Vehicle No", mData.getString(VEHICLE_NO_KEY), getKey(), -1));
        dest.add(new ReviewItem("Transport Method", mData.getString(TRANSPORT_METHOD_KEY), getKey(), -1));
        dest.add(new ReviewItem("Delivery Location", mData.getString(DELIVERY_LOCATION_KEY), getKey(), -1));
    }
    
    @Override
    public boolean isCompleted() {
    	
    	Boolean isComplete = false;
    	isComplete = !TextUtils.isEmpty(mData.getString(LPO_NO_KEY));
    	isComplete = isComplete ? !TextUtils.isEmpty(mData.getString(TRANSPORT_METHOD_KEY)) : false;
    	
    	if(isComplete) {
    		
    		if(MovaPay.get().getString(R.string.to_be_collected).equals(mData.getString(TRANSPORT_METHOD_KEY))) {
    			isComplete = !TextUtils.isEmpty(mData.getString(VEHICLE_NO_KEY));
        	} else if(MovaPay.get().getString(R.string.to_be_collected).equals(mData.getString(TRANSPORT_METHOD_KEY))) {
        		isComplete = !TextUtils.isEmpty(mData.getString(DELIVERY_LOCATION_KEY));
        	}
    	}
    	
        return isComplete;
    }
}