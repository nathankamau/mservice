package com.movas.mcement.entity;

public class Order {

	Integer orderrequestid;
	String datecreated;
	Integer type;
	String name;
	String sessionid;
	String lponumber;
	String clientaccountid;
	String collectionpoint;
	String status;
	String vehiclereg;
	String deliverylocation;
	
	public String getVehiclereg() {
		return vehiclereg;
	}
	public void setVehiclereg(String vehiclereg) {
		this.vehiclereg = vehiclereg;
	}
	public Integer getOrderrequestid() {
		return orderrequestid;
	}
	public void setOrderrequestid(Integer orderrequestid) {
		this.orderrequestid = orderrequestid;
	}
	public String getDatecreated() {
		return datecreated;
	}
	public void setDatecreated(String datecreated) {
		this.datecreated = datecreated;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSessionid() {
		return sessionid;
	}
	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}
	public String getLponumber() {
		return lponumber;
	}
	public void setLponumber(String lponumber) {
		this.lponumber = lponumber;
	}
	public String getClientaccountid() {
		return clientaccountid;
	}
	public void setClientaccountid(String clientaccountid) {
		this.clientaccountid = clientaccountid;
	}
	public String getCollectionpoint() {
		return collectionpoint;
	}
	public void setCollectionpoint(String collectionpoint) {
		this.collectionpoint = collectionpoint;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDeliverylocation() {
		return deliverylocation;
	}
	public void setDeliverylocation(String deliverylocation) {
		this.deliverylocation = deliverylocation;
	}
}