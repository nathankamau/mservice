package com.movas.mcement.data;

import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import com.loopj.android.http.JsonHttpResponseHandler;

public interface StatementService {
	
	public void getMiniStatement(String token, JsonHttpResponseHandler handler) throws UnsupportedEncodingException, JSONException;
	
	public void requestFullStatement(String token, String fromDate, String toDate, JsonHttpResponseHandler handler) throws UnsupportedEncodingException, JSONException;
}
