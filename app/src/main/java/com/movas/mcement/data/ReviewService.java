package com.movas.mcement.data;

import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import com.loopj.android.http.JsonHttpResponseHandler;

public interface ReviewService {
	
	public void submitReview(String token, String sapClientId, String id, String review, Integer type, JsonHttpResponseHandler handler)
			throws UnsupportedEncodingException, JSONException;
}