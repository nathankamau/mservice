package com.movas.mcement.data;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;


public class NetworkDAO {

    public String request(String uri) throws ClientProtocolException, IOException {

        String result = "";

        HttpGet httpGet = new HttpGet(uri);


        ResponseHandler<String> responseHandler = new BasicResponseHandler();


        HttpClient httpClient = new DefaultHttpClient();


        result = httpClient.execute(httpGet, responseHandler);


        return result;
    }


}


