package com.movas.mservice12;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.movas.mcement.data.AuthService;
import com.movas.mcement.data.AuthServiceImpl;
import com.movas.mcement.helper.DialogHelper;
import com.movas.mcement.helper.ErrorHandler;
import com.movas.mcement.util.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.UUID;

import cz.msebera.android.httpclient.Header;
import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.annotations.MinLength;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;

public class CustomerRegistrationActivity extends ActionBarActivity implements OnClickListener {

    @NotEmpty(messageId = R.string.validation_customer_id, order = 1)
    @MinLength(value = 3, messageId = R.string.validation_customer_id_length, order = 2)
    EditText customerIdentifier;

    EditText pinView;

    Button loginBtn;

    static int count = 0;

    static int maxTries =3;



    Boolean isPhoneLogin = false;

    Boolean blocking = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_registration);

//        Intent intent = new Intent(CustomerRegistrationActivity.this,HomeActivity.class);
//        startActivity(intent);

        customerIdentifier = (EditText) findViewById(R.id.customerIdentifier);
        pinView = (EditText) findViewById(R.id.pinView);

        loginBtn = (Button) findViewById(R.id.loginBtn);
        loginBtn.setOnClickListener(this);

        if (getIntent().hasExtra(Constants.LOGIN_TYPE)) {
            if ("phone".equals(getIntent().getStringExtra(Constants.LOGIN_TYPE))) {
                customerIdentifier.setHint(getString(R.string.phone));
                customerIdentifier.setInputType(InputType.TYPE_CLASS_NUMBER);
                isPhoneLogin = true;
            }
        }

        blocking = getIntent().getBooleanExtra(Constants.BLOCKING, true);

        SharedPreferences prefs = getApplication().getSharedPreferences(Constants.MovaPay, Context.MODE_PRIVATE);

        if (!isPhoneLogin) {
            if (prefs.contains(Constants.CUSTOMER_ID)) {
                customerIdentifier.setText(prefs.getString(Constants.CUSTOMER_ID, ""));
                pinView.requestFocus();
            }
        } else {
            customerIdentifier.setText(prefs.getString(Constants.PHONE, ""));
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.customer_registration, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v == loginBtn) {

            if (FormValidator.validate(this, new SimpleErrorPopupCallback(this))) {
                login();
            }
        }
    }




    private void login() {

        DialogHelper.showProgressDialog(this, getString(R.string.please_wait), false);

        AuthService service = new AuthServiceImpl();

        try {
            //changed to apply from  commit

            getApplication()
                    .getSharedPreferences(Constants.MovaPay, Context.MODE_PRIVATE)
                    .edit().clear().apply();

            service.login(
                    customerIdentifier.getText().toString(),
                    pinView.getText().toString(), isPhoneLogin ? "phone" : "customerid", new JsonHttpResponseHandler() {

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            super.onSuccess(statusCode, headers, response);
                            DialogHelper.hideProgressDialog();

                            try {

                                String status = response.has("status") ? response.getString("status") : "error";

                                if ("error".equals(status)) {
                                    String message = response.getString("status");

                                    ErrorHandler.handleError(CustomerRegistrationActivity.this, new Throwable(message));

                                } else {

                                    String customerID = response.getString("accountnumber");

                                    //save customer id
                                    SharedPreferences prefs = getApplication().getSharedPreferences(Constants.MovaPay, Context.MODE_PRIVATE);
                                    prefs.edit()
                                            .putString(Constants.CUSTOMER_ID, customerID)
                                            .putString("token", response.getString("token"))
                                            .putString(Constants.PHONE, isPhoneLogin ? customerIdentifier.getText().toString() : "")
                                            .putString(Constants.CUSTOMER_UUID, UUID.randomUUID().toString().substring(0, 6))
                                            .apply();



                                    Intent intent = new Intent(CustomerRegistrationActivity.this, HomeActivity.class);
                                    startActivity(intent);

                                    finish();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                            super.onFailure(statusCode, headers, responseString, throwable);

                            DialogHelper.hideProgressDialog();
                            ErrorHandler.handleError(CustomerRegistrationActivity.this, throwable);
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            super.onFailure(statusCode, headers, throwable, errorResponse);
                            try {

                                String message = errorResponse.getString("status");
                                DialogHelper.hideProgressDialog();
                                ErrorHandler.handleError(CustomerRegistrationActivity.this, new Throwable(message));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }




                        }

                    });
        } catch (UnsupportedEncodingException | JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {


            Intent intent = new Intent(this, SplashActivity.class);
            startActivity(intent);
            finish();

    }


}