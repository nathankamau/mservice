package com.movas.mservice12;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.movas.mcement.adapter.MyOrdersMenuAdapter;
import com.movas.mcement.data.OrderService;
import com.movas.mcement.data.OrderServiceImpl;
import com.movas.mcement.entity.Order;
import com.movas.mcement.helper.DialogHelper;
import com.movas.mcement.helper.ErrorHandler;
import com.movas.mcement.util.Constants;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;

import cz.msebera.android.httpclient.Header;

public class MyOrdersActivity extends ActionBarActivity {
		
	private MyOrdersMenuAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;
            
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_orders);
		
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyOrdersActivity.this.onBackPressed();
            }
        });
				
        mRecyclerView = (RecyclerView) findViewById(R.id.list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        
        mAdapter = new MyOrdersMenuAdapter(new ArrayList<Order>(), R.layout.row_my_order_item, MyOrdersActivity.this);
        mRecyclerView.setAdapter(mAdapter);
        
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        
        SharedPreferences prefs = getApplication().getSharedPreferences(Constants.MovaPay, Context.MODE_PRIVATE);
        
        loadOrders();
	}
	
	private void loadOrders() {

		OrderService service = new OrderServiceImpl();

		SharedPreferences prefs = getApplication().getSharedPreferences(Constants.MovaPay, Context.MODE_PRIVATE);
		String token = prefs.getString("token", "");
		
		try {
			service.loadMyOrders(token, new JsonHttpResponseHandler() {
				
				@Override
				public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
					super.onSuccess(statusCode, headers, response);

                    try {


						Integer status = response.getInt("status");



						if(status == 0) {
                        //if(response.has("status") && response.getInt("status").equals("0")) {

                            JSONArray details = response.getJSONArray("details");

                            for(int i = 0; i < details.length(); i++) {

                                JSONObject row;

                                row = details.getJSONObject(i);

                                Order order = new Order();
                                order.setOrderrequestid(row.getInt("orderrequestid"));
                                //order.setType(row.getInt("type"));
                                order.setName(row.getString("name"));
                                order.setLponumber(row.getString("lpo"));
                                order.setCollectionpoint(row.getString("collectionpoint"));
                                order.setStatus(row.getString("orderstatus"));
                                order.setVehiclereg(row.getString("vehiclereg"));
                                order.setDeliverylocation(row.getString("deliverylocation"));
                                order.setDatecreated(row.getString("date"));

                                mAdapter.addOrder(order);

                            }

                        } else {


                        }

                    } catch (Exception e) {
                    }

                    mRecyclerView.setVisibility(View.VISIBLE);
                    mProgressBar.setVisibility(View.GONE);
				}
				
				@Override
				public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
					super.onFailure(statusCode, headers, responseString, throwable);
					DialogHelper.hideProgressDialog();
					ErrorHandler.handleError(MyOrdersActivity.this, throwable);
				}
				
				@Override
				public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
					super.onFailure(statusCode, headers, throwable, errorResponse);
					DialogHelper.hideProgressDialog();
					ErrorHandler.handleError(MyOrdersActivity.this, throwable);
				}
			});
		} catch (UnsupportedEncodingException | JSONException e) {}
	}
	
	public void hideSoftKeyPad() {
		InputMethodManager inputManager = (InputMethodManager) this
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		
		View v = this.getCurrentFocus();
		if (v == null)
			return;
		
		inputManager.hideSoftInputFromWindow(v.getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}
	
	@Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(menu);
	}
	
	public boolean onContextItemSelected(MenuItem item) {
		return super.onContextItemSelected(item);
	}
	
	public Order getOrder(int pos) {
		return mAdapter.getOrder(pos);
	}
}