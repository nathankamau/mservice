package com.movas.mcement.entity;

public class HomeMenuItem {
	
	public String tag;
	
	int itemCount;
	
	public int iconRes = 0;

	public String itemID;

	public HomeMenuItem() {
	}
	
	public HomeMenuItem(String tag) {
		this.tag = tag;
	}

	public HomeMenuItem(String tag, int iconRes) {
		this.tag = tag;
		this.iconRes = iconRes;
		this.itemID = "";
	}

	public HomeMenuItem(String tag, int iconRes, String id) {
		this.tag = tag;
		this.iconRes = iconRes;
		this.itemID = id;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public int getIconRes() {
		return iconRes;
	}

	public void setIconRes(int iconRes) {
		this.iconRes = iconRes;
	}

	public String getItemID() {
		return itemID;
	}

	public void setItemID(String itemID) {
		this.itemID = itemID;
	}

	public int getItemCount() {
		return itemCount;
	}

	public void setItemCount(int itemCount) {
		this.itemCount = itemCount;
	}
}