package com.movas.mservice12;

import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import org.json.JSONObject;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.movas.mcement.data.OrderService;
import com.movas.mcement.data.OrderServiceImpl;
import com.movas.mcement.helper.DialogHelper;
import com.movas.mcement.helper.ErrorHandler;
import com.movas.mcement.util.Constants;
import com.movas.mcement.util.ToastUtil;

import cz.msebera.android.httpclient.Header;
import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.annotations.MinLength;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class ConfirmDeliveryActivity extends ActionBarActivity {
	
	Button submitBtn;
	
	@NotEmpty(messageId = R.string.validation_delivery_note_no, order = 2)
	@MinLength(value = 3, messageId = R.string.validation_delivery_note_no_length, order = 3)
	EditText deliveryNoteNoView;
	EditText noteView;
	CheckBox isComplaintCheck;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_confirm_delivery);
		
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfirmDeliveryActivity.this.onBackPressed();
            }
        });
        
        deliveryNoteNoView = (EditText) findViewById(R.id.deliveryNoteNoView);
        noteView = (EditText) findViewById(R.id.noteView);
        isComplaintCheck = (CheckBox) findViewById(R.id.isComplaintCheck);
        
        submitBtn = (Button) findViewById(R.id.submitBtn);
        submitBtn.setOnClickListener(new View.OnClickListener() {
        	
			@Override
			public void onClick(View v) {
				
				if(FormValidator.validate(ConfirmDeliveryActivity.this, new SimpleErrorPopupCallback(ConfirmDeliveryActivity.this))) {

					SharedPreferences prefs = getApplication().getSharedPreferences(Constants.MovaPay, Context.MODE_PRIVATE);
					String uuid = prefs.getString(Constants.CUSTOMER_UUID, "");
					String token = prefs.getString("token", "");
					
					hideKeyBoard();
					DialogHelper.showProgressDialog(ConfirmDeliveryActivity.this, getString(R.string.confirming_delivery), false);
					OrderService service = new OrderServiceImpl();
					try {
						service.confirmDelivery(token,deliveryNoteNoView.getText().toString(), noteView.getText().toString(), isComplaintCheck.isChecked(), uuid, new JsonHttpResponseHandler() {
							@Override
							public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
								super.onSuccess(statusCode, headers, response);
								
								DialogHelper.hideProgressDialog();
								
								try {
									
									Integer status = response.getInt("status");
									
									if(status == 0) {
										ToastUtil.makeText(getApplicationContext(), getString(R.string.delivery_confirm_success), Toast.LENGTH_LONG).show();
										ConfirmDeliveryActivity.this.finish();
									} else {
										ErrorHandler.handleError(ConfirmDeliveryActivity.this, new Throwable(response.getString("message")));
									}
									
								} catch (JSONException e1) {}
							}
							
							@Override
							public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
								super.onFailure(statusCode, headers, responseString, throwable);
								DialogHelper.hideProgressDialog();
								ErrorHandler.handleError(ConfirmDeliveryActivity.this, throwable);
							}
							
							@Override
							public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
								super.onFailure(statusCode, headers, throwable, errorResponse);
								
								DialogHelper.hideProgressDialog();
								ErrorHandler.handleError(ConfirmDeliveryActivity.this, throwable);
							}
						});
					} catch (UnsupportedEncodingException | JSONException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}
	
	private void hideKeyBoard() {
		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(deliveryNoteNoView.getWindowToken(), 0);
	}
}