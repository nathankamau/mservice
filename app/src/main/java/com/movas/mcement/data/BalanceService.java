package com.movas.mcement.data;

import java.io.UnsupportedEncodingException;

import org.json.JSONException;

import com.loopj.android.http.JsonHttpResponseHandler;

public interface BalanceService {

	public void getBalance(String token, JsonHttpResponseHandler handler) throws UnsupportedEncodingException, JSONException;
}
